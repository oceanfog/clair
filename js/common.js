$(document).ready(function(){

	$(".order_chk_tbl tr th:last, .order_chk_tbl tr td:last").css({"border-right":"none"});

	$(".find_id_bg").hide();
	$("#findID").click(function(){
		$(".find_id_bg").slideToggle("fast");
		$(".find_pw_bg").slideUp("fast");
	});
	$(".find_id_inner a.btn_cancel").click(function(){
		$(".find_id_bg").slideUp("fast");
	});

	$(".find_pw_bg").hide();
	$("#findPW").click(function(){
		$(".find_pw_bg").slideToggle("fast");
		$(".find_id_bg").slideUp("fast");
	});
	$(".find_pw_inner a.btn_cancel").click(function(){
		$(".find_pw_bg").slideUp("fast");
	});

/* 시안확정팝업 */
	$(".draft_confirm_pop").hide();
	$("#draftConfirm").click(function(){
		$(".draft_confirm_pop").show();
	});
	$(".draft_confirm_pop ul li a").click(function(){
		$(".draft_confirm_pop").hide();
	});
/* 시안확정팝업 end */

/* 주문취소팝업 */
	$(".order_cancel_pop").hide();
	$(".btn_order_cancel").click(function(){
		$(".order_cancel_pop").show();
	});
	$(".order_cancel_pop ul li a").click(function(){
		$(".order_cancel_pop").hide();
	});
/* 주문취소팝업 end */

/* 주문취소팝업 */
	$(".draft_modify_pop").hide();
	$("#draftModify").click(function(){
		$(".draft_modify_pop").show();
	});
	$(".draft_modify_pop ul li a").click(function(){
		$(".draft_modify_pop").hide();
	});
/* 주문취소팝업 end */

/* 장바구니팝업 */
	$(".cart_pop").hide();
	$(".btn_cart").click(function(){
		$(".cart_pop").show();
	});
	$(".cart_pop ul li a").click(function(){
		$(".cart_pop").hide();
	});
/* 장바구니팝업 end */

});
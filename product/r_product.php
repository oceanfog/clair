<?php
    session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" >
<meta name="keywords" content="공기청정기, 나만의 공기청정기">
<title>클레어 기술 알아보기</title>


<link href="../css/common.css" rel="stylesheet" />
<link href="../css/reset.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />

<link href="../css/bootstrap_custom.css" rel="stylesheet" > 

<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="../shop2/app_root.js"></script>
<script type="text/javascript" src="../shop2/app_orderProc2.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


<?php include_once("../anal/analyticstracking.php"); ?>
</head>


<body ng-controller="OrderProc2Controller">
    <input type="hidden" value="{{topMenuStyle.01['color'] = '' }}">
    <input type="hidden" value="{{topMenuStyle.02['color'] = '#ff8500' }}">
    <input type="hidden" value="{{topMenuStyle.03['color'] = '' }}">
    <input type="hidden" value="{{topMenuStyle.04['color'] = '' }}">

        
    <? include "../inc/header2.php" ?>

    <div class="visible-xs visible-sm visible-md visible-lg" >
        <img src="../images/product/m_product_01.jpg" class="img-responsive" />
    </div>
    <div class="visible-xs visible-sm visible-md visible-lg" >
        <img src="../images/product/product_12.jpg" class="img-responsive" />
    </div>
    <div class="col-xs-12 col-lg-6" >
        <img src="../images/product/product_31.png" class="img-responsive" />
    </div>

    <div class="row">
        
        <div class="col-xs-12 col-lg-6 text-center " >
            <h4 class="line_height">
            <strong>
            불편함으로만 느껴지던 정전기가<br/>
            혁신적인 필터로 재탄생 되었습니다.<br/> 
            인간의 호흡기인 폐로도 걸러낼 수 없는<br/> 
            초 미세먼지까지도 손쉽게 포집이 가능한<br/> 
            <span style="color:#ff8400;">clair의 e2f 필터</span>를 만나보세요.
            </strong>
            </h4>
            
        </div>
        <div class="col-xs-12 col-lg-6 text-center margin_bottom" >
            <h5 class="line_height">
            <strong>
            공기 중 오염물질의 대부분은 (+)혹은 (–)극성.<br/>
            클레어 e2f 필터는 이런 원리에 착안 하여 <br/>
            필터 표면에  (+),( -) 극을 주입 하여,<br/>
            서로 다른 극성을 가진 오염물질을  끌어당기는<br/> 
            흡착형 필터를 사용해 공기중의 아주 작은(0.1㎛이하)<br/>  
            오염물질까지 포집합니다.
            </strong>
            </h5>
            
        </div>

    </div>


    <div class="movie col-xs-12 col-lg-6" >
        <div class="videoWrapper">
            <iframe  height="100%" src="//www.youtube.com/embed/yJK7ZDlQAFQ" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>

    <div class="row">
        <div class="movie col-xs-12 col-lg-6" >
            <h4>담배연기까지도 순식간에 정화하는<br />Clair의 놀라운 공기정화 능력을<br />바로 확인해보세요.</h4>
            
        </div>
    </div>
    <div class="row">
        <div class="movie col-xs-4" >
            <img src="../images/main/m_thumb01.gif" alt="" />
        </div>
        <div class="movie col-xs-4" >
            <img src="../images/main/m_thumb02.gif" alt="" />
        </div>
        <div class="movie col-xs-4" >
            <img src="../images/main/m_thumb03.gif" alt="" />
        </div>
    </div>

    <div class="row padding_top">
        <div class="col-xs-12 col-lg-12" >
            <img src="../images/product/product_41.png" class="img-responsive" />
        </div>
    </div>

    <div class="col-xs-12 col-lg-6" >
        <img src="../images/product/product_42.png" class="img-responsive" />
    </div>

    <div class="col-xs-12 col-lg-6" >
        <img src="../images/product/product_43.png" class="img-responsive" />
    </div>


    
    <!-- benefit 
    <div class="benefit col-xs-12">
        <div class="inner">
            <dl class="box01">
                <dt><span class="txt">"아직도 매월 3만원에서 5만원까지 렌트비를 내고 계시나요?</span><span>24시간 365일 사용해도,</span><br />전기 요금, 필터 관리 및 교체<br /><em>비용 부담이 적은 Clair</em></dt>
                <dd><img src="../images/main/img_benefit01.jpg" alt="24시간 365일 사용해도, 전기요금 1년에 약 2,000원! (기본료 제외, 누진요금 제외한 실 사용 전기요금 계산 시)" /></dd>
                <dd class="t01">필터 교체 비용 + 전기요금 = <span>1년 유지비용 2만 2천원!</span></dd>
                <dd class="t02">저렴한 전기요금으로 365일 24시간 마음 놓고!</dd>
            </dl>
            <dl class="box02">
                <dt>번거로운 필터 관리,<br /><em>청소기로 쉽고 간편하게!</em></dt>
                <dd>스스로 교환하는 필터와 청소기로 직접하는 필터관리로<br />유지비용을 현저히 낮췄습니다.</dd>
                <dd><img src="../images/main/img_benefit02.jpg" alt="" /></dd>
            </dl>
        </div>
    </div>

    -->
        
    <!-- control 
    <div class="control">
        <div class="inner">
            <h2>누구나 사용할 수 있는<br />clair공기청정기의 <em>간편한 조작방법!</em></h2>
            <p class="txt01">사용자가 직접 필터 분리, 교체, 청소가 가능한 손쉬운 유지관리는 기본,<br />버튼 하나로 동작하는 간편한 실행 방법으로 누구나 쉽게 사용하는 clair공기청정기!</p>
            <ol>
                <li><img src="../images/main/img_control01.jpg" alt="" /></li>
                <li><img src="../images/main/img_control02.jpg" alt="" /></li>
                <li><img src="../images/main/img_control03.jpg" alt="" /></li>
                <li><img src="../images/main/img_control04.jpg" alt="" /></li>
            </ol>
            
            <div class="skill">
                <div class="info">
                    <h3>초미세먼지 뿐 아니라 알레르센, 바이러스,<br />세균 및 휘발성 유기 화합물(VOC)<br /><em>제거에 특화된 clair의 기술력</em></h3>
                    <p>기존의 공기청정기와 다른 새로운 방식의 공기청정기!<br />clair공기청정기만의 특허 받은 e2f 정전기필터를 소개합니다.</p>
                </div>
                <div class="txt02">
                    <p>이 세상 대부분의 오염물질은 모두 극성을 가지고 있다는 점을 아시나요?<br />clair 공기청정기는 오염된 실내 공기가 고분자 합성수지 필름 양면에 (+,-)극성을 가진 e2f필터를 통과하면서 정전기 극성과 반대되는 극성을 가진 오염물질들이 필터에 붙게 되는 원리를 이용하여 개발되었습니다.</p>
                    <p>e2f필터는 헤파필터 등에서 포집하기 어려운 0.3μm이하의 초 미세먼지까지도 포집 가능합니다. (0.3이하 오염물질 99.9%포집) 또한, 무극성 오염물질의 경우 필터를 통과하면서 발생되는 유도 정전기를 통해 포집하여 주변의 오염물질을 빠짐없이 잡아냅니다.</p>
                </div>
            </div>
        </div>
    </div>
    -->
        
    <!-- safety
    <div class="safety">
        <div class="inner">
            <h2><em>철저한 제품 인증</em>으로 소비자의 안전을 생각합니다.</h2>
            <ul class="mark">
                <li><img src="../images/main/img_mark01.png" alt="유럽통합안전인증" /></li>
                <li><img src="../images/main/img_mark02.png" alt="전기용품 자율안전확인" /></li>
                <li><img src="../images/main/img_mark03.png" alt="생산물 배상책임보험 가입" /></li>
            </ul>
            <ul class="list">
                <li><img src="../images/main/img_list01.png" alt="국제특허1-1" /></li>
                <li><img src="../images/main/img_list02.png" alt="국제특허1-2" /></li>
                <li><img src="../images/main/img_list03.png" alt="실용신안 0360298호" /></li>
                <li><img src="../images/main/img_list04.png" alt="실용신안 0390077호" /></li>
                <li><img src="../images/main/img_list05.png" alt="실용신안 0391592호 모빌" /></li>
            </ul>
        </div>
    </div>
     -->        

        
        
   <? include "../inc/footer.php" ?>

    <script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script> <script type="text/javascript"> if(!wcs_add) var wcs_add = {}; wcs_add["wa"] = "c05970264893c8"; wcs_do(); </script>
</body>
</html>
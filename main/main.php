<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
    session_start();
    
?>
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<meta name="keywords" content="공기청정기, 나만의 공기청정기">
<title>마이클레어</title>

<script>
    FileAPI = {
        jsUrl: './js/FileAPI.min.js',
        flashUrl: './js/FileAPI.flash.swf',
    }

</script>
<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="../shop2/app_root.js"></script>
<script type="text/javascript" src="../controller/app_main.js"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1538686756348466']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1538686756348466&amp;ev=PixelInitialized" /></noscript>

<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link href="../css/reset.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<link href="../css/style_order.css" rel="stylesheet" />
<!-- <link href="../css/bootstrap_custom.css" rel="stylesheet" > 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
-->

<!-- Latest compiled and minified JavaScript -->
<style>


</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56547270-2', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body ng-controller="mainController">

    <!-- wrap -->
    <div id="wrap">
        <!-- header -->
        <? include "../inc/header.php" ?>
        <!-- // header -->
       
        <div class="cont">
            <div class="main_image">

                <a target="_blank" href="http://www.cleanair-clair.co.kr/" >클레어 공식 홈페이지 보기</a>
                
                <!-- 14.12.17 주석처리
                <img id="banner_01" src="../images/popup/popup_01.jpg" style="position: relative; top:285px; left:-309px; cursor: pointer;"/>
                -->

                <script>
                  $("#banner_01").click(function(){
                    window.location.href="http://www.cleanair-clair.co.kr/?p=3592";
                  });
                </script>

            </div>
            <div class="main_banner">
            	<a href="https://www.facebook.com/myclair.kr"><img src="../images/main/pc_facebook.jpg"></a>
            	<a href="https://goto.kakao.com/@마이클레어"><img src="../images/main/pc_kakao.jpg"></a>
              
              
            </div>
        </div>


        <? include "../inc/footer.php" ?>
        
    </div>
    <!-- wrap -->

    <script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script> <script type="text/javascript"> if(!wcs_add) var wcs_add = {}; wcs_add["wa"] = "c05970264893c8"; wcs_do(); </script>
    
</body>
</html>
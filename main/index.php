<? include "../inc/top.php" ?>
<script type="text/javascript">
//gnbScroll
$.fn.gnbscroll = function(options) {
    var defaults = {
        speed : 600,
        fx : "easeInQuad"
    };
    var options = $.extend(defaults, options);
    return $(this).each(function() {
        var element = this;
        $(element).click(function(event) {
            var locationHref = window.location.href;
            var elementClick = $(element).attr("href");
            var destination = $(elementClick).offset().top;
            $("html,body").animate({
                scrollTop : destination - 80
            }, options.speed, options.fx);
            event.preventDefault();
            return false;
        })
    })
}
$(function(){
    $('#gnb a').gnbscroll({
        fx : "easeInQuad"
    });
    //visual
    $('.visual_slide').bxSlider({
        auto:true,
        pager:false
    });
    //movie_layer
    $('.m_thumb').click(function(){
        $("<div class='inner'><object width='770' height='550'><param name='movie' value='https://www.youtube.com/v/wdY_H0gzJ0Y'><param name='allowFullScreen' value='true'><param name='allowscriptaccess' value='always'><param name='windowlessVideo' value='true'><embed src='https://www.youtube.com/v/wdY_H0gzJ0Y' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='770' height='550' wmode='transparent'></object><a href='#none' class='close'><img src='../images/main/btn_close.png' alt='close' /></a></div>").prependTo('.m_layer');
        $("<div class='layer_bg'></div>").prependTo('.visual_slide');
        $('.m_layer, .layer_bg').fadeIn();
        $('.m_layer .close').click(function(){
            $('.m_layer, .layer_bg').fadeOut();
            $('.m_layer .inner, .layer_bg').remove();
        });
    });
    /*
    //movie
    $('.movie_txt li a').click(function(){
        var obj = $(this).attr('href');
        //event.preventDefault();
        //$('.movie_box object').remove();
        //$("<object width='640' height='390'><param name='movie' class='link_value' value='"+ obj +"'><param name='allowFullScreen' value='true'><param name='allowscriptaccess' value='always'><param name='windowlessVideo' value='true'><embed class='link_embed' src='"+ obj +"' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='640' height='390' wmode='transparent'></object>").prependTo('.movie_box');
        $("<div><object width='640' height='390'><param name='movie' class='link_value' value='http://www.youtube.com/watch?v=5-Y4Yky-2P0'><param name='allowFullScreen' value='true'><param name='allowscriptaccess' value='always'><param name='windowlessVideo' value='true'><embed class='link_embed' src='http://www.youtube.com/watch?v=5-Y4Yky-2P0' type='application/x-shockwave-flash' allowscriptaccess='always' allowfullscreen='true' width='640' height='390' wmode='transparent'></object></div>").prependTo('.movie_box');
    });
    */
    
});
</script>
        <? include "../inc/header.php" ?>
        
        <!-- mcont -->
        <div class="mcont">
            
            <!-- visual -->
            <div class="visual">
                <div class="visual_slide">
                    <div class="v01">
                        <div class="inner">
                            <h2><span><em>보이지않는 살인자</em> 미세먼지,</span><br />지금도 당신의 가족들을<br />서서히 병들게 하고있습니다.</h2>
                        </div>
                    </div>
                    <div class="v02">
                        <div class="inner">
                            <h2><span><em>보이지않는 살인자</em> 미세먼지,</span><br />지금도 당신의 가족들을<br />서서히 병들게 하고있습니다.</h2>
                        </div>    
                    </div>
                </div>
                
            </div>
            <!-- // visual -->
            
            <!-- news -->
            <div class="news">
                <div class="inner">
                    
                    <div class="new_box">
                        <h3>죽음의 미세먼지 위험성</h3>
                        <dl>
                            <dt>[키워드 뉴스]죽음의 미세먼지의 위험성</dt>
                            <dd class="date">기사입력 2014-01-08 09:08</dd>
                            <dd class="photo"><a href="http://news.naver.com/main/read.nhn?mode=LPOD&mid=tvh&oid=449&aid=0000004411" target="_blank"><img src="../images/main/img_news01.gif" alt="" /></a></dd>
                            <dd class="more"><a href="http://news.naver.com/main/read.nhn?mode=LPOD&mid=tvh&oid=449&aid=0000004411" target="_blank">기사전문보기</a></dd>
                        </dl>
                    </div>
                    
                    <div class="new_box">
                        <h3>미세먼지 속에서 1시간 산책</h3>
                        <dl>
                            <dt>[아침 신문 보기] 미세먼지 속에서 1시간 산책, 디젤 매연 220분 마시는 격 外</dt>
                            <dd class="date">기사입력 2014-01-08 09:08</dd>
                            <dd class="photo"><a href="http://imnews.imbc.com/replay/2014/nwtoday/article/3422321_13495.html" target="_blank"><img src="../images/main/img_news02.gif" alt="" /></a></dd>
                            <dd class="more"><a href="http://imnews.imbc.com/replay/2014/nwtoday/article/3422321_13495.html" target="_blank">기사전문보기</a></dd>
                        </dl>
                    </div>
                    
                </div>
            </div>
            <!--//  news -->
            
        </div>
        <!-- // mcont -->
        
        <? include "../inc/footer.php" ?>

<?php
session_start();


include("../Dao/pdoObject.php");

$returnArray = array();


//part of receving id
$postJsonInfo = json_decode( $_POST["myObj"], true );


$orderNo = $postJsonInfo["no"];
$userId = $postJsonInfo["userId"];


ini_set("display_errors", "1");
$dirRoot = '/www/myclair_kr/images/adminUploadPicture/';
$uploaddir = $dirRoot.$userId;
$uploadFileName = basename(iconv("UTF-8", "cp949", $_FILES["file"]["name"]));
$uploadfile = $uploaddir."/".$uploadFileName;
 



$returnArray["id"] = $userId;
$returnArray["is_dir"] = is_dir($uploaddir);
$returnArray["uploaddir"] = $uploaddir;

//if the folder has not existed then make folder
if(is_dir($uploaddir)){
	$returnArray["message"] = "the folder name has existed";
}else{	
	$returnArray["message"] = "make folder";
	$returnArray["mkdir"] = mkdir($uploaddir, 0777);
}




if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    $returnArray["result"] = "file upload success";
    $returnArray["uploadFileName"] = $uploadFileName;
    

    //db에도 반영한다.
    $dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_list SET previewAddr = :previewAddr, orderStatus = :orderStatus WHERE no=:no' );
	$stmt->bindValue(':no', intval($orderNo), PDO::PARAM_INT);
	$stmt->bindValue(':previewAddr', $uploadFileName, PDO::PARAM_STR);
	$stmt->bindValue(':orderStatus', "시안 업로드", PDO::PARAM_STR);

	$result = $stmt->execute();

	//$stmt->debugDumpParams();

	$returnArray["dbUpdateResult"] = $result;
	$returnArray["orderNo"] = $orderNo;

} else {
	$returnArray["result"] = "file upload fail";
    //print "there are exisist availability file upload attack \n";
    //echo 'this is specific debuging info:';
	//print_r($_FILES);
}




echo json_encode( $returnArray );



?>


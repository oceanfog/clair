
angular.module('orderProc2')
 


.factory('factory', ['$http', '$upload', function($http, $upload){
	var obj = {};

	obj.loginUserInfo = {
		"userId":""
	};

 	obj.getPriceSum = function( p_cartList ){
		var sum = 0;
	 	for( i = 0 ; i < p_cartList.length; i++){
	 		sum = sum + parseInt( p_cartList[i].price );	 		
	 	}
	 	return sum;
	};

	obj.getRestfulResults = function( p_url ){
		return $http({
	      method: 'GET',
	      url: p_url,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.getRestfulResult = function( p_url, p_parameter ){
		return $http({
	      method: 'GET',
	      url: p_url + p_parameter,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.postRestfulResult = function( p_url, p_data ){
		return $http({
			method:'POST',
			url: p_url,
			data: p_data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}

		});

	};

	obj.putRestfulResult = function( p_url, p_data ){
		return $http({
	      method: 'POST',
	      url: p_url,
	      data: p_data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.fileUpload = function(p_url, p_order, p_file){
		return $upload.upload({
	        url: p_url, //upload.php script, node.js route, or servlet url
	        data: {myObj: p_order},
	        file: p_file,
	      })
	};

	obj.showImageOrignSize = function(){
		//image, location of print
		alert("imageorg");

	};
	

	return obj;
}]);



angular.module('orderProc2')
.controller('OrderProc2Controller', ['$scope','$http', '$upload', 'factory', function($scope, $http, $upload, factory){
	

	$scope.configuration = {};
	$scope.loginUserInfo = {
    	"userId":"",
    	"userName":"",
    	"hp":""
    };
    $scope.topMenuStyle = {
    	"01":{'color':''},
    	"02":{'color':''},
    	"03":{'color':''},
    	"04":{'color':'orange'}

    };


    $scope.bar = {"loaded":0, "total":0};
    $scope.insertTmpOrderResult = "";
    
	$scope.dpGoods = {};
	$scope.pagePointer = "1";
	$scope.cartItem = cartItem;
	$scope.cartList = [];
	$scope.orderTempAll = [];
	$scope.orderTemp = [];
	$scope.colors = [
		{value:'green', displayName:'그린'},
	    {value:'orange',displayName:'오렌지'},
	];
	$scope.types = [
		{value:"picture", displayName:'사진형'},
	    {value:"logo", displayName:'로고형'},
	    {value:"C타입", displayName:'C타입'}
    ];

    $scope.isOldAndNew = "old";

    $scope.address = {
    	"name":"",
    	"hp":"",
    	"zip1":"",
    	"zip2":"",
    	"orderAddr1":"",
    	"orderAddr2":""

    };
    $scope.oldAddress = {
    	"name":"",
    	"hp":"",
    	"zip1":"",
    	"zip2":"",
    	"orderAddr1":"",
    	"orderAddr2":""

    };

    $scope.tempOrder = {
    	"userId":"",
    	"totalPrice":"",
    	"discountAmount":"",
    	"finalPrice":"",
    	"pictureName":"",
    	"pictureType":"picture",
    	"color":"green",
    	"amount":"",
    	"price":"",
    	"hp":"",
    	"addr1":"",
    	"addr2":"",
    	"agree":""
    };

    $scope.mobileOrderModel = {
    	"price":"",
    	"hp":"",
    	"email":"",
    	"paymethod":"",
    	"zip1":"",
    	"zip2":"",
    	"orderAddr1":"",
    	"orderAddr2":""
    };

    $scope.userInfo = {

    };

    $scope.jsonMobileOrderModel = JSON.stringify( $scope.mobileOrderModel );

    
    $scope.init = function(){
    	//alert("hello");
    	$scope.getConfiguration();
    	//factory.printHello();
    }

    $scope.getConfiguration = function(){
    	$http({
	      method: 'GET',
	      url: "../restful/config.php/",
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    })

		.
	    success(function(response) {
	    	$scope.configuration =  response.result[0];
	    	
	    	$scope.getDpGoods();
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    }

    
    $scope.getPriceSum = function(){
	 	return factory.getPriceSum( $scope.cartList[i].price);
	}

    

	/***************************************************
	* 	daF
	***************************************************/
    //bring and binding the data of cart table in mysql using restful
    $scope.getCart = function(){
    	
		$http({
	      method: 'GET',
	      url: '../restful/cart.php/cart/'+$scope.tempOrder.userId,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log(response);
	    	
	        $scope.cartList = response.cartList;
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });

    };

    //call deleteCartItem for delete cart items
    $scope.deleteCartItem = function( p_no ){

    	$http({
	      method: 'GET',
	      url: '../restful/cart.php/delete_item/'+p_no,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	
	    	//console.log( response );
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };


	$scope.getDpGoods = function(){
		$http({
	      method: 'GET',
	      url: "../restful/goods.php/" + $scope.configuration["selectedItem"],
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//$scope.configuration =  response.result[0];
	    	$scope.dpGoods = response.result[0];
	    	
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    }


	$scope.getOrderTemp = function(){

	    factory.getRestfulResult('../restful/order_tmp.php/order/', $scope.loginUserInfo.userId).
	    success(function(response) {
	    	//console.log( response );
	    	$scope.orderTemp = response.result;
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };


    $scope.updateOrderTemp = function( p_tempOrderItem ){
    	var data = 'tmpOrderItem='+JSON.stringify( p_tempOrderItem );
    	var lv_return = "";

    	$http({
		      method: 'POST',
		      url: '../restful/order_tmp.php/update',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        //console.log( response );
		        //console.log( JSON.parse( response.requestJsonData ) );
	        	$scope.getOrderTemp();
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

    };


    $scope.deleteOrderTemp = function(){
    	$http({
	      method: 'GET',
	      url: '../restful/order_tmp.php/delete/' + $scope.loginUserInfo.userId,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	    	$scope.getOrderTemp();
	        
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };

    $scope.getOrderTempAll = function(){
    	
	    factory.getRestfulResults('../restful/order_tmp.php/order/').
	    success(function(response) {
	    	//console.log( response );
	    	$scope.orderTempAll = response.result;
	        
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };

    $scope.putOrderTemp = function( p_tempOrder){
    	var data = 'tmpOrderData='+JSON.stringify( p_tempOrder );
		factory.putRestfulResult( '../restful/order_tmp.php/put' , data )
			.success(function(response) {
		        
		        $scope.insertTmpOrderResult = "";
		        $scope.insertTmpOrderResult =  response["insertTmpOrderResult"];
		        
		    }).
		    error(function(response) {
		    	//alert("error");
		    	$scope.insertTmpOrderResult = "";
		        
		    });

		
    };



	/****************************************************	 button Listener
	****************************************************/

	


	//next button on select option page
    $scope.next_option = function(){
		if( validationPictureInputForm( $scope.tempOrder ) != false ){
			//$scope.putOrderTemp();
			//$scope.getOrderTemp();
			if( $scope.loginUserInfo.userId == "" || $scope.loginUserInfo.userId == null){
				var v = confirm("주문을 하시려면 로그인을 하셔야 합니다. 로그인 페이지로 이동하시겠습니까?");
				if (v == true){
					location.href = "./login3.html";
				}
			}else{
				$scope.tempOrder.totalPrice = $scope.dpGoods.dprice * $scope.tempOrder.amount;
				$scope.pagePointer = 2;	
			}
	    	
		}
    };


    $scope.move_to_orderProc = function(){
    	location.href = "./orderProc3.html";
    };


	$scope.moveOrderTempPage = function(){
		window.open("./getTempOrder.html", "","width=500, height=300");
	};

	$scope.callOrderPage = function(){
    	/*before move to the order.php
			insert temp order item to the table 'order'
    	*/
    	
    	if( validationCallOrder() != false ){
    		$scope.putOrderTemp( $scope.tempOrder );

    		if( $scope.insertTmpOrderResult == true){
    			//console.log( $scope.insertTmpOrderResult );
    			

    			if( MobileCheck() == "Computer"){
    				//location.href = "./m_inicis.html";
    				location.href = "./order.php";
    			} else {
    				location.href = "./m_inicis.html";
    			}
    			

    		} else {
    			//alert("다시 시도 해주세요.");
    		}
			
    	}

    };

    $scope.clickUpdate = function(p_tempOrderItem){
    	var r = confirm("수정 하시겠습니까?");

    	if( r ){
    		//console.log( p_tempOrderItem );
    		$scope.updateOrderTemp( p_tempOrderItem );
    	} else {
    		alert("수정하지 않습니다.");
    	}
    };

    $scope.findZip = function(){
		openDaumPostcode($scope);

	};

	$scope.findZip2 = function(){
		openDaumPostcode2($scope);

	};

	$scope.getUser = function(){
		factory.getRestfulResult('../restful/user.php/user/', $scope.mobileOrderModel.userId).
	    success(function(response) {

	    	//console.log( response );
	    	$scope.userInfo = response.result;

	    	$scope.mobileOrderModel.hp = response.result.hp;
	    	$scope.mobileOrderModel.email = response.result.email;
	    	$scope.mobileOrderModel.zip1 = response.result.zip1;
	    	$scope.mobileOrderModel.zip2 = response.result.zip2;
	    	$scope.mobileOrderModel.orderAddr1 = response.result.addr1;
	    	$scope.mobileOrderModel.orderAddr2 = response.result.addr2;

	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
	};



	//when you select file at orderProc3.html then the file uploaded to server
	$scope.onFileSelect = function($files) {
	    var file = $files[0];
	      
	    $scope.tempOrder.pictureName = "";

	    factory.fileUpload('./upload.php', $scope.loginUserInfo, file).progress(function(evt) {
	        //console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
	    	$scope.bar.loaded = parseInt( 100.0 * evt.loaded / evt.total );
	        $scope.bar.total = evt.total;

	    }).success(function(data, status, headers, config) {
	        // file is uploaded successfully
	        //console.log(data);
	        
	        //$scope.tempOrder.pictureName = data.fileName;
	        $scope.tempOrder.pictureName = data.savedFile;
	    });
	};


	$scope.changeOldorNew = function(){
		
		if( $scope.isOldAndNew == "new" ){
			$scope.address = angular.copy({}) ;
		} else {
				
			$scope.address = angular.copy($scope.oldAddress) ;
		}
	};

	


/*--------------------------------------------------
-- code for test
----------------------------------------------------*/
	$scope.test_order = function(){
		var test_tempOrder = {
			"userId":"oceanfog",
	    	"totalPrice":"178000",
	    	"discountAmount":"",
	    	"finalPrice":"178000",
	    	"pictureName":"463834047_584.jpg",
	    	"pictureType":"picture",
	    	"color":"green",
	    	"amount":"1",
	    	"price":"178000",
	    	"hp":"010-3588-6265",
	    	"addr1":"서울시 관악구 오피스",
	    	"addr2":"3동 402호",
	    	"agree":"ok"
		};

		
		var data = 'tmpOrderData='+JSON.stringify( test_tempOrder );
		factory.putRestfulResult( '../restful/order_tmp.php/put' , data )
		.success(function(response) {
			//console.log( response )	;
			if( response.insertTmpOrderResult){
				moveLocation();				


			}else{
				alert("tempOrder가 저장되지 않았습니다.");
			}
		});

		
		


	};





}]);




var cartItem = {
	color:"그린",
	type:"사진형",
	amount:"1",
	phrase:"",
	price:"",
	pictureName:"",
	userId:""

}


var validationPictureInputForm = function( p_cartItem ){
	
	/***************************************************
	**	모든 조건을 만족하면 true를 하나라도 만족하지 않으면 false를 return한다.
	**	receive cartItem object and check whether the value exisist nor nothing
	*************************************************/

	if( p_cartItem.pictureName == "" ){
		alert("사진을 올려주세요.");
		return false;
	}

	if( p_cartItem.agree != true ){
		alert("안내문을 읽었습니다에 체크 해주세요.");
		return false;
	}
}


var validationCallOrder = function(){

}


var MobileCheck = function(){
	var MobileVersi = navigator.userAgent.toLowerCase();
	var MobileArray = new Array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone");
	var checkCount  = 0;
	for(i=0; i<MobileArray.length; i++){ if(MobileVersi.indexOf(MobileArray[i]) != -1){ 
		checkCount++; break; }
	}
	
	return (checkCount >= 1) ? "Mobile" : "Computer";
}


function openDaumPostcode( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            
            p_scope.address.zip1 = data.postcode1;
            p_scope.address.zip2 = data.postcode2;
            p_scope.address.orderAddr1 = data.address;

            document.getElementById('zip1').value = data.postcode1;
            document.getElementById('zip2').value = data.postcode2;
            document.getElementById('orderAddr1').value = data.address;

            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;

           
        }
    }).open();
}


function openDaumPostcode2( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            p_scope.mobileOrderModel.zip1 = data.postcode1;
            p_scope.mobileOrderModel.zip2 = data.postcode2;
            p_scope.mobileOrderModel.orderAddr1 = data.address;

            document.getElementById('zip1').value = data.postcode1;
            document.getElementById('zip2').value = data.postcode2;
            document.getElementById('orderAddr1').value = data.address;

            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;

            
            
        },
        width : '100%',
        height : '100%'
    }).embed(element);

        // iframe을 넣은 element를 보이게 한다.
    element.style.display = 'block';
}


var moveLocation = function(){
	if( MobileCheck() == "Computer"){
		location.href = "./m_inicis.html";
		//location.href = "./order.php";
	} else {
		location.href = "./m_inicis.html";
	}
}
<?php
	
	$oid = $_GET['P_OID'];  
	echo $oid;	
	//return_url 뒤에 get방식으로 전달한 주문식별자 (ex: P_RETURN_URL = http://www.inicis.com/mx_rreturn.php?oid=123456)


	//oid를 가지고 P_NOTI_URL에서 처리한(DB에 입력된) 데이터를 가져와서 보여준다.
	//P_NOTI_URL이 먼저 호출되고,  P_RETURN_URL이 호출되는 것이라서 일반적인 경우는 문제가 없지만,
	//P_NOTI_URL쪽 처리가 지연되는 경우가 발생하면, 이 페이지가 호출되는 시점에 상점DB상에 데이터가 
	//존재하지 않을 수 있음. 따라서 select 결과가 없다고 무조건 승인 데이터가 없는 것은 아님

	//String SQL_QUERY = "select * from 상점테이블 where oid = 'oid' ";
	//쿼리 실행
	
	
  if(true){  // 쿼리결과가 있으면

		if($row[6] == "00") {  // 결과값중 P_STATUS 가 "00" 승인일 경우 
			echo "결제종류 : " . $paymethod_type . "<br />";
			echo $row[13]. "원 결제가 성공하였습니다. <br />";
			if($row[7] =="CARD") echo "승인번호 : " . $row[17];
			if($row[7] =="VBANK") {
				$explode_data = explode('|', $row[14]);
				$aaa = explode('=', $explode_data[0]);
				$bbb = explode('=', $explode_data[1]);
				echo "입금은행 : " . $row[11]. "<br />";
				echo "입금계좌 : " . $aaa[1] . "<br />";
				echo "입금기한 : " . $bbb[1] . "<br />";

			}
		}
		else {  // P_STATUS 가 "01" 실패일 경우 
			//echo "결제가 실패하였습니다. <br />";
		}
	}
	else {  // 쿼리 결과가 없으면 
	  //  처리지연 가능성 출력 / 개인 페이지에서 결제내역을 다시 확인하시기 바랍니다.
		echo "승인 내역이 존재하지 않습니다. 승인내역 처리가 늦어서 조회되지 않는 경우일 수 있습니다. 개인결제내역 페이지에서 내역을 다시 확인해 주시기 바랍니다.";
	}

		


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width"/>

<title>마이클레어</title>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/m_style_order_result.css" rel="stylesheet" />

<script>
    //optional need to be loaded before angular-file-upload-shim(.min).js
    FileAPI = {
        //only one of jsPath or jsUrl.
        jsUrl: '../js/FileAPI.min.js',

        //only one of staticPath or flashUrl.
        
        flashUrl: '../js/FileAPI.flash.swf',

        //forceLoad: true, html5: false //to debug flash in HTML5 browsers
    }
</script>
<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="./app_root.js"></script>
<script type="text/javascript" src="./app_orderProc2.js"></script>
<script type="text/javascript" src="../controller/m_resultOrder.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>
</head>
<body>


<div class="container">
	<div ng-hide="true" class="row">
		
		<input type="hidden" ng-init="inimx.oid = '<? echo $oid; ?>'" />
	</div>
	<div class="row">
		<div class="col-md-12 col-xs-12 col_01">
	      <div class="container">
	        <h2>
	          <h2>승인결과<small>Mobile</small></h2>
	        </h2>
	      </div>
	    </div>
	</div>

	<div ng-hide="inimx.oid == '' || inimx.oid == null " class="row imageArea">
		
		<div class="col-xs-12">
			<img src="../images/main/04_order_accept.jpg" class="img-responsive center-block">
		</div>



		<div class="col-xs-12">
			<img src="../images/main/04_order_accept_02.jpg" class="img-responsive center-block">
		</div>

	</div>

	<div ng-show="inimx.oid == '' || inimx.oid == null " class="row imageArea" >
		
		<div class="col-xs-12">
			<img class="img-responsive center-block" src='../images/main/04_order_cancel.jpg' />
		</div>
	</div>

	



	<div class="row top-buffer">
		<div class="col-xs-5 col-xs-offset-1">
	    	<button class="btn btn_common btn-block" onclick="location.href = '../shop2/orderProc3.html';" >확인</button>
	    	
	    </div>
	    <div class="col-xs-5">
	    	<button class="btn btn_common btn-block" onclick="location.href = '../shop2/myPage.php';" >마이페이지</button>
	    </div>
	</div>


</div>

</BODY>
</HTML>


<?php
    session_start();
    if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
        echo "<meta http-equiv='refresh' content='0;url=login2.html'>";
        exit;
    }

    function getConnection(){
        $connect=mysqli_connect('db.myclair.kr','myclair','myclair8702') or die ('not connect to server');
        mysqli_select_db($connect, 'dbmyclair') or die('select db error');
        mysqli_query($connect, 'set names utf8');

        return $connect;
    }

    function getResult( $p_userId ){
        //가격 불러오기
        $connect = getConnection();
        $sql="select * from order_tmp where userId ='".$p_userId."' limit 1";
        $result = mysqli_query($connect, $sql);


        if (!$result) {
            printf("Error: %s\n", mysqli_error($connect));
            exit();
        }        

        return mysqli_fetch_assoc($result);
    }
    

    $fetch_array = getResult($_SESSION['user_id']);

    //print_r($fetch_array);

    if( $fetch_array["price"] == 0 || $fetch_array["price"] == "0"){
        $fetch_array = getResult($_SESSION['user_id']);
        //echo "<meta http-equiv='refresh' content='0; url=order.php'>"; 
    }


    require("../INIpay50/libs/INILib.php");
    $inipay = new INIpay50;
    $inipay->SetField("inipayhome", "/www/myclair_kr/INIpay50");  
    $inipay->SetField("type", "chkfake");
    $inipay->SetField("debug", "true");
    $inipay->SetField("enctype","asym");
    $inipay->SetField("admin", "1111");
    $inipay->SetField("checkopt", "false");
    $inipay->SetField("mid", "myclairkr1");
    $inipay->SetField("price", $fetch_array["price"]);
    $inipay->SetField("nointerest", "no");
    $inipay->SetField("quotabase", "lumpsum:00:02:03:04:05:06:07:08:09:10:11:12");
    $inipay->startAction();
    if( $inipay->GetResult("ResultCode") != "00" ) 
    {
        echo $inipay->GetResult("ResultMsg");
        exit(0);
    }

    $_SESSION['INI_MID'] = "myclairkr1";    //상점ID
    $_SESSION['INI_ADMIN'] = "1111";
    $_SESSION['INI_PRICE'] = $fetch_array['price'];     //가격 
    $_SESSION['INI_RN'] = $inipay->GetResult("rn"); //고정 (절대 수정 불가)
    $_SESSION['INI_ENCTYPE'] = $inipay->GetResult("enctype"); //고정 (절대 수정 불가)
    



?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>결제 하기</title>


<script language=javascript src="http://plugin.inicis.com/pay61_secuni_cross.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>

<script>
    //optional need to be loaded before angular-file-upload-shim(.min).js
    FileAPI = {
        //only one of jsPath or jsUrl.
        jsUrl: '../js/FileAPI.min.js',

        //only one of staticPath or flashUrl.
        
        flashUrl: '../js/FileAPI.flash.swf',

        //forceLoad: true, html5: false //to debug flash in HTML5 browsers
    }
</script>
<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="./app_root.js"></script>
<script type="text/javascript" src="./app_orderProc2.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>

<script language=javascript>
  StartSmartUpdate();
</script>


<script language=javascript>

var openwin;

function pay(frm)
{
    // MakePayMessage()를 호출함으로써 플러그인이 화면에 나타나며, Hidden Field
    // 에 값들이 채워지게 됩니다. 일반적인 경우, 플러그인은 결제처리를 직접하는 것이
    // 아니라, 중요한 정보를 암호화 하여 Hidden Field의 값들을 채우고 종료하며,
    // 다음 페이지인 INIsecureresult.php로 데이터가 포스트 되어 결제 처리됨을 유의하시기 바랍니다.

    if(document.ini.sel_gopaymethod[0].checked == true){
        document.ini.gopaymethod.value = "onlycard";
    }else if(document.ini.sel_gopaymethod[1].checked == true){
        document.ini.gopaymethod.value = "onlydbank";
    }else if(document.ini.sel_gopaymethod[2].checked == true){
        document.ini.gopaymethod.value = "onlyvbank";
    }


    if(document.ini.clickcontrol.value == "enable")
    {

        if(document.ini.goodname.value == "")  // 필수항목 체크 (상품명, 상품가격, 구매자명, 구매자 이메일주소, 구매자 전화번호)
        {
            alert("상품명이 빠졌습니다. 필수항목입니다.");
            return false;
        }
        else if(document.ini.buyername.value == "")
        {
            alert("구매자명이 빠졌습니다. 필수항목입니다.");
            return false;
        } 
        else if(document.ini.buyeremail.value == "")
        {
            alert("구매자 이메일주소가 빠졌습니다. 필수항목입니다.");
            return false;
        }
        else if(document.ini.buyertel.value == "")
        {
            alert("구매자 전화번호가 빠졌습니다. 필수항목입니다.");
            return false;
        }
        else if( ( navigator.userAgent.indexOf("MSIE") >= 0 || navigator.appName == 'Microsoft Internet Explorer' ) &&  (document.INIpay == null || document.INIpay.object == null) )  // 플러그인 설치유무 체크
        {
            alert("\n이니페이 플러그인 128이 설치되지 않았습니다. \n\n안전한 결제를 위하여 이니페이 플러그인 128의 설치가 필요합니다. \n\n다시 설치하시려면 Ctrl + F5키를 누르시거나 메뉴의 [보기/새로고침]을 선택하여 주십시오.");
            return false;
        }
        else
        {
            /******
             * 플러그인이 참조하는 각종 결제옵션을 이곳에서 수행할 수 있습니다.
             * (자바스크립트를 이용한 동적 옵션처리)
             */
            
            if (MakePayMessage( frm ))
            {
                disable_click();
                openwin = window.open("childwin.html","childwin","width=100,height=0");     
                return true;
            }
            else
            {
                if( IsPluginModule() )     //plugin타입 체크
                {
                    alert("결제를 취소하셨습니다.");
                }
                return false;
            }
        }
    }
    else
    {
        return false;
    }
}


function enable_click()
{
    document.ini.clickcontrol.value = "enable"
}

function disable_click()
{
    document.ini.clickcontrol.value = "disable"
}

function focus_control()
{
    if(document.ini.clickcontrol.value == "disable")
        openwin.focus();
}


function submitIni(){

}
</script>


<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/reset.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<link href="../css/style_order.css" rel="stylesheet" />

<?php include_once("../anal/analyticstracking.php"); ?>

<!-- Facebook Conversion Code for 3.myclair.kr 결제하기 페이지 -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6021398295507', {'value':'0.00','currency':'KRW'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6021398295507&amp;cd[value]=0.00&amp;cd[currency]=KRW&amp;noscript=1" /></noscript>

</head>


<body onload="javascript:enable_click()" onFocus="javascript:focus_control()">

    <? include "../inc/header.php" ?>
        


    <!-- container -->
    <div class="cont" ng-controller="OrderProc2Controller">
        <input type="hidden" ng-init = "loginUserInfo.userId = '<?php echo $_SESSION['user_id']?>'" ng-model="loginUserInfo.userId" value="<?php echo $_SESSION['user_id'] ?>" />
        <input type="hidden" ng-init = "cartItem.userId = '<?php echo $_SESSION['user_id']?>'" ng-model="cartItem.userId" value="<?php echo $_SESSION['user_id'] ?>" />
        <input type="hidden" ng-init = "tempOrder.userId = '<?php echo $_SESSION['user_id']?>'" ng-model="tempOrder.userId" value="<?php echo $_SESSION['user_id'] ?>" />

        <div class="guide_01">
            <img src="../images/main/04_process03.jpg" style="width:1097px; text-align:center" /> 
        </div>
<!--************************************************
**** 4.주문 하는 페이지
*************************************************-->
        
        
        <div class="discount_section" ng-init="getOrderTemp()">
                <h2>주문 최종 확인</h2>

            <table class="pay_tbl1" summary="">
                <caption>결제 테이블</caption>
                <colgroup>
                    <col width="112">
                    <col width="326">
                    <col width="82">
                    <col width="95">
                    <col width="95">
                    <col width="95">
                    <col width="170">
                </colgroup>
                <thead>
                    <tr>
                        <th>상품정보</th>
                        <th></th>
                        <th>수량</th>
                        <th>상품금액</th>
                        <th >할인금액</th>
                        <th class="" >배송비</th>
                        <th class="b_f7f7f7">주문금액</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="cart in orderTemp">
                        <td>
                            <img src="../images/userUploadPicture/{{loginUserInfo.userId}}/{{cart.pictureName}}" width="100" height="100" alt="..." class="img-rounded">
                            
                            
                        </td>
                        <td style="text-align:left;">
                            <p>[<span>주문번호</span>]마이클레어-BF2025<br><span>·옵션 선택 : 색상({{cart.color=="green"?"그린":"오렌지"}})/사진타입({{cart.pictureType=="picture"?"사진형":"로고형"}})</span></p>
                        </td>
                        <td>{{cart.amount}}개</td>
                        <td>{{cart.price | number:0}}원</td>
                        <td >{{0 | number:0}}원</td>
                        <td class="" >0원</td>
                        <td class="b_f7f7f7">{{cart.price | number:0}}원</td>
                    </tr>
                    
                </tbody>
            </table>


            <!-- 할인 및 최종결제금액 -->
            <div class="discount_section">
                <h2>할인 및 최종결제금액</h2>
                <table class="pay_tbl2" summary="">
                    <caption>할인 및 최종결제금액 테이블</caption>
                    <colgroup>
                        <col width="120">
                        <col width="574">
                        <col width="96">
                        <col width="210">
                    </colgroup>
                    <tr>
                        <th>상품/주문쿠폰</th>
                        <td>

                        </td>
                        <td class="orange_bg">
                            <dl>
                                <dt>총 상품금액</dt>
                                <dd>{{<?php echo $fetch_array['price']; ?> | number:0}}원</dd>
                            </dl>
                            <dl>
                                <dt>배송비</dt>
                                <dd>0원</dd>
                            </dl>
                            <dl>
                                <dt>할인금액</dt>
                                <dd>0원</dd>
                            </dl>
                            <dl style="margin-top:10px; background: #e77800; padding: 10px 0px 10px 0px;">
                                <dt>결제금액</dt>
                                <dd>{{<?php echo $fetch_array['price']; ?> | number:0}}원</dd>
                            </dl>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- 할인 및 최종결제금액 end -->
            
            <form name=ini class="form-horizontal" method=post action="INIsecureresult.php" onSubmit="pay(this)">

            <input type="hidden" ng-init="oldAddress.name = '<?php echo $_SESSION['user_name']?>'"/>
            <input type="hidden" ng-init="oldAddress.hp = '<?php echo $_SESSION['hp']?>'" />
            <input type="hidden" ng-init="oldAddress.zip1 = '<?php echo $_SESSION['zip1']?>'"/>
            <input type="hidden" ng-init="oldAddress.zip2 = '<?php echo $_SESSION['zip2']?>'" />
            <input type="hidden" ng-init="oldAddress.orderAddr1 = '<?php echo $_SESSION['addr1']?>'" />
            <input type="hidden" ng-init="oldAddress.orderAddr2 = '<?php echo $_SESSION['addr2']?>'" />
            


            <!-- 배송지 정보입력 -->
            <div class="destination_section">
                <h2>배송지 정보입력</h2>
                <span class="orange_txt_l">● 필수입력</span>
                <span class="orange_txt_r">● 도서산간 지역의 경우 추후 수령 시 추가 배송비가 과금 될 수 있습니다. </span>
                <table class="pay_tbl3" summary="">
                    <caption>배송지 정보입력 테이블</caption>
                    <colgroup>
                        <col width="120">
                        <col width="574">
                        <col width="306">
                    </colgroup>
                    <tr>
                        <th>배송지 선택</th>
                        <td>
                            <p class="pay_tbl3_radios">
                                <input id="addr1" ng-change="changeOldorNew()" ng-model="isOldAndNew" name="addr" value="old" type="radio">
                                <label for="addr1">기본배송지</label>
                                <input id="addr2" ng-change="changeOldorNew()" ng-model="isOldAndNew" name="addr" value="new" type="radio">
                                <label for="addr2">신규배송지</label>
                            </p>

                            
                            <div class="pay_tbl3_info" ng-init="address = oldAddress">
                                <dl>
                                    <dt>수령인</dt>
                                    <dd><input type="text" name="userName" ng-model="address.name" id="inputEmail3" placeholder="이름"></dd>
                                </dl>
                                <dl>
                                    <dt>연락처</dt>
                                    <dd><input type="text" name="hp" ng-model="address.hp" id="inputPassword3" placeholder="연락처"></dd>
                                </dl>

                                <dl>
                                    <dt>우편번호</dt>
                                    <dd>
                                        <input type="text" id="zip1" ng-model="address.zip1" class="inputPostal" placeholder="우편번호1" ng-disabled="isOldAndNew=='old'" > -
                                        <input type="text" id="zip2" ng-model="address.zip2" class="inputPostal" placeholder="우편번호2" ng-disabled="isOldAndNew=='old'" >


                                        <a href="javascript:void(0);" ng-hide="isOldAndNew=='old'" ng-click="findZip()" class="btn btn-default btn-xs">우편번호</a>
                                    </dd>
                                </dl>

                                <dl>
                                    <dt>배송지</dt>

                                    <dd>
                                    <input type="text" id="orderAddr1" name="orderAddr1" ng-model="address.orderAddr1" class="input1" ng-disabled="isOldAndNew=='old'"  placeholder="주소" >
                                    <input type="text" id="orderAddr2" name="orderAddr2" ng-model="address.orderAddr2" class="input1" ng-disabled="isOldAndNew=='old'"  placeholder="상세 주소" >

                                    </dd>
                                </dl>
                                
                            </div>
                        </td>
                        <td rowspan="2" style="border-right:none;" class="gray">
                            <div class="orderer_info_ttl"><p>주문자 정보</p></div>
                            <div class="orderer_info">
                                <span class="orderer_name"><?php echo $_SESSION['user_name']?></span>
                                <br>핸드폰 번호 (<?php echo $_SESSION['hp']?>)
                                <br>이메일 주소(<?php echo $_SESSION['email']?>)</div>
                        </td>
                    </tr>
                    <tr>
                        <th>배송메모</th>
                        <td>
                            <p class="pay_tbl3_memo">
                                <span class="t_fe8422">배송메모란에는 배송시 참고할 사항이 있으면 적어주십시오.</span>
                                <br><span style="font-size:12px;">예) 부재시 경비실에 맡겨주세요.</span>
                            </p>
                            <textarea ng-model="address.expressMemo" class="pay_tbl3_txtarea"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- 배송지 정보입력 end -->
            <!-- 결제 정보입력 -->
				<div class="payment_section">
					<h2>결제 정보입력</h2>
					<span class="orange_txt_r">● 결제를 위한 소비자정보는 해당 결제 대행사에 제공됩니다.</span>
					<table class="pay_tbl4" summary="">
						<caption>결제 정보입력 테이블</caption>
						<colgroup>
							<col width="120">
							<col width="977">
						</colgroup>
						<tr>
							<th>일반결제</th>
							<td>
								<p class="pay_tbl4_radios">
                                    
                                    <input id="payM1" name="sel_gopaymethod" type="radio" value="0" checked>
									<label for="payM1">신용카드</label>
									<input id="payM2" name="sel_gopaymethod" type="radio" value="1" >
									<label for="payM2">실시간계좌이체</label>
									<input id="payM3" name="sel_gopaymethod" type="radio" value="2" >
									<label for="payM3">무통장입금</label>

                                    <input type=hidden name=gopaymethod value="">


								</p>
							</td>
						</tr>
						
						<tr>
							<th>결제안내</th>
							<td><p class="guide_txt">·일반 신용카드의 결제 혜택은 '결제하기' 클릭 시 노출되는 신용카드 결제창에서 확인하실 수 있습니다.</p></td>
						</tr>
						<tr>
							<th>결제방식안내</th>
							<td><p class="guide_txt">·가맹정마다 제공되는 <span class="t_008800">결제방식은 상이</span>할 수 있으며, 결제방식에서 확인되지 않은 결제수단은 지원되지 않습니다.<br>·결제를 위한 소비자 정보는 해당 결제 대행사에 제공됩니다.</p></td>
						</tr>
						
					</table>
				</div>
				<!-- 결제 정보입력 end -->

            <div>
            </div>
            
                <input type="hidden" name=buyername size=20 value="<?php echo $_SESSION["user_name"]; ?>">
                <input type="hidden" name=buyertel size=20 value="<?php echo $_SESSION["hp"]; ?>">

				
                

                <input type="hidden" name=goodname size=20 value="클레어 공기청정기">

                <input type="hidden" name=buyeremail size=20 value="<?php echo $_SESSION["email"]; ?>">
                <input type="hidden" name=currency size=20 value="WON">
                <!--
                <input type="hidden" name=acceptmethod size=20 value="HPP(1):Card(0):OCB:receipt:cardpoint">

                -->
                <input type="hidden" name=acceptmethod size=20 value="onlycard">
                <input type="hidden" name=oid size=40 value="oid_1234567890">

                <input type="hidden" name=ini_encfield value="<?php echo($inipay->GetResult("encfield")); ?>">
                <input type="hidden" name=ini_certid value="<?php echo($inipay->GetResult("certid")); ?>">
                <input type="hidden" name=quotainterest value="">
                <input type="hidden" name=paymethod value="Card">
                <input type="hidden" name=cardcode value="">
                <input type="hidden" name=cardquota value="">
                <input type="hidden" name=rbankcode value="">
                <input type="hidden" name=reqsign value="DONE">
                <input type="hidden" name=encrypted value="">
                <input type="hidden" name=sessionkey value="">
                <input type=hidden name=uid value=""> 
                <input type=hidden name=sid value="">
                <input type=hidden name=version value=4000>
                <input type=hidden name=clickcontrol value="">
                
                <input type="hidden" ng-model="address.orderAddr1" name=orderAddr1 value="{{address.orderAddr1}}">
                <input type="hidden" ng-model="address.orderAddr1" name=orderAddr2 value="{{address.orderAddr2}}">
                <input type="hidden" name=pictureAddr value="<?php echo $fetch_array['pictureName']; ?>">
                <input type="hidden" name="pictureType" value="<?php echo $fetch_array['pictureType']; ?>">
                <input type="hidden" name="color" value="<?php echo $fetch_array['color']; ?>">
                <input type="hidden" name="amount" value="<?php echo $fetch_array['amount']; ?>">
                <input type="hidden" ng-model="address.expressMemo" name="expressMemo" value="{{address.expressMemo}}">


                                                        
                
                
                
                
            
            <div class="step_btn_area">
                    <div class="step_btns">
                        <ul>
                            
                            <li class="two_button" ng-click="move_to_orderProc()">이전으로</li>
                            <li class="two_button"><button>결제하기</button></li>

                        </ul>
                    </div>
            </div>
            </form>
            
            

            <!--
            <button ng-click="pagePointer = 3" class="btn btn-success btn-lg">이전</button>
            -->

            </div>
                    
    </div>
    <!-- // container -->
        
    <br><br><br><br><br><br>
    
    <? include "../inc/footer.php" ?>

    <script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script>
    <script type="text/javascript"> if(!wcs_add) var wcs_add = {}; wcs_add["wa"] = "c05970264893c8"; wcs_do(); 
    </script>
    
</body>
</html>

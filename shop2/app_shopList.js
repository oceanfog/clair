(function(){
	var app = angular.module('order', ['angularFileUpload']);
	
	app.filter('krw', [ '$filter', '$locale',function(filter, locale) {
	    var currencyFilter = filter('currency');
	    var formats = locale.NUMBER_FORMATS;
	    return function(amount, currencySymbol) {
	      var value = currencyFilter(amount, "");
	      var sep = value.indexOf(formats.DECIMAL_SEP);
	      if(amount >= 0) { 
	        return value.substring(0, sep);
	      }
	      return value.substring(0, sep) + ')';
	    };
	  } ]);
	
	app.controller('OrderController', ['$scope','$http', '$upload', function($scope, $http, $upload){
		this.product = airCleaner;
		this.hello = "hello";
		$scope.session = session;
		$scope.colors = colors;
		$scope.types = types;
		
		$scope.val = "1";
		$scope.product = airCleaner;
		$scope.cartList = "";
		$scope.cartItem = cartItem;
		$scope.imageFileName = "hello.png";

		//when you select file then the file uploaded to server
		$scope.onFileSelect = function($files) {
		    //$files: an array of files selected, each file has name, size, and type.
		    for (var i = 0; i < $files.length; i++) {
		      var file = $files[i];
		      
		      $scope.upload = $upload.upload({
		        url: './upload.php', //upload.php script, node.js route, or servlet url
		        data: {myObj: $scope.myModelObj},
		        file: file,
		      }).progress(function(evt) {
		        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
		      }).success(function(data, status, headers, config) {
		        // file is uploaded successfully
		        console.log(data);
		        $scope.cartItem.pictureName = data.name;
		      });
		    }
		};


		$scope.testDataInsert = function(){
			$scope.cartItem = testCartItem;
		};
		
		$scope.printMsg = function( p_response ){
			if( p_response.insertCartResult == 1 ){
				if( confirm("장바구니에 등록 했습니다. 장바구니로 이동합니까?") ){
					location.href = "cart.html";
				}else {

				}
			}
		}


		$scope.putCart = function(){
			
			
			data = "cartData="+JSON.stringify( $scope.cartItem );
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			$http({
		      method: 'POST',
		      url: '../restful/cart.php/put/',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		    	//console.log( response );
		        $scope.printMsg( response );
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

		};



		$scope.purchase = function(){
			if( $scope.session.id != ""){
				location.href = "./INIsecurestart.php";
			} else {
				location.href = "./login.html";
			}
			
		};

		$scope.getCart = function(){

			data = 'myData='+JSON.stringify(airCleaner);
			data = "myData="+"hello";
			console.log(data);
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			$http({
		      method: 'GET',
		      url: '../restful/cart.php/cart',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        //$scope.cart = response.cartList;
		        console.log( response );
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

		    
		};

	}]);
})();

var session = {
	id:""
}

var colors = [
	{value:'green', displayName:'green'},
    {value:'orange',displayName:'orange'},
]

var types = [
	{value:"a", displayName:'A타입'},
    {value:"b", displayName:'B타입'},
    {value:"c", displayName:'C타입'}
]

var cartItem = {
	color:"",
	type:"",
	amount:"",
	phrase:"",
	price:"",
	pictureName:"",
	userId:""

}

var testCartItem = {
	color:"green",
	type:"a",
	amount:"1",
	phrase:"",
	price:"160000",
	pictureName:"/www/myclair.kr/images",
	userId:""
}

var airCleaner = {
	name:"",
	color:colors[0].name,
	type:"",
	price:2.95,
	colors: colors,
	soldOut:true
}


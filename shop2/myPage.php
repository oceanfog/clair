<?php
/****************************
 * 0. 세션 시작             *
****************************/
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
   echo "<meta http-equiv='refresh' content='0;url=login2.html'>";
    exit;
}

?>

<!DOCTYPE html>
<html lang="ko" ng-app="orderProc2">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <title>마이페이지</title>
        
        
        
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    		
        <link href="../css/reset.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet" />
        <link href="../css/style_order.css" rel="stylesheet" />
        <link href="../css/style_admin.css" rel="stylesheet" />
        <link href="../css/style_mypage.css" rel="stylesheet" />

        <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
        <script src="http://dmaps.daum.net/map_js_init/postcode.js"></script>


        <script>
            FileAPI = {
                jsUrl: '../js/FileAPI.min.js',
                flashUrl: '../js/FileAPI.flash.swf',
            }
        </script>
        <script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.js"></script>
        <script type="text/javascript" src="../js/angular-file-upload.min.js"></script>


        <script type="text/javascript" src="./app_root.js" charset="utf-8"></script>
        <script type="text/javascript" src="./app_orderProc2.js"></script>
        <script type="text/javascript" src="./app_myPage.js"></script>
        
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.2.js"></script>
    </head>

    <body>
        <div id="wrap">
            <? include "../inc/header.php" ?>
            <!-- 내용 -->
            <div class="cont" ng-controller="OrderController">
                <input type="hidden" ng-init = "loginUserInfo.userId = '<?php echo $_SESSION['user_id']?>'" ng-model="loginUserInfo.userId" value="<?php echo $_SESSION['user_id'] ?>" />

  

                <div>
                	<script type="text/ng-template" id="myModalContent.html">
	                    <div class="modal-header">
	                        <h3 class="modal-title">시안 수정 요청</h3>
	                    </div>
	                    <div class="modal-body">
	                    	<textarea rows="4" cols="50" ng-model="items.content"  class="form-control" ></textarea>

	                    </div>
	                    <div class="modal-footer">
                            <button class="btn btn-primary" ng-click="ok()">확인</button>
	                    	<button class="btn btn-warning" ng-click="cancel()">취소</button>
	                        
	                    </div>
                	</script>
                </div>

                

                <div class="mypage_list_area" >
                    <div class="orign_image" ng-hide="popupImageLocation == ''">
                        
                        <img src="{{popupImageLocation}}" ng-click="clickPopup()">
                        
                    </div>

                    <tabset>
                        <tab>
                        <tab-heading class="tab_default">
                            주문확인
                        </tab-heading>



                        <div class="order_chk_tbl_ttl">
                            
                        </div>
                      
                        <table class="my_order_chk_tbl" summary="" ng-init="getOrderList()">
                            <caption>주문확인 테이블</caption>
                            <colgroup>
                                <col width="69">
                                <col width="89">
                                <col width="177">
                                <col width="159">
                                <col width="137">
                                <col width="117">
                                <col width="94">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>주문번호</th>
                                    <th>주문일</th>
                                    <th>상품명(주문옵션)</th>
                                    <th>주문상태</th>
                                    <th>시안확인</th>
                                    <th>시안확정</th>
                                    <th>주문취소</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="order in orderList">
                                    <td>{{order.no}}</td>
                                    <td>{{order.orderDate}}</td>
                                    <td>
                                        {{order.modelNm}}<br>
                                        {{order.color == "green" ? "그린":"오렌지"}}( {{order.pictureType=="picture"?"사진형":"로고형"}} )
                                    </td>
                                    <td>{{order.orderStatus}}</td>
                                    <td>
                                        <img class="preview_image" ng-click="clickImage(order)" ng-show="order.previewAddr != '' && order.previewAddr != null " alt="" src="../images/adminUploadPicture/{{loginUserInfo.userId}}/{{order.previewAddr}}" >
                                        <img ng-show="order.previewAddr == '' || order.previewAddr == null" alt="" src="../images/adminUploadPicture/preparing.jpg" width="100px" height="100px">
                                    </td>
                                    <td>
                                        <a ng-show="order.orderStatus != '시안확정'" class="btn_draft" href="javascript:void(0);" ng-click="confirmPreview(order.no)" id="draftConfirm">시안확정하기</a>
                                        <a ng-show="order.orderStatus != '시안확정'" class="btn_draft" href="javascript:void(0);" ng-click="open('lg', order.userId, order.no)" id="draftModify">시안수정요청</a>
                                    </td>
                                    <td class="lastRow">
                                        <a ng-show="order.orderStatus != '시안확정'" class="btn_order_cancel" ng-click="askOrderCancel(order.no)" href="javascript:void(0);">주문취소요청</a>
                                    </td>
                                </tr>
                                <tr ng-show = "orderList == ''">
                                    <td colspan="7">주문 내역이 없습니다.</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="mypage_order_info">
                            
                            * 주문 및 결제 완료 2일 후 마이페이지에서 시안확정을 눌러주셔야 제작이 시작됩니다.<br/>
                            * 시안확정과 동시에 주문제작이 들어가는 상품으로 시안확정 시점부터 제품발송까지 3~5일(국/공휴일 제외)이 소요됩니다.<br/>
                            * 주문제작상품으로 시안확정을 눌러주신 시점부터 초기불량을 제외한 단순변심 반품 및 환불이 불가합니다.<br/>
                        </div>
                    </tab>

                    <tab ng-init="getUser()">
                      <tab-heading class="tab_default" ng-click="getUser()" >
                        회원정보 수정
                      </tab-heading>
                      

                      <div class="modify_user_info">
                        <dl>
                          <dt><span class="t_fe8422">＊</span>아이디</dt>
                          <dd>
                            <input class="input200" ng-model="userInfo.mem_id" ng-disabled="true"  name="" type="text" >
                          </dd>
                        </dl>
                        <dl>
                          <dt><span class="t_fe8422">＊</span>비밀번호</dt>
                          <dd><input ng-model="userInfo.mem_pass" class="input200"  type="password"></dd>
                        </dl>
                        <dl>
                          <dt><span class="t_fe8422">＊</span>비밀번호확인</dt>
                          <dd><input ng-model="userInfo.checkPassword" class="input200"  name="" type="password"></dd>
                        </dl>
                        <dl>
                          <dt><span class="t_fe8422">＊</span>이름</dt>
                          <dd><input ng-disabled="true" class="input200" ng-model="userInfo.kmem_name1" name="" type="text"></dd>
                        </dl>
                        <dl>
                          <dt><span class="t_fe8422">＊</span>핸드폰 번호</dt>
                          <dd>
                            <!-- 본인인증 서비스 팝업을 호출하기 위해서는 다음과 같은 form이 필요합니다. -->
                            <input type="text" ng-model="userInfo.hp" class="input200"  />
                              
                          </dd>
                        </dl>
                        
                        <dl>
                          <dt><span class="t_fe8422">＊</span>이메일</dt>
                          <dd>
                            
                            <input type="text" ng-model="userInfo.email" class="input200"  />
                          </dd>
                        </dl>
                        <dl>
                          <dt><span class="t_fe8422" >＊</span>주소</dt>
                          <dd>
                            

                            <input type="text" class="input46" ng-model="userInfo.zip1" id="zip1">
                            <p class="myinfo_opt_txt">-</p>
                            <input type="text" class="input46" ng-model="userInfo.zip2" id="zip2">
                            
                            
                            <a class="btn_zip" ng-click="findZip()" href="javascript:void(0);" >우편번호검색</a>
                            <input class="detail_addrr" id="addr1" ng-model="userInfo.addr1"  type="text" />
                            <input class="detail_addr2" id="addr2" ng-model="userInfo.addr2" type="text" />
                            
                          </dd>

                        </dl>

                      </div>
                      <div class="btn_box">
                        <button ng-click="updateUserInfo()" class="btn btn_common">수정하기</button>
                      </div>
                      

                        
                    </tab>
                  </tabset>


                </div>
            </div>
            <!-- 내용 end -->

            <div>
                <? include "../inc/footer.php" ?>
            </div>
        </div>

      <script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script>
      <script type="text/javascript"> if(!wcs_add) var wcs_add = {}; wcs_add["wa"] = "c05970264893c8"; wcs_do(); 
      </script>    
    </body>

</html>
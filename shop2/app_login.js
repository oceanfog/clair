angular.module('orderProc2')
.controller('LoginController', ['$scope','$http', '$upload', 'factory', function($scope, $http, $upload, factory){
		$scope.idPwInd = "closeBoth";
		$scope.loginInfo = {
			mem_id:"",
			mem_pass:""
		};
		$scope.findIdInfo = {
			"name":"",
			"email":""
		};

		$scope.findPasswordInfo = {
			"id":"",
			"name":"",
			"email":""
		};



		$scope.getLoginOk = function( p_loginInfo ){

			data = 'loginInfo='+JSON.stringify( p_loginInfo );
			//console.log(data);


			factory.postRestfulResult('login.php', data).
		    success(function(response) {
		    	$scope.loginInfo.mem_id = "";
		        $scope.loginInfo.mem_pass = "";

		        //console.log( response );
		        pai( response );	
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });
			
			/*
			$http({
		      method: 'POST',
		      url: 'login.php',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        
		        $scope.loginInfo.mem_id = "";
		        $scope.loginInfo.mem_pass = "";

		        //console.log( response );
		        pai( response );

		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

		    */
		};

		$scope.test = function(){
			//print return value for login
			if( loginValidation( $scope.loginInfo ) != false ){
				var result = $scope.getLoginOk( $scope.loginInfo );	
			}
			
			//console.log( result);

		};

		$scope.insertTestInfo = function(){
			//binding test data object
			//$scope.loginInfo = testLoginInfo;
		};

		$scope.findId = function(){

			data = 'findIdInfo='+JSON.stringify( $scope.findIdInfo );

			$http({
		      method: 'POST',
		      url: '../restful/user.php/findid',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        //console.log( response );
		        if( response["rowCount"] > 0 ){
		        	alert(response["list"]["email"]+"로 id를 발송 했습니다.");

		        	$scope.findIdInfo.name = "";
		        	$scope.findIdInfo.email = "";

		        } else {
		        	alert("일치하는 회원 정보가 없습니다. 확인 바랍니다.");
		        }
		        

		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });
		};


		$scope.findPassword = function(){

			data = 'findPasswordInfo='+JSON.stringify( $scope.findPasswordInfo );

			$http({
		      method: 'POST',
		      url: '../restful/user.php/findpassword',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        //console.log( response );
		        if( response["rowCount"] > 0 ){
		        	alert(response["list"]["email"]+"로 password를 발송 했습니다.");

		        	$scope.findPasswordInfo.id = "";
		        	$scope.findPasswordInfo.name = "";
		        	$scope.findPasswordInfo.email = "";

		        } else {
		        	alert("일치하는 회원 정보가 없습니다. 확인 바랍니다.");
		        }
		        

		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });
		};


	}]);


var pai = function( p_resultObject ){
	if( p_resultObject.resultCode == 1 ){
		//session에 id, 이름 등 로그인 정보를 입력한다.
		//main page로 이동한다.
		location.href = "http://myclair.kr/";

	} else {
		alert("아이디 또는 비밀번호를 확인 해주세요.");
	}
}



var testLoginInfo = {
	mem_id:"oceanfog",
	mem_pass:"1123"
}



var loginValidation = function( p_loginInfo ){
	if( p_loginInfo.mem_id == "" ){
		alert("아이디를 입력 해주세요.");
		return false;
	}

	if( p_loginInfo.mem_pass == "" ){
		alert("비밀번호를 입력 해주세요.");
		return false;
	}
}
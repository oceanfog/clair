//hello
angular.module('orderProc2')
.controller('OrderController', ['$scope','$http', 'factory', '$modal', '$log', function($scope, $http, factory, $modal, $log  ){

	$scope.years = [];
	for(var i = 1901 ; i < 2030 ; i++) {
	  $scope.years.push( {"value":i, "displayName":i} );
	}

	$scope.searchCondition = searchCondition;
	$scope.loginUserInfo = {
		"userId":""
	};
	$scope.orderList = "";

	$scope.sessionInfo = {
		userId:"",
		userName:""
	};

	$scope.popupImageLocation = "";


	$scope.clickImage = function( p_order ){
		
		
		//factory.showImageOrignSize();

		$scope.popupImageLocation = "../images/adminUploadPicture/"+p_order.userId+"/"+p_order.previewAddr;
		
	};

	$scope.clickPopup = function(){
		$scope.popupImageLocation = "";
	};


	$scope.getPriceSum = function(){
		var sum = 0;
	 	for( i = 0 ; i < $scope.cartList.length; i++){
	 		
	 		sum = sum + parseInt( $scope.cartList[i].price );
	 		
	 	}
	 	return sum;
	}


	$scope.getUser = function(){

	    factory.getRestfulResult('../restful/user.php/user/', $scope.loginUserInfo.userId).
	    success(function(response) {
	    	//console.log( response );
	    	$scope.userInfo = response.result;
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };
	
    $scope.updateUserInfo = function(){
    	if( validationModifyUserInfo( $scope.userInfo ) != false ){
    		var data = 'userInfo='+JSON.stringify( $scope.userInfo );
			factory.putRestfulResult( '../restful/user.php/update' , data )
			.success(function(response) {
				
				if( response.updateUserInfoResult){
					alert("회원정보가 변경되었습니다.");

					$scope.getUser();
					location.href="./myPage.php";

				}else{
					
				}
			});
    	}
    	
    };

    $scope.findZip = function(){
		openDaumPostcode($scope);

	};

	$scope.getOrderList = function(){
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		$http({
	      method: 'GET',
	      url: '../restful/order.php/order/' + $scope.loginUserInfo.userId,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	        $scope.orderList = response.result;
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });

	    
	};



	$scope.confirmPreview = function( p_orderNo){
		var isConfirm = confirm("시안을 확인 후, 확정 하시겠습니까? \n확정 후에는 취소 및 수정이 불가능 합니다.", p_orderNo);

		if( isConfirm ){

			$http({
		      method: 'GET',
		      url: '../restful/order.php/update/test/' + p_orderNo,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		    	//console.log( response );
		    	$scope.getOrderList();
		        
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

			alert("확정 했습니다.");
		} else{

		}

	};

	

	$scope.askOrderCancel = function( p_orderNo ){
		
		var isAskCancel = confirm("취소 요청 하시겠습니까?");
		if( isAskCancel){
			
		    factory.getRestfulResult( '../restful/order.php/askcancel/',  p_orderNo).
		    success(function(response) {
		    	console.log( response );
		    	$scope.getOrderList();
		    	alert("주문 취소 요청 되었습니다. 담당자가 승인을 한 후에 취소가 완료 됩니다.");
		        
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

			
		}

	};

	$scope.open = function (size, p_userId, p_orderNo) {

	    var modalInstance = $modal.open({
	      templateUrl: 'myModalContent.html',
	      controller: 'ModalInstanceCtrl',
	      size: size,
	      resolve: {
	        items: function () {
	          return {"userId":p_userId, "orderNo":p_orderNo };
	        }
	      }
	    });

	    modalInstance.result.then(function (selectedItem) {
	      $scope.selected = selectedItem;
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
  };

}]);


angular.module('orderProc2')
.controller('ModalInstanceCtrl', ['$scope','$modalInstance','items','factory',  function ($scope, $modalInstance, items, factory) {
	

	$scope.items = items;
	$scope.selected = {
    //item: $scope.items[0]
    //console.log( "modalInstanceCtrl" + items);
	};

	$scope.ok = function () {

		if( validationAskModify( items ) != false ){
			var data = 'askModifyArticle='+JSON.stringify( items );

		  	factory.putRestfulResult('../restful/ask_modify.php/test/', data).
			    success(function(response) {
			    	console.log( response );
			    	//$scope.orderTemp = response.result;

			    	if( response.result && response.result2){
			    		alert("수정 요청 되었습니다.");
			    		location.reload();
			    	}else {
			    		console.log (response.result);
			    		console.log (response.result2);
			    	}
			        
			    }).
			    error(function(response) {
			        $scope.codeStatus = response || "Request failed";
			    });


	    	$modalInstance.close($scope.selected.item);
		}

	
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);


var colors = [
	{name:'green', shade:'green'},
    {name:'orange', shade:'orange'},
]


var searchCondition = {
	userId:""
}



var validationAskModify = function( p_askModify ){
	
	/***************************************************
	**	모든 조건을 만족하면 true를 하나라도 만족하지 않으면 false를 return한다.
	**	receive askModify object and check whether the value exisist nor nothing
	*************************************************/

	if( p_askModify.content == "" || p_askModify.content == null ){
		alert("내용을 입력해주세요");
		return false;
	}
}

var validationModifyUserInfo = function( p_userInfo ){
	if( p_userInfo.mem_pass == "" || p_userInfo.mem_pass == null ){
		alert("비밀번호를 입력해주세요.");
		return false;
	}
	if( p_userInfo.checkPassword == "" || p_userInfo.checkPassword == null ){
		alert("비밀번호 확인을 입력해주세요.");
		return false;
	}

	if( p_userInfo.mem_pass != p_userInfo.checkPassword ){
		alert("비밀번호와 비밀번호 확인이 서로 다릅니다.");
		return false;
	}	
}



function openDaumPostcode( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            p_scope.userInfo.zip1 = data.postcode1;
            p_scope.userInfo.zip2 = data.postcode2;
            p_scope.userInfo.addr1 = data.address;

            

            document.getElementById('zip1').value = data.postcode1;
            document.getElementById('zip2').value = data.postcode2;
            document.getElementById('addr1').value = data.address;

            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;


            

           
        }
    }).open();
}
<?php
include("../master/db_connect.inc");

/*
회원 가입 form에서 입력 받은 데이터를 db에 저장한다.
성공하면 success를 return하고 실패하면 false를 return한다.

*/

function getPasswordCheckResult($p_connect, $p_loginInfo){
	
	$sql="select mem_id, mem_pass, mem_gubun from member where mem_id='".$p_loginInfo['mem_id']."'";
	$sql = iconv('utf8', 'euckr', $sql);
	$result = mysqli_query($p_connect, $sql);

	if (!$result) {
	    printf("Error: %s\n", mysqli_error($p_connect));
	    exit();
	}

	$r = mysqli_fetch_array($result);

	if( $p_loginInfo['mem_pass'] == $r['mem_pass'] ){
	    return true;
	} else {
	    return false;
	}
}

function getUserInfo($p_connect, $p_loginInfo){
	$sql="select mem_gubun, mem_id, kmem_name1, hp, email, zip1, zip2, addr1, addr2 from member where mem_id='".$p_loginInfo['mem_id']."'";
	$sql = iconv('utf8', 'euckr', $sql);
	$result = mysqli_query($p_connect, $sql);

	if (!$result) {
	    printf("Error: %s\n", mysqli_error($p_connect));
	    exit();
	}

	return mysqli_fetch_array($result);
}

function printIntoJson( $p_fetchArray ){
	return json_encode( $p_fetchArray );
}


//post로 받은 로그인 정보(id, password)
$loginInfo = json_decode($_POST['loginInfo'], true);

//result of password check. the result returned into true or false
$passwordCheckResult = getPasswordCheckResult($connect, $loginInfo);

$userInfo;




//password check 결과에 따라 비즈니스 로직을 수행함.
if( $passwordCheckResult == true ){

	//get user info
	$userInfo = getUserInfo( $connect, $loginInfo );
	

	//start session
	echo "{\"resultCode\":1, \"message\":\"id, pw ok\", \"userInfo\":".json_encode( $userInfo )."}";

} else {
    echo "{\"resultCode\":2, \"message\":\"password wrong\"}";
}


session_start();
$_SESSION['user_id'] = $userInfo["mem_id"];
$_SESSION['user_name'] = $userInfo["kmem_name1"];
$_SESSION['hp'] = $userInfo["hp"];
$_SESSION['email'] = $userInfo["email"];
$_SESSION['zip1'] = $userInfo["zip1"];
$_SESSION['zip2'] = $userInfo["zip2"];
$_SESSION['addr1'] = $userInfo["addr1"];
$_SESSION['addr2'] = $userInfo["addr2"];
$_SESSION['mem_gubun'] = $userInfo["mem_gubun"];



?>
<?php
	session_start();

	require("../INIpay50/libs/INImx.php");
	include("../Dao/pdoObject.php");
	
	$inimx = new INImx;


	/////////////////////////////////////////////////////////////////////////////
	///// 1. 변수 초기화 및 POST 인증값 받음                                 ////
	/////////////////////////////////////////////////////////////////////////////
	
	$inimx->reqtype 	= "PAY";  //결제요청방식
	$inimx->inipayhome 	= "/www/myclair_kr/INIpay50"; //로그기록 경로 (이 위치의 하위폴더에 log폴더 생성 후 log폴더에 대해 777 권한 설정)
	$inimx->status		= $P_STATUS;
	$inimx->rmesg1		= $P_RMESG1;
	$inimx->tid		= $P_TID;
	$inimx->req_url		= $P_REQ_URL;
	$inimx->noti		= $P_NOTI;
	
	
	/////////////////////////////////////////////////////////////////////////////
	///// 2. 상점 아이디 설정 :                                              ////
	/////    결제요청 페이지에서 사용한 MID값과 동일하게 세팅해야 함...      ////
	/////    인증TID를 잘라서 사용가능 : substr($P_TID,'10','10');           ////
	/////////////////////////////////////////////////////////////////////////////
	$inimx->id_merchant = substr($P_TID,'10','10');  //

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta name="viewport" content="width=device-width"/>

<title>마이클레어</title>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/m_style_order_result.css" rel="stylesheet" />

<script>
    //optional need to be loaded before angular-file-upload-shim(.min).js
    FileAPI = {
        //only one of jsPath or jsUrl.
        jsUrl: '../js/FileAPI.min.js',

        //only one of staticPath or flashUrl.
        
        flashUrl: '../js/FileAPI.flash.swf',

        //forceLoad: true, html5: false //to debug flash in HTML5 browsers
    }
</script>
<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="./app_root.js"></script>
<script type="text/javascript" src="./app_orderProc2.js"></script>
<script type="text/javascript" src="../controller/m_resultOrder.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>



<?php include_once("../anal/analyticstracking.php"); ?>
</head>

<body ng-controller="ResultOrderCtrl">


<?
	/*
	echo "----------";
	print_r( $inimx );
	echo "----------";
	print_r($_POST);
	echo "----------";
	echo $_POST["addr1"];
	echo "----------";

	*/






	/////////////////////////////////////////////////////////////////////////////
	///// 3. 인증결과 확인 :                                                 ////
	/////    인증값을 가지고 성공/실패에 따라 처리 방법                      ////
	/////////////////////////////////////////////////////////////////////////////
  if($inimx->status =="00")   // 모바일 인증이 성공시
  {
	/////////////////////////////////////////////////////////////////////////////
	///// 4. 승인요청 :                                                      ////
	/////    인증성공시  P_REQ_URL로 승인요청을 함...                        ////
	/////////////////////////////////////////////////////////////////////////////
	$inimx->startAction();  // 승인요청
	  
	$inimx->getResult();  //승인결과 파싱, P_REQ_URL에서 내려준 결과값 파싱 

?>

<?
	
	if( $inimx->m_resultCode == "00"){
		$a = explode( "|", $inimx->m_noti );

		$order = array();
		$order["orderDate"] = $inimx->m_pgAuthDate;
		$order["orderTime"] = $inimx->m_pgAuthTime;
		$order["userId"] = $a[0];
		$order["userName"] = $a[1];
		$order["pmtMethod"] = $inimx->m_payMethod;
		$order["orderAddr1"] = $a[2];
		$order["pictureAddr"] = $a[4];
		$order["price"] = $inimx->m_resultprice;
		$order["pictureType"] = $a[5];
		$order["color"] = $a[6];
		$order["amount"] = $a[7];
		$order["hp"] = $_SESSION["hp"];
		$order["orderAddr2"] = $a[3];
		$order["expressMemo"] = $a[8];
		$order["tid"] = $inimx->m_tid;

		if( $inimx->m_payMethod == "VBANK"){
			$order["orderStatus"] = "결제대기";
		}else {
			$order["orderStatus"] = "결제완료";
		}

		//print_r( $order );

		/*
		orderDate, orderTime, userId, userName, pmtMethod,
		orderAddr1,	pictureAddr, price,	pictureType, color, amount,	hp,
		orderAddr2,	expressMemo, tid, orderStatus
		*/
		
		$insertResult = insertOrder_new( $order );


	}else {

	}
	


?>


<?	
	switch($inimx->m_payMethod){   
    	case(CARD):  //신용카드 안심클릭
    	/*
	  		echo("승인번호:".$inimx->m_authCode."<br>");
			echo("할부개월:".$inimx->m_cardQuota."<br>");
			echo("카드코드:".$inimx->m_cardCode."<br>");
			echo("발급사코드:".$inimx->m_cardIssuerCode."<br>");
			echo("카드번호:".$inimx->m_cardNumber."<br>");
			echo("가맹점번호:".$inimx->m_cardMember."<br>");
			echo("매입사코드:".$inimx->m_cardpurchase."<br>");
			echo("부분취소가능여부(0:불가, 1:가능):".$inimx->m_prtc."<br>");
		*/
		break;
?>

<?
		case(VBANK):  //가상계좌
?>
		<input type="hidden" ng-init="inimx.m_vacct = '<? echo $inimx->m_vacct; ?>'" />
		<input type="hidden" ng-init="inimx.m_dtinput = '<? echo $inimx->m_dtinput; ?>'" />
		<input type="hidden" ng-init="inimx.m_tminput = '<? echo $inimx->m_tminput; ?>'" />
		<input type="hidden" ng-init="inimx.m_nmvacct = '<? echo $inimx->m_nmvacct; ?>'" />
		<input type="hidden" ng-init="inimx.m_vcdbank = '<? echo $inimx->m_vcdbank; ?>'" />
<?
		break;
		
		default: //문화상품권,해피머니

	  }
	
	}else{                      // 모바일 인증 실패



	?>
		<!--인증결과메세지 -->
	 	<input type="hidden" ng-init="inimx.rmesg1 = '<? echo $inimx->rmesg1; ?>'" />
<?
	}

?>
<input type="hidden" ng-init="inimx.status = '<? echo $inimx->status; ?>'" />
<input type="hidden" ng-init="inimx.m_resultCode = '<? echo $inimx->m_resultCode; ?>'" />
<input type="hidden" ng-init="inimx.m_resultMsg = '<? echo $inimx->m_resultMsg; ?>'" />

<input type="hidden" ng-init="inimx.m_payMethod = '<? echo $inimx->m_payMethod; ?>'" />
<input type="hidden" ng-init="inimx.m_moid = '<? echo $inimx->m_moid; ?>'" />
<input type="hidden" ng-init="inimx.m_tid = '<? echo $inimx->m_tid; ?>'" />

<input type="hidden" ng-init="inimx.m_resultprice = '<? echo $inimx->m_resultprice; ?>'" />
<input type="hidden" ng-init="inimx.m_pgAuthDate = '<? echo $inimx->m_pgAuthDate; ?>'" />
<input type="hidden" ng-init="inimx.m_pgAuthTime = '<? echo $inimx->m_pgAuthTime; ?>'" />
<input type="hidden" ng-init="inimx.m_mid = '<? echo $inimx->m_mid; ?>'" />
<input type="hidden" ng-init="inimx.m_buyerName = '<? echo $inimx->m_buyerName; ?>'" />
<input type="hidden" ng-init="inimx.m_noti = '<? echo $inimx->m_noti; ?>'" />
<input type="hidden" ng-init="inimx.m_nextUrl = '<? echo $inimx->m_nextUrl; ?>'" />
<input type="hidden" ng-init="inimx.m_notiUrl = '<? echo $inimx->m_notiUrl; ?>'" />


<div class="container">
	<div class="row">
		<div class="col-md-12 col-xs-12 col_01">
	      <div class="container">
	        <h2>
	          <h2>승인결과<small>Mobile</small></h2>
	        </h2>
	      </div>
	    </div>
	</div>

	<div class="row">
		<div class="col-xs-12">
	        <!--  
	        지불수단	m_payMethod
			주문번호	m_moid
			TID			m_tid
			승인금액	m_resultprice
			승인일		m_pgAuthDate
			승인시각	m_pgAuthTime
			상점ID		m_mid
			구매자명	m_buyerName
			P_NOTI		m_noti
			NEXT_URL	m_nextUrl
			NOTI_URL	m_notiUrl
			-->


	        <!--
				test parameter 넣는 부분.
				아래 markup을 주석 밖에 놓아 값을 설정 할 수 있다.

				<div ng-init="inimx.m_resultCode = '00'"></div> 	
        		<div ng-init="inimx.m_payMethod = 'VBANK'"></div>

			-->


	        
        	
	        
		    <div class="imageArea" ng-show="inimx.m_resultCode == '00' && inimx.m_payMethod != 'VBANK'">
				<div class="col-xs-12">
					<img src="../images/main/04_order_complete.jpg" class="img-responsive center-block">
				</div>
			</div>


			<div class="imageArea" ng-show="inimx.m_resultCode == '00' && inimx.m_payMethod == 'VBANK'">
				<div class="col-xs-12">
					<img src="../images/main/04_order_accept.jpg" class="img-responsive center-block">
				</div>

				<div class="col-xs-10 col-xs-offset-1">
					<div class="innerBox center-block">
				    	<!--
						가상계좌번호	$inimx->m_vacct
						입금예정일	$inimx->m_dtinput
						입금예정시각	$inimx->m_tminput
						예금주	$inimx->m_nmvacct
						은행코드	$inimx->m_vcdbank
						-->
						

		        		계좌번호       : {{inimx.m_vacct}}<br/>
						은행           : {{bankCode[inimx.m_vcdbank]}}  <br/>
						예금주 명      : 에이치앤드컴퍼니 <br/>
						송금자 명      : {{inimx.m_buyerName}}<br/>
						송금 예정 일자 : {{inimx.m_dtinput}}<br/>
					</div>
				</div>

				<div class="col-xs-12">
					<img src="../images/main/04_order_accept_02.jpg" class="img-responsive center-block">
				</div>
			
		  	</div><!-- imageArea -->

	    
			<div class="imageArea" ng-hide="inimx.m_resultCode == '00'">
				<div class="col-xs-12">
					<img src="../images/main/04_order_cancel.jpg" class="img-responsive center-block">
				</div>
			</div> <!-- imageArea -->
		</div>
	</div>
	
	<div class="row top-buffer">
		<div class="col-xs-5 col-xs-offset-1">
	    	<button class="btn btn_common btn-block" onclick="location.href = '../shop2/orderProc3.html';" >확인</button>
	    	
	    </div>
	    <div class="col-xs-5">
	    	<button class="btn btn_common btn-block" onclick="location.href = '../shop2/myPage.php';" >마이페이지</button>
	    </div>
	</div>


</body>

</html>



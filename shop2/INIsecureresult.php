<?php
/* INIsecurepay.php
 *
 * 이니페이 플러그인을 통해 요청된 지불을 처리한다.
 * 지불 요청을 처리한다.
 * 코드에 대한 자세한 설명은 매뉴얼을 참조하십시오.
 * <주의> 구매자의 세션을 반드시 체크하도록하여 부정거래를 방지하여 주십시요.
 *  
 * http://www.inicis.com
 * Copyright (C) 2006 Inicis Co., Ltd. All rights reserved.
 */

  /****************************
   * 0. 세션 시작             *
   ****************************/
  	session_start();								//주의:파일 최상단에 위치시켜주세요!!
  	include_once("../anal/analyticstracking.php");
  	
    if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
        echo "<meta http-equiv='refresh' content='0;url=login2.html'>";
        exit;
    }
	/**************************
	 * 1. 라이브러리 인클루드 *
	 **************************/
	require("../INIpay50/libs/INILib.php");
	include("../master/db_connect.inc");


	
	/***************************************
	 * 2. INIpay50 클래스의 인스턴스 생성 *
	 ***************************************/
	$inipay = new INIpay50;

	/*********************
	 * 3. 지불 정보 설정 *
	 *********************/
	$inipay->SetField("inipayhome", "/www/myclair_kr/INIpay50");  
	$inipay->SetField("type", "securepay");                         // 고정 (절대 수정 불가)
	$inipay->SetField("pgid", "INIphp".$pgid);                      // 고정 (절대 수정 불가)
	$inipay->SetField("subpgip","203.238.3.10");                    // 고정 (절대 수정 불가)
	$inipay->SetField("admin", $_SESSION['INI_ADMIN']);    // 키패스워드(상점아이디에 따라 변경)
	$inipay->SetField("debug", "true");                             // 로그모드("true"로 설정하면 상세로그가 생성됨.)
	$inipay->SetField("uid", $uid);                                 // INIpay User ID (절대 수정 불가)
	$inipay->SetField("goodname",  iconv("utf-8","euc-kr",$goodname) );                       // 상품명 
	$inipay->SetField("currency", $currency);                       // 화폐단위

	$inipay->SetField("mid", $_SESSION['INI_MID']);        // 상점아이디
	$inipay->SetField("rn", $_SESSION['INI_RN']);          // 웹페이지 위변조용 RN값
	$inipay->SetField("price", $_SESSION['INI_PRICE']);		// 가격
	$inipay->SetField("enctype", $_SESSION['INI_ENCTYPE']);// 고정 (절대 수정 불가)


     /*----------------------------------------------------------------------------------------
       price 등의 중요데이터는
       브라우저상의 위변조여부를 반드시 확인하셔야 합니다.

       결제 요청페이지에서 요청된 금액과
       실제 결제가 이루어질 금액을 반드시 비교하여 처리하십시오.

       설치 메뉴얼 2장의 결제 처리페이지 작성부분의 보안경고 부분을 확인하시기 바랍니다.
       적용참조문서: 이니시스홈페이지->가맹점기술지원자료실->기타자료실 의
                      '결제 처리 페이지 상에 결제 금액 변조 유무에 대한 체크' 문서를 참조하시기 바랍니다.
       예제)
       원 상품 가격 변수를 OriginalPrice 하고  원 가격 정보를 리턴하는 함수를 Return_OrgPrice()라 가정하면
       다음 같이 적용하여 원가격과 웹브라우저에서 Post되어 넘어온 가격을 비교 한다.

		$OriginalPrice = Return_OrgPrice();
		$PostPrice = $_SESSION['INI_PRICE']; 
		if ( $OriginalPrice != $PostPrice )
		{
			//결제 진행을 중단하고  금액 변경 가능성에 대한 메시지 출력 처리
			//처리 종료 
		}

      ----------------------------------------------------------------------------------------*/
	$inipay->SetField("buyername",  iconv("utf-8","euc-kr",$buyername));       // 구매자 명
	$inipay->SetField("buyertel",  $buyertel);        // 구매자 연락처(휴대폰 번호 또는 유선전화번호)
	$inipay->SetField("buyeremail",$buyeremail);      // 구매자 이메일 주소
	$inipay->SetField("paymethod", $paymethod);       // 지불방법 (절대 수정 불가)
	$inipay->SetField("encrypted", $encrypted);       // 암호문
	$inipay->SetField("sessionkey",$sessionkey);      // 암호문
	$inipay->SetField("url", "http://www.your_domain.co.kr"); // 실제 서비스되는 상점 SITE URL로 변경할것
	$inipay->SetField("cardcode", $cardcode);         // 카드코드 리턴
	$inipay->SetField("parentemail", $parentemail);   // 보호자 이메일 주소(핸드폰 , 전화결제시에 14세 미만의 고객이 결제하면  부모 이메일로 결제 내용통보 의무, 다른결제 수단 사용시에 삭제 가능)
	
	/*-----------------------------------------------------------------*
	 * 수취인 정보 *                                                   *
	 *-----------------------------------------------------------------*
	 * 실물배송을 하는 상점의 경우에 사용되는 필드들이며               *
	 * 아래의 값들은 INIsecurepay.html 페이지에서 포스트 되도록        *
	 * 필드를 만들어 주도록 하십시요.                                  *
	 * 컨텐츠 제공업체의 경우 삭제하셔도 무방합니다.                   *
	 *-----------------------------------------------------------------*/
	$inipay->SetField("recvname",$recvname);	// 수취인 명
	$inipay->SetField("recvtel",$recvtel);		// 수취인 연락처
	$inipay->SetField("recvaddr",$recvaddr);	// 수취인 주소
	$inipay->SetField("recvpostnum",$recvpostnum);  // 수취인 우편번호
	$inipay->SetField("recvmsg",$recvmsg);		// 전달 메세지

	$inipay->SetField("joincard",$joincard);  // 제휴카드코드
	$inipay->SetField("joinexpire",$joinexpire);    // 제휴카드유효기간
	$inipay->SetField("id_customer",$id_customer);    //user_id

	
	/****************
	 * 4. 지불 요청 *
	 ****************/
	$inipay->startAction();
	
	/****************************************************************************************************************
	 * 5. 결제  결과                  
	 *      												
	 *  1 모든 결제 수단에 공통되는 결제 결과 데이터                                                      		
	 * 	거래번호 : $inipay->GetResult('TID')                                       					
	 * 	결과코드 : $inipay->GetResult('ResultCode') ("00"이면 지불 성공)           				
	 * 	결과내용 : $inipay->GetResult('ResultMsg') (지불결과에 대한 설명)          			
	 * 	지불방법 : $inipay->GetResult('PayMethod') (매뉴얼 참조)  								
	 * 	상점주문번호 : $inipay->GetResult('MOID')										
	 *	결제완료금액 : $inipay->GetResult('TotPrice')							
	 *																					
	 * 결제 되는 금액 =>원상품가격과  결제결과금액과 비교하여 금액이 동일하지 않다면  
	 * 결제 금액의 위변조가 의심됨으로 정상적인 처리가 되지않도록 처리 바랍니다. (해당 거래 취소 처리) 
	 *													
	 *														
	 *  2. 신용카드,ISP,핸드폰, 전화 결제, 은행계좌이체, OK CASH BAG Point 결제 결과 데이터        			
	 *      (무통장입금 , 문화 상품권 포함) 								        
	 * 	이니시스 승인날짜 : $inipay->GetResult('ApplDate') (YYYYMMDD)
	 * 	이니시스 승인시각 : $inipay->GetResult('ApplTime') (HHMMSS)  
	 *  														
	 *  3. 신용카드 결제 결과 데이터 
         *												
	 * 	신용카드 승인번호 : $inipay->GetResult('ApplNum')                         				
	 * 	할부기간 : $inipay->GetResult('CARD_Quota')                                 				
	 * 	무이자할부 여부 : $inipay->GetResult('CARD_Interest') ("1"이면 무이자할부) 			
	 * 	신용카드사 코드 : $inipay->GetResult('CARD_Code') (매뉴얼 참조)             	
	 * 	카드발급사 코드 : $inipay->GetResult('CARD_BankCode') (매뉴얼 참조)       	
	 * 	본인인증 수행여부 : $inipay->GetResult('CARD_AuthType') ("00"이면 수행)      					
	 *      각종 이벤트 적용 여부 : $inipay->GetResult('EventCode')                    		
	 *	                                                                       
	 *      ** 달러결제 시 통화코드와  환률 정보 **                           
	 *	해당 통화코드 : $inipay->GetResult('OrgCurrency')                               
	 *	환율 : $inipay->GetResult('ExchangeRate')	                                    
	 *														
	 *      아래는 "신용카드 및 OK CASH BAG 복합결제" 또는"신용카드 지불시에 OK CASH BAG적립"시에 추가되는 데이터   
	 * 	OK Cashbag 적립 승인번호 : $inipay->GetResult('OCB_SaveApplNum')           					
	 * 	OK Cashbag 사용 승인번호 : $inipay->GetResult('OCB_PayApplNum')            				
	 * 	OK Cashbag 승인일시 : $inipay->GetResult('OCB_ApplDate') (YYYYMMDDHHMMSS)   		
	 * 	OCB 카드번호 : $inipay->GetResult('OCB_Num')			   						
	 * 	OK Cashbag 복합결재시 신용카드 지불금액 : $inipay->GetResult('CARD_ApplPrice')     	
	 * 	OK Cashbag 복합결재시 포인트 지불금액 : $inipay->GetResult('OCB_PayPrice')       	
	 *	                                                                                
	 * 4. 실시간 계좌이체 결제 결과 데이터                                               
	 *                                                                                 
	 * 	은행코드 : $inipay->GetResult('ACCT_BankCode')                                
	 *	현금영수증 발행결과코드 : $inipay->GetResult('CSHR_ResultCode')
	 *	현금영수증 발행구분코드 : $inipay->GetResult('CSHR_Type') 
	 *														*
	 * 5. OK CASH BAG 결제수단을 이용시에만  결제 결과 데이터		
	 * 	OK Cashbag 적립 승인번호 : $inipay->GetResult('OCB_SaveApplNum')           					
	 * 	OK Cashbag 사용 승인번호 : $inipay->GetResult('OCB_PayApplNum')            				
	 * 	OK Cashbag 승인일시 : $inipay->GetResult('OCB_ApplDate') (YYYYMMDDHHMMSS)   		
	 * 	OCB 카드번호 : $inipay->GetResult('OCB_Num')			   						
	 *														
         * 6. 무통장 입금 결제 결과 데이터							                        *
	 * 	가상계좌 채번에 사용된 주민번호 : $inipay->GetResult('VACT_RegNum')              					*
	 * 	가상계좌 번호 : $inipay->GetResult('VACT_Num')                                					*
	 * 	입금할 은행 코드 : $inipay->GetResult('VACT_BankCode')                           					*
	 * 	입금예정일 : $inipay->GetResult('VACT_Date') (YYYYMMDD)                      					*
	 * 	송금자 명 : $inipay->GetResult('VACT_InputName')                                  					*
	 * 	예금주 명 : $inipay->GetResult('VACT_Name')                                  					*
	 *														*	
	 * 7. 핸드폰, 전화 결제 결과 데이터( "실패 내역 자세히 보기"에서 필요 , 상점에서는 필요없는 정보임)             *
         * 	전화결제 사업자 코드 : $inipay->GetResult('HPP_GWCode')                        					*
	 *														*	
	 * 8. 핸드폰 결제 결과 데이터								                        *
	 * 	휴대폰 번호 : $inipay->GetResult('HPP_Num') (핸드폰 결제에 사용된 휴대폰번호)       					*
	 *														*
	 * 9. 전화 결제 결과 데이터								                        *
   * 	전화번호 : $inipay->GetResult('ARSB_Num') (전화결제에  사용된 전화번호)      						*
   * 														*		
   * 10. 문화 상품권 결제 결과 데이터							                        *
   * 	컬쳐 랜드 ID : $inipay->GetResult('CULT_UserID')	                           					*
   *														*
   * 11. K-merce 상품권 결제 결과 데이터 (K-merce ID, 틴캐시 아이디 공통사용)                                     *
   *      K-merce ID : $inipay->GetResult('CULT_UserID')                                                                       *
   *                                                                                                              *
   * 12. 모든 결제 수단에 대해 결제 실패시에만 결제 결과 데이터 							*
   * 	에러코드 : $inipay->GetResult('ResultErrorCode')                             					*
   * 														*
   * 13.현금영수증 발급 결과코드 (은행계좌이체시에만 리턴)							*
   *    $inipay->GetResult('CSHR_ResultCode')                                                                                     *
   *                                                                                                              *
   * 14.틴캐시 잔액 데이터                                							*
   *    $inipay->GetResult('TEEN_Remains')                                           	                                *
   *	틴캐시 ID : $inipay->GetResult('CULT_UserID')													*
   * 15.스마트 문상							*
   *	사용 카드 갯수 : $inipay->GetResult('GAMG_Cnt')                 					        *
   *														*
   ****************************************************************************************************************/
         
	/*******************************************************************
	 * 7. DB연동 실패 시 강제취소                                      *
	 *                                                                 *
	 * 지불 결과를 DB 등에 저장하거나 기타 작업을 수행하다가 실패하는  *
	 * 경우, 아래의 코드를 참조하여 이미 지불된 거래를 취소하는 코드를 *
	 * 작성합니다.                                                     *
	 *******************************************************************/


	$cancelFlag = "false";

	// $cancelFlag를 "ture"로 변경하는 condition 판단은 개별적으로
	// 수행하여 주십시오.

	function insertOrder( $p_connection, $p_order ){
		$sql = "insert into order_list(	
				orderDate, orderTime, userId, userName, pmtMethod,
				orderAddr1,	pictureAddr, price,	pictureType, color, amount,	hp,
				orderAddr2,	expressMemo, tid, orderStatus

			) values(
				'".$p_order["orderDate"]."',
				'".$p_order["orderTime"]."',
				'".$p_order["userId"]."', 
				'".$p_order["userName"]."', 
				'".$p_order["pmtMethod"]."',
				'".$p_order["orderAddr1"]."',
				'".$p_order["pictureAddr"]."',
				'".$p_order["price"]."',
				'".$p_order["pictureType"]."',
				'".$p_order["color"]."',
				'".$p_order["amount"]."',
				'".$p_order["hp"]."',
				'".$p_order["orderAddr2"]."',
				'".$p_order["expressMemo"]."',
				'".$p_order["tid"]."',
				'".$p_order["orderStatus"]."'
			)";
		
		$result = mysqli_query($p_connection, $sql);

		return $result;
	}

	$result = "";
	if( $inipay->GetResult('ResultCode') == "00" 
		&& $inipay->GetResult('PayMethod') != "VBank" ){
		
		$order = array( );
		
		$order["orderDate"]	= $inipay->GetResult('ApplDate');		
		$order["orderTime"]	= $inipay->GetResult('ApplTime');		
		$order["userId"]	= $_SESSION['user_id'];		
		$order["userName"]	= $_POST['userName'];		
		$order["pmtMethod"]	= $inipay->GetResult('PayMethod');		
		$order["orderAddr1"]	= $_POST['orderAddr1'];		
		$order["pictureAddr"]	= $_POST['pictureAddr'];		
		$order["price"]	= $inipay->GetResult('TotPrice');		
		$order["pictureType"]	= $_POST['pictureType'];		
		$order["color"]	= $_POST['color'];		
		$order["amount"]	= $_POST['amount'];		
		$order["hp"]	= $_POST['hp'];		
		$order["orderAddr2"]	= $_POST['orderAddr2'];		
		$order["expressMemo"]	= $_POST['expressMemo'];		
		$order["tid"]	= $inipay->GetResult('TID');		
		$order["orderStatus"]	= "결제완료";		

		$result = insertOrder( $connect, $order );

/*
		    if (!$result) {
		        printf("Error: %s\n", mysqli_error($connect));
		        exit();
		    }

*/
		    if( $result == 1){
		    	$sql_delete = "delete from order_tmp where userId='".$_SESSION['user_id']."'";
				$result2 = mysqli_query($connect, $sql_delete);								
		    }
			
			//echo "<meta http-equiv='refresh' content='0;url=myPage.php'>";

	} else {
		$order = array( );
		
		$order["orderDate"]	= $inipay->GetResult('ApplDate');		
		$order["orderTime"]	= $inipay->GetResult('ApplTime');		
		$order["userId"]	= $_SESSION['user_id'];		
		$order["userName"]	= $_POST['userName'];		
		$order["pmtMethod"]	= $inipay->GetResult('PayMethod');		
		$order["orderAddr1"]	= $_POST['orderAddr1'];		
		$order["pictureAddr"]	= $_POST['pictureAddr'];		
		$order["price"]	= $inipay->GetResult('TotPrice');		
		$order["pictureType"]	= $_POST['pictureType'];		
		$order["color"]	= $_POST['color'];		
		$order["amount"]	= $_POST['amount'];		
		$order["hp"]	= $_POST['hp'];		
		$order["orderAddr2"]	= $_POST['orderAddr2'];		
		$order["expressMemo"]	= $_POST['expressMemo'];		
		$order["tid"]	= $inipay->GetResult('TID');		
		$order["orderStatus"]	= "결제대기";		

		$result = insertOrder( $connect, $order );


	    if( $result == 1){
	    	$sql_delete = "delete from order_tmp where userId='".$_SESSION['user_id']."'";
			$result2 = mysqli_query($connect, $sql_delete);								
	    }
	}

	if($result != 1){
		$cancelFlag = "true";	
	}

	if($cancelFlag == "true")
	{
		$TID = $inipay->GetResult("TID");
		$inipay->SetField("type", "cancel"); // 고정
		$inipay->SetField("tid", $TID); // 고정
		$inipay->SetField("cancelmsg", "DB FAIL"); // 취소사유
		$inipay->startAction();
		if($inipay->GetResult('ResultCode') == "00")
		{
      $inipay->MakeTXErrMsg(MERCHANT_DB_ERR,"Merchant DB FAIL");
		}
	}
?>


<!-------------------------------------------------------------------------------------------------------
 *        												
 *	아래 내용은 결제 결과에 대한 출력 페이지 샘플입니다. 
 *	
 -------------------------------------------------------------------------------------------------------->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<title>마이클레어</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="X-UA-Compatible" content="requiresActiveX=true" />


<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
<link href="../css/reset.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
<link href="../css/style_order.css" rel="stylesheet" />


<script>
	//var openwin=window.open("childwin.html","childwin","width=299,height=149");
	//openwin.close();
	
	function show_receipt(tid) // 영수증 출력
	{
		if("<?php echo ($inipay->GetResult('ResultCode')); ?>" == "00")
		{
			var receiptUrl = "https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid=" + "<?php echo($inipay->GetResult('TID')); ?>" + "&noMethod=1";
			//window.open(receiptUrl,"receipt","width=430,height=700");
		}
		else
		{
			alert("해당하는 결제내역이 없습니다");
		}
	}
		
	function errhelp() // 상세 에러내역 출력
	{
		var errhelpUrl = "http://www.inicis.com/ErrCode/Error.jsp?result_err_code=" + "<?php echo($inipay->GetResult('ResultErrorCode')); ?>" + "&mid=" + "<?php echo($inipay->GetResult('MID')); ?>" + "&tid=<?php echo($inipay->GetResult('TID')); ?>" + "&goodname=" + "<?php echo($inipay->GetResult('GoodName')); ?>" + "&price=" + "<?php echo($inipay->GetResult('TotPrice')); ?>" + "&paymethod=" + "<?php echo($inipay->GetResult('PayMethod')); ?>" + "&buyername=" + "<?php echo($inipay->GetResult('BuyerName')); ?>" + "&buyertel=" + "<?php echo($inipay->GetResult('BuyerTel')); ?>" + "&buyeremail=" + "<?php echo($inipay->GetResult('BuyerEmail')); ?>" + "&codegw=" + "<?php echo($inipay->GetResult('HPP_GWCode')); ?>";
		window.open(errhelpUrl,"errhelp","width=520,height=150, scrollbars=yes,resizable=yes");
	}
	
</script>

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);
//-->
</script>

<script>
    //optional need to be loaded before angular-file-upload-shim(.min).js
    FileAPI = {
        //only one of jsPath or jsUrl.
        jsUrl: '../js/FileAPI.min.js',

        //only one of staticPath or flashUrl.
        
        flashUrl: '../js/FileAPI.flash.swf',

        //forceLoad: true, html5: false //to debug flash in HTML5 browsers
    }
</script>
<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="./app_root.js"></script>
<script type="text/javascript" src="./app_orderProc2.js"></script>
<script type="text/javascript" src="../controller/orderResult.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>



</head>
<body ng-controller="OrderResultController">
<div id="wrap">
    <? include "../inc/header.php" ?>        

    <div class="cont">
    	
    	<div class="row clearfix" >
        	<div class="guide_01">
            	<img src="../images/main/04_process04.jpg" style="width:1097px; text-align:center" /> 
        	</div>

        	
        	<input type="hidden" ng-init="resultStatus.resultCode='<? echo $inipay->GetResult('ResultCode'); ?>'" />
			<input type="hidden" ng-init="resultStatus.payMethod='<? echo $inipay->GetResult('PayMethod'); ?>'" />
			<!--결과내용 -->
			<input type="hidden" ng-init="resultStatus.resultMsg='<?php echo iconv('euc-kr','utf-8',$inipay->GetResult('ResultMsg')); ?>'" />

			<!--거래번호-->
			<input type="hidden" ng-init="resultStatus.TID='<?php echo $inipay->GetResult('TID'); ?>'"/>
			
			<!--결제완료금액-->
			<input type="hidden" ng-init="resultStatus.totPrice='<?php echo $inipay->GetResult('TotPrice'); ?>'"/>  


        	
			<div ng-init="resultStatus.resultCode = '00'"></div>
			<div ng-init="resultStatus.payMethod = 'VBank'"></div>
        	

        	<div ng-show="resultStatus.resultCode == '00' && resultStatus.payMethod == 'Card'">
        		<div class='order_done'></div>	
        	</div>

        	<div ng-show="resultStatus.resultCode == '00' && resultStatus.payMethod == 'VCard'">
        		<div class='order_done'></div>
        	</div>

        	<div ng-show="resultStatus.resultCode == '00' && resultStatus.payMethod == 'DirectBank'">
        		<div class='order_done'></div>
        	</div>

        	<div ng-show="resultStatus.resultCode == '00' && resultStatus.payMethod == 'VBank'">
        		<div class="vbank_msg">
        			<img src="../images/main/04_order_accept.jpg" />
        			
        		</div>
        		<div class="result_ok">
	        		계좌번호       : <?php echo $inipay->GetResult('VACT_Num'); ?>  <br/>
					은행           : {{bankCode["<?php echo $inipay->GetResult('VACT_BankCode'); ?>"]}}  <br/>
					예금주 명      : 에이치앤드컴퍼니 <br/>
					송금자 명      : <?php echo iconv('euc-kr','utf-8', $inipay->GetResult('VACT_InputName') ); ?>  <br/>
					송금 예정 일자 : <?php echo $inipay->GetResult('VACT_Date'); ?>  <br/>
				</div>
				<div class="vbank_info">
					<img src="../images/main/04_order_accept_02.jpg" />
					
				</div>

        	</div>

        	<div ng-show="resultStatus.resultCode == '01'" class='order_cancel'>
        		<img class='order_cancel_image' src='../images/main/04_order_cancel.jpg' />
        	</div>


        	<div class="result_btn_box">
        		<div class="result_btnbox_inner">
	        		<button class="btn_common" onclick="location.href = '../main/main.php';" >확인</button>
	            	<button class="btn_common" onclick="location.href = '../shop2/myPage.php';" >마이페이지</button>
	            </div>
            </div>



<?php                    
	/*-------------------------------------------------------------------------------------------------------
	 *																			*
	 *  아래 부분은 결제 수단별 결과 메세지 출력 부분입니다. 					*	
	 *																			*
	 *  1.  신용카드 , ISP 결제 결과 출력 (OK CASH BAG POINT 복합 결제 내역 )	*
	 -------------------------------------------------------------------------------------------------------*/

	if($inipay->GetResult('PayMethod') == "Card" || $inipay->GetResult('PayMethod') == "VCard" ){
		
		echo "	
				<!--	
				<tr> 
        		  <td width='18' align='center'></td>
        		  <td width='109' height='25'>신용카드번호</td>
        		  <td width='343'>".$inipay->GetResult('CARD_Num')."****</td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
				<tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>승 인 날 짜</td>
                  <td width='343'>".$inipay->GetResult('ApplDate')."</td>
                </tr>
                <tr> 
                  <td height='1' colspan='3' align='center' ></td>
                </tr>
                <tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>승 인 시 각</td>
                  <td width='343'>".$inipay->GetResult('ApplTime')."</td>
                </tr>                	    
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
        		<tr> 
        		  <td width='18' align='center'></td>
        		  <td width='109' height='25'>승 인 번 호</td>
        		  <td width='343'>".$inipay->GetResult('ApplNum')."</td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
        		<tr> 
        		  <td width='18' align='center'></td>
        		  <td width='109' height='25'>할 부 기 간</td>
        		  <td width='343'>".$inipay->GetResult('CARD_Quota')."개월&nbsp;<b><font color=red>".$interest."</font></b></td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
        		<tr> 
        		  <td width='18' align='center'></td>
        		  <td width='109' height='25'>카 드 종 류</td>
        		  <td width='343'>".$inipay->GetResult('CARD_Code')."</td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
        		<tr> 
        		  <td width='18' align='center'></td>
        		  <td width='109' height='25'>카드발급사</td>
        		  <td width='343'>".$inipay->GetResult('CARD_BankCode')."</td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>
        		<tr> 
        		  <td height='1' colspan='3'>&nbsp;</td>
        		</tr>
        		<tr> 
    		  <td style='padding:0 0 0 9' colspan='3'><img src='img/icon.gif' width='10' height='11'> 
          	  </td>
    		</tr>
        		<tr> 
        		  <td height='1' colspan='3' align='center' ></td>
        		</tr>                    		
        		<tr> 
        		  <td height='1' colspan='3'>&nbsp;</td>
        		</tr>
        		<tr> 
    		  <td style='padding:0 0 0 9' colspan='3'><img src='img/icon.gif' width='10' height='11'> 
          	  </td>
    		</tr>
    		-->
    		";
                    
          }
        
        /*-------------------------------------------------------------------------------------------------------
	 *													*
	 *  아래 부분은 결제 수단별 결과 메세지 출력 부분입니다.    						*	
	 *													*
	 *  2.  은행계좌결제 결과 출력 										*
	 -------------------------------------------------------------------------------------------------------*/
	 
          else if($inipay->GetResult('PayMethod') == "DirectBank"){
          	
          	echo"		
          			<tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>승 인 날 짜</td>
                  <td width='343'>".$inipay->GetResult('ApplDate')."</td>
                </tr>
                <tr> 
                  <td height='1' colspan='3' align='center' ></td>
                </tr>
                <tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>승 인 시 각</td>
                  <td width='343'>".$inipay->GetResult('ApplTime')."</td>
                </tr>
                <tr> 
                  <td height='1' colspan='3' align='center' ></td>
                </tr>
                <tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>은 행 코 드</td>
                  <td width='343'>".$inipay->GetResult('ACCT_BankCode')."</td>
                </tr>
                <tr> 
                  <td height='1' colspan='3' align='center' ></td>
                </tr>
                <tr> 
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>현금영수증<br>발급결과코드</td>
                  <td width='343'>".$inipay->GetResult('CSHR_ResultCode')."</td>
                </tr>
                <tr> 
                  <td height='1' colspan='3' align='center' ></td>
                </tr>
				<tr>
                  <td width='18' align='center'></td>
                  <td width='109' height='25'>현금영수증<br>발급구분코드</td>
                  <td width='343'>".$inipay->GetResult('CSHR_Type')." <font color=red><b>(0 - 소득공제용, 1 - 지출증빙용)</b></font></td>
                </tr>
                <tr>
                  <td height='1' colspan='3' align='center' ></td>
                </tr>";
          }
          
        /*-------------------------------------------------------------------------------------------------------
	 *													*
	 *  아래 부분은 결제 수단별 결과 메세지 출력 부분입니다.    						*	
	 *													*
	 *  3.  무통장입금 입금 예정 결과 출력 (결제 성공이 아닌 입금 예정 성공 유무)				*
	 -------------------------------------------------------------------------------------------------------*/
	 
          else if($inipay->GetResult('PayMethod') == "VBank"){
          	
          	
          }
         
?>
                  </table></td>
              </tr>
            </table>


            
        </div>

        <? include "../inc/footer.php" ?>
	</div>
	<!-- /wrap -->
</div>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 

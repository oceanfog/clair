angular.module('orderProc2')
.controller('adminController', ['$scope','$http', '$upload', 'factory',  function($scope, $http, $upload, factory){
	$scope.orderList = [];
	$scope.userList = [];
	$scope.goodsList = [];

	$scope.bar = {"loaded":0, "total":0};

	$scope.orderStatus = [
		{value:"결제대기", displayName:'결제대기'},
		{value:"결제완료", displayName:'결제완료'},
		{value:"주문취소요청", displayName:"주문취소요청"},
		{value:"주문취소완료", displayName:"주문취소완료"},
		{value:"시안작업중", displayName:'시안작업중'},
		{value:"시안작업완료", displayName:'시안작업완료'},
		{value:"시안확정", displayName:"시안확정"},
		{value:"시안수정요청", displayName:"시안수정요청"},
		{value:"배송중", displayName:'배송중'}
    ];

	$scope.init = function(){
		
	};


	$scope.manageUserPageOption = {
		"editable":false
	};

	$scope.goodsItem = {
		"dprice":"",
		"sang_name":""
	};

	$scope.statusAddGoods = "hide";
	$scope.uploadedPictureName = "";

	$scope.list = [{"id":"krk"},{"id":"krk2"}];
	$scope.order = order;

	$scope.setDpItem = function( p_sangNo ){

 		$http({
	      method: 'GET',
	      url: "../restful/config.php/select/" + p_sangNo,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response.result1 )
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    }

	$scope.getUserList = function(){

		data = 'searchCondition='+JSON.stringify(searchCondition);
		//console.log(data);
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		$http({
	      method: 'GET',
	      url: '../restful/user.php/user/',
	      data: data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	        $scope.userList = response.result;
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });

	};


	$scope.getGoodsList = function(){

		data = 'searchCondition='+JSON.stringify(searchCondition);
		//console.log(data);
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		$http({
	      method: 'GET',
	      url: '../restful/goods.php/',
	      data: data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	        $scope.goodsList = response.result;
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });

	    
	};


	$scope.getOrderList = function(){

		data = 'searchCondition='+JSON.stringify(searchCondition);
		//console.log(data);
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		$http({
	      method: 'GET',
	      url: '../restful/order.php/order/',
	      data: data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	        $scope.orderList = response.result;
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });

	    
	};

	//call deleteGoodsItem
    $scope.deleteGoodsItem = function( p_no ){

    	$http({
	      method: 'GET',
	      url: '../restful/goods.php/delete_item/'+p_no,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	
	    	//console.log( response );
	    	$scope.getGoodsList();
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };

    $scope.updateOrderItem = function( p_orderItem ){
    	var data = 'orderItem='+JSON.stringify( p_orderItem );

    	$http({
		      method: 'POST',
		      url: '../restful/order.php/update',
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        //console.log( response );
	        	//$scope.getOrderTemp();
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

    };

    $scope.getAskModifyList = function(){
    	factory.getRestfulResults('../restful/ask_modify.php/').
	    success(function(response) {
	    	//console.log( response );
	    	$scope.askModifyList = response.list;
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
    };


/***************************************************
*	 button Listener
****************************************************/
	$scope.toggleEditMode = function(){
		 $scope.manageUserPageOption.editable = !$scope.manageUserPageOption.editable;
	};

	$scope.changeModifyMode = function( p_memNo){
		alert( p_memNo );
	};

	$scope.callAddGoods = function(){
		window.open("./addGoods.html", "","width=600, height=600");
	};

	$scope.addGoods = function(){
		$http({
		      method: 'POST',
		      url: '../restful/goods.php/put',
		      data: 'goodsItem='+JSON.stringify( $scope.goodsItem),
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		    	//console.log( $scope.goodsItem );
		        //console.log( response );

		        if( response.insertGoodsItemResult == 1 ){
					$scope.getGoodsList();
					$scope.statusAddGoods = "hide";
					$scope.goodsItem={};
		        }
		        
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });
	};

	$scope.callManageUser = function(){
		window.open("./manageUser.html", "","width=1200, height=600");
	};

	$scope.callManageGoods = function(){
		window.open("./manageGoods.html", "","width=1200, height=600");
	};


    $scope.changeOrderStatus = function(p_orderItem){
    	var r = confirm(p_orderItem["orderStatus"] + "(으)로 수정 하시겠습니까?");

    	if( r ){
    		//console.log( p_tempOrderItem );
    		$scope.updateOrderItem( p_orderItem );
    	} else {
    		alert("수정하지 않습니다.");
    		$scope.getOrderList();
    	}
    	

    };



/*************************
*	Event Listener
*
**************************/

	$scope.updateSelectedGoods = function( p_sangNo){
		alert("updated :" + p_sangNo.sang_no);
		//console.log( p_sangNo );

		$scope.setDpItem(p_sangNo.sang_no);
	};

	$scope.updatePrice = function(){
		alert("hello");
		data = 'price='+$scope.order.price;
		//console.log(data);
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		$http({
	      method: 'GET',
	      url: './sessionProcess.php/',
	      data: data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    }).
	    success(function(response) {
	    	//console.log( response );
	        
	    }).
	    error(function(response) {
	        $scope.codeStatus = response || "Request failed";
	    });
	};

	

	//when you select file then the file uploaded to server
	$scope.onFileSelect = function($files, p_order) {
	    //$files: an array of files selected, each file has name, size, and type.

	      var file = $files[0];

	      factory.fileUpload('http://myclair.kr/shop2/fileUploadAdmin.php', p_order, file)
	      .progress(function(evt) {
	        //console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
	        $scope.bar.loaded = parseInt( 100.0 * evt.loaded / evt.total );
	        $scope.bar.total = evt.total;
	      }).success(function(data, status, headers, config) {
	        // file is uploaded successfully
	        //console.log(data);
	        
	        $scope.uploadResult = data;
	        $scope.uploadedPictureName = data.name;
	        $scope.getOrderList();
	      });
	    
	};


}]);



var searchCondition = {
	userId:""
}

var order = {
	"price":""
}
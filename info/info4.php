<?php
    session_start();
    
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="orderProc2" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" >
<meta name="keywords" content="공기청정기, 나만의 공기청정기">
<title>디자인엿보기</title>

<script type="text/javascript" src="../js/angular-file-upload-shim.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.min.js"></script>

<script type="text/javascript" src="../shop2/app_root.js"></script>
<script type="text/javascript" src="../shop2/app_orderProc2.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<link href="../css/reset.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />

<link href="../css/bootstrap_custom.css" rel="stylesheet" > 

<style>


</style>

<?php include_once("../anal/analyticstracking.php"); ?>
</head>


<body ng-controller="OrderProc2Controller">

<div class="container">
    <input type="hidden" value="{{topMenuStyle.01['color'] = '#ff8500' }}">
    <input type="hidden" value="{{topMenuStyle.02['color'] = '' }}">
    <input type="hidden" value="{{topMenuStyle.03['color'] = '' }}">
    <input type="hidden" value="{{topMenuStyle.04['color'] = '' }}">
        

    <!-- header -->
    <? include "../inc/header2.php" ?>
    <!-- // header -->
        
    

    <div class="visible-xs visible-sm visible-md visible-lg" >
        
        
        <img src="../images/info/m_info_01.jpg" class="img-responsive" />
        <div class="inner_title">마이클레어란?</div>
        
    </div>


    <div class="pdt_intro col-xs-12 col-lg-12">
        <div class="inner">
            <dl>
                <dt class="title">
                    
                </dt>
                <dd class="txt1">

                    <span style="font-weight: bold;">원하는 사진, 그림으로</span> 디자인 하여 제작되는 <span style="font-weight: bold;">맞춤형</span> 클레어 공기청정기 입니다.
                </dd>
            </dl>
        </div>
    </div>

    

    
    <div class="pdt_intro2 col-xs-12">
        <div class="inner">
            <dl class="sub">
                <dt>
                    특별한 추억을 담은 최고의 선물!
                </dt>
                <dd class="txt2">
                    가족 친구 이웃에게 특별한 추억이 담긴 세상 단 하나뿐인 공기청정기를 선물하세요.
                </dd>
            <dl>

            <dl >
                <dt>
                    인테리어 포인트 가구로도 강력추천!
                </dt>
                <dd class="txt3">
                    원하는 사진으로 직접 디자인 가능한 마이클레어로 방안 인테리어에 포인트를 주세요. 
                </dd>
            <dl>
        </dl>        
    </div>
    


 </div>   
 <? include "../inc/footer.php" ?>
    
    <script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script> <script type="text/javascript"> if(!wcs_add) var wcs_add = {}; wcs_add["wa"] = "c05970264893c8"; wcs_do(); </script>   

</body>
</html>
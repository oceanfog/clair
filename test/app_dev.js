(function(){
	var app = angular.module('dev', []);
	
	app.filter('krw', [ '$filter', '$locale',function(filter, locale) {
	    var currencyFilter = filter('currency');
	    var formats = locale.NUMBER_FORMATS;
	    return function(amount, currencySymbol) {
	      var value = currencyFilter(amount, "");
	      var sep = value.indexOf(formats.DECIMAL_SEP);
	      if(amount >= 0) { 
	        return value.substring(0, sep);
	      }
	      return value.substring(0, sep) + ')';
	    };
	  } ]);

	app.controller('DevController', ['$scope','$http', function($scope, $http){
		this.product = airCleaner;
		$scope.product = airCleaner;
		$scope.searchCondition = searchCondition;
		$scope.cartList = "";

		$scope.getPriceSum = function(){
			var sum = 0;
		 	for( i = 0 ; i < $scope.cartList.length; i++){
		 		
		 		sum = sum + parseInt( $scope.cartList[i].price );
		 		
		 	}
		 	return sum;
		}

		

		$scope.getCart = function(){

			data = 'searchCondition='+JSON.stringify(searchCondition);
			console.log(data);
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			$http({
		      method: 'GET',
		      url: '../restful/cart.php/cart/'+$scope.searchCondition.userId,
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		    	console.log( response );
		        $scope.cartList = response.cartList;
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

		    
		};

		$scope.deleteCartItem = function( p_no ){
	    	

	    	$http({
		      method: 'GET',
		      url: '../restful/cart.php/delete_item/'+p_no,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		    	console.log( response );
		    	$scope.getCart();
		        
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });
	    };

		$scope.pay2 = function(frm){
			console.log( frm );
			if(document.ini.clickcontrol.value == "enable")
			{
				if(document.ini.goodname.value == "")  // 필수항목 체크 (상품명, 상품가격, 구매자명, 구매자 이메일주소, 구매자 전화번호)
				{
					alert("상품명이 빠졌습니다. 필수항목입니다.");
					return false;
				}
				else if(document.ini.buyername.value == "")
				{
					alert("구매자명이 빠졌습니다. 필수항목입니다.");
					return false;
				} 
				else if(document.ini.buyeremail.value == "")
				{
					alert("구매자 이메일주소가 빠졌습니다. 필수항목입니다.");
					return false;
				}
				else if(document.ini.buyertel.value == "")
				{
					alert("구매자 전화번호가 빠졌습니다. 필수항목입니다.");
					return false;
				}
				else if( ( navigator.userAgent.indexOf("MSIE") >= 0 || navigator.appName == 'Microsoft Internet Explorer' ) &&  (document.INIpay == null || document.INIpay.object == null) )  // 플러그인 설치유무 체크
				{
					alert("\n이니페이 플러그인 128이 설치되지 않았습니다. \n\n안전한 결제를 위하여 이니페이 플러그인 128의 설치가 필요합니다. \n\n다시 설치하시려면 Ctrl + F5키를 누르시거나 메뉴의 [보기/새로고침]을 선택하여 주십시오.");
					return false;
				}
				else
				{
					
					/******
					 * 플러그인이 참조하는 각종 결제옵션을 이곳에서 수행할 수 있습니다.
					 * (자바스크립트를 이용한 동적 옵션처리)
					 */
					
								 
					if (MakePayMessage( $("#ini")[0] ) )
					{
						disable_click();

						$http({
						        method  : 'POST',
						        url     : 'INIsecureresult.php',
						        data    : $("#ini")[0],  // pass in data as strings
						        headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
						    })
						        .success(function(data) {
						            console.log(data);

						            if (!data.success) {
						            	// if not successful, bind errors to error variables
						                $scope.errorName = data.errors.name;
						                $scope.errorSuperhero = data.errors.superheroAlias;
						            } else {
						            	// if successful, bind success message to message
						                $scope.message = data.message;
						            }
						        });

						return true;
					}
					else
					{
						if( IsPluginModule() )     //plugin타입 체크
		   				{
							alert("결제를 취소하셨습니다.");
						}
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		};

	}]);
})();


var colors = [
	{name:'green', shade:'green'},
    {name:'orange', shade:'orange'},
]

var airCleaner = {
	name:"",
	color:colors[0].name,
	type:"",
	price:2.95,
	colors: colors,
	soldOut:true
}

var searchCondition = {
	userId:""
}
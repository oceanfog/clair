





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html ng-app="login" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

</head>
<body>


<?php
include("../master/db_connect.inc");
include("../master/JSON.php");




function getUserInfo($p_connect){

	$sql="select * from member";
	$sql = iconv('utf8', 'euckr', $sql);
	$result = mysqli_query($p_connect, $sql);

	if (!$result) {
	    printf("Error: %s\n", mysqli_error($p_connect));
	    exit();
	}

	return $result;
}

function printIntoJson( $p_fetchArray )
{	
	echo json_encode( $p_fetchArray, JSON_UNESCAPED_UNICODE );
}





$userInfo = getUserInfo( $connect );




if(mysqli_num_rows($userInfo)){
    echo '{"testData":[';

    $first = true;
    $row=mysqli_fetch_assoc($userInfo);
    while($row=mysqli_fetch_row($userInfo)){
        //  cast results to specific data types

        if($first) {
            $first = false;
        } else {
            echo ',';
        }
        echo json_encode($row);
    }
    echo ']}';
} else {
    echo '[]';
}




?>

</body>

</html>
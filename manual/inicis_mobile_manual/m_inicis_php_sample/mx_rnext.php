<?php
	require("../libs/INImx.php");
	
	$inimx = new INImx;


	/////////////////////////////////////////////////////////////////////////////
	///// 1. 변수 초기화 및 POST 인증값 받음                                 ////
	/////////////////////////////////////////////////////////////////////////////
	
	$inimx->reqtype 		= "PAY";  //결제요청방식
	$inimx->inipayhome 	= "/home/ts/INIpay50/"; //로그기록 경로 (이 위치의 하위폴더에 log폴더 생성 후 log폴더에 대해 777 권한 설정)
	$inimx->status			= $P_STATUS;
	$inimx->rmesg1			= $P_RMESG1;
	$inimx->tid		= $P_TID;
	$inimx->req_url		= $P_REQ_URL;
	$inimx->noti		= $P_NOTI;
	
	
	/////////////////////////////////////////////////////////////////////////////
	///// 2. 상점 아이디 설정 :                                              ////
	/////    결제요청 페이지에서 사용한 MID값과 동일하게 세팅해야 함...      ////
	/////    인증TID를 잘라서 사용가능 : substr($P_TID,'10','10');           ////
	/////////////////////////////////////////////////////////////////////////////
	$inimx->id_merchant = substr($P_TID,'10','10');  //
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////
	///// 3. 인증결과 확인 :                                                 ////
	/////    인증값을 가지고 성공/실패에 따라 처리 방법                      ////
	/////////////////////////////////////////////////////////////////////////////
  if($inimx->status =="00")   // 모바일 인증이 성공시
  {


	/////////////////////////////////////////////////////////////////////////////
	///// 4. 승인요청 :                                                      ////
	/////    인증성공시  P_REQ_URL로 승인요청을 함...                        ////
	/////////////////////////////////////////////////////////////////////////////
	  $inimx->startAction();  // 승인요청
	  
	  
	  
	  $inimx->getResult();  //승인결과 파싱, P_REQ_URL에서 내려준 결과값 파싱 
	  
	 
	 switch($inimx->m_payMethod)
	 {   
    
    case(CARD):  //신용카드 안심클릭
    
    
	   echo("승인결과코드:".$inimx->m_resultCode."<br>");
		 echo("결과메시지:".$inimx->m_resultMsg."<br>");
		 echo("지불수단:".$inimx->m_payMethod."<br>");
		 echo("주문번호:".$inimx->m_moid."<br>");
		 echo("TID:".$inimx->m_tid."<br>");
		 echo("승인금액:".$inimx->m_resultprice."<br>");
		 echo("승인일:".$inimx->m_pgAuthDate."<br>");
		 echo("승인시각:".$inimx->m_pgAuthTime."<br>");
		 echo("상점ID:".$inimx->m_mid."<br>");
		 echo("구매자명:".$inimx->m_buyerName."<br>");
		 echo("P_NOTI:".$inimx->m_noti."<br>");
		 echo("NEXT_URL:".$inimx->m_nextUrl."<br>");
		 echo("NOTI_URL:".$inimx->m_notiUrl."<br>");
     echo("승인번호:".$inimx->m_authCode."<br>");
		 echo("할부개월:".$inimx->m_cardQuota."<br>");
		 echo("카드코드:".$inimx->m_cardCode."<br>");
		 echo("발급사코드:".$inimx->m_cardIssuerCode."<br>");
		 echo("카드번호:".$inimx->m_cardNumber."<br>");
		 echo("가맹점번호:".$inimx->m_cardMember."<br>");
		 echo("매입사코드:".$inimx->m_cardpurchase."<br>");
		 echo("부분취소가능여부(0:불가, 1:가능):".$inimx->m_prtc."<br>");

		
		break;
		
	  case(MOBILE):  //휴대폰결제
    
	   echo("승인결과코드:".$inimx->m_resultCode."<br>");
		 echo("결과메시지:".$inimx->m_resultMsg."<br>");
		 echo("지불수단:".$inimx->m_payMethod."<br>");
		 echo("주문번호:".$inimx->m_moid."<br>");
		 echo("TID:".$inimx->m_tid."<br>");
		 echo("승인금액:".$inimx->m_resultprice."<br>");
		 echo("승인일:".$inimx->m_pgAuthDate."<br>");
		 echo("승인시각:".$inimx->m_pgAuthTime."<br>");
		 echo("상점ID:".$inimx->m_mid."<br>");
		 echo("구매자명:".$inimx->m_buyerName."<br>");
		 echo("P_NOTI:".$inimx->m_noti."<br>");
		 echo("NEXT_URL:".$inimx->m_nextUrl."<br>");
		 echo("NOTI_URL:".$inimx->m_notiUrl."<br>");
     echo("통신사:".$inimx->m_codegw."<br>");
		
		break;
		
		case(VBANK):  //가상계좌
    
	   echo("승인결과코드:".$inimx->m_resultCode."<br>");
		 echo("결과메시지:".$inimx->m_resultMsg."<br>");
		 echo("지불수단:".$inimx->m_payMethod."<br>");
		 echo("주문번호:".$inimx->m_moid."<br>");
		 echo("TID:".$inimx->m_tid."<br>");
		 echo("승인금액:".$inimx->m_resultprice."<br>");
		 echo("요청일:".$inimx->m_pgAuthDate."<br>");
		 echo("요청시각:".$inimx->m_pgAuthTime."<br>");
		 echo("상점ID:".$inimx->m_mid."<br>");
		 echo("구매자명:".$inimx->m_buyerName."<br>");
		 echo("P_NOTI:".$inimx->m_noti."<br>");
		 echo("NEXT_URL:".$inimx->m_nextUrl."<br>");
		 echo("NOTI_URL:".$inimx->m_notiUrl."<br>");
		 echo("가상계좌번호:".$inimx->m_vacct."<br>");
		 echo("입금예정일:".$inimx->m_dtinput."<br>");
		 echo("입금예정시각:".$inimx->m_tminput."<br>");
		 echo("예금주:".$inimx->m_nmvacct."<br>");
		 echo("은행코드:".$inimx->m_vcdbank."<br>");

		break;
		
		default: //문화상품권,해피머니

     echo("승인결과코드:".$inimx->m_resultCode."<br>");
		 echo("결과메시지:".$inimx->m_resultMsg."<br>");
		 echo("지불수단:".$inimx->m_payMethod."<br>");
		 echo("주문번호:".$inimx->m_moid."<br>");
		 echo("TID:".$inimx->m_tid."<br>");
		 echo("승인금액:".$inimx->m_resultprice."<br>");
		 echo("승인일:".$inimx->m_pgAuthDate."<br>");
		 echo("승인시각:".$inimx->m_pgAuthTime."<br>");
		 echo("상점ID:".$inimx->m_mid."<br>");
		 echo("구매자명:".$inimx->m_buyerName."<br>");
		 echo("P_NOTI:".$inimx->m_noti."<br>");
		 echo("NEXT_URL:".$inimx->m_nextUrl."<br>");
		 echo("NOTI_URL:".$inimx->m_notiUrl."<br>");
	  }
	
	}
	else                      // 모바일 인증 실패
	{
	  echo("인증결과코드:".$inimx->status);
	  echo("<br>");
	  echo("인증결과메시지:".$inimx->rmesg1);
	}
	  
  
?>

<?php
    //**************************************************************************************************************
    //NICE평가정보 Copyright(c) KOREA INFOMATION SERVICE INC. ALL RIGHTS RESERVED
    
    //서비스명 :  체크플러스 - 안심본인인증 서비스
    //페이지명 :  체크플러스 - 결과 페이지
    
    //보안을 위해 제공해드리는 샘플페이지는 서비스 적용 후 서버에서 삭제해 주시기 바랍니다. 
    //**************************************************************************************************************
    
	session_start();
	
    $sitecode = "G7341";                // NICE로부터 부여받은 사이트 코드
    $sitepasswd = "LDRKEOVJJA0J";           // NICE로부터 부여받은 사이트 패스워드
    
    $cb_encode_path = "/www/myclair_kr/CheckPlusSafe_PHP/module/Linux/64bit/CPClient";        // NICE로부터 받은 암호화 프로그램의 위치 (절대경로+모듈명)
		
    $enc_data = $_POST["EncodeData"];		// 암호화된 결과 데이타
    $sReserved1 = $_POST['param_r1'];		
	$sReserved2 = $_POST['param_r2'];
	$sReserved3 = $_POST['param_r3'];

		//////////////////////////////////////////////// 문자열 점검///////////////////////////////////////////////
    if(preg_match('~[^0-9a-zA-Z+/=]~', $enc_data, $match)) {echo "입력 값 확인이 필요합니다 : ".$match[0]; exit;} // 문자열 점검 추가. 
    if(base64_encode(base64_decode($enc_data))!=$enc_data) {echo "입력 값 확인이 필요합니다"; exit;}
    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReserved1, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReserved2, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
    if(preg_match("/[#\&\\+\-%@=\/\\\:;,\.\'\"\^`~\_|\!\/\?\*$#<>()\[\]\{\}]/i", $sReserved3, $match)) {echo "문자열 점검 : ".$match[0]; exit;}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
    if ($enc_data != "") {

        $plaindata = `$cb_encode_path DEC $sitecode $sitepasswd $enc_data`;		// 암호화된 결과 데이터의 복호화
        //echo "[plaindata]  " . $plaindata . "<br>";

        if ($plaindata == -1){
            $returnMsg  = "암/복호화 시스템 오류";
        }else if ($plaindata == -4){
            $returnMsg  = "복호화 처리 오류";
        }else if ($plaindata == -5){
            $returnMsg  = "HASH값 불일치 - 복호화 데이터는 리턴됨";
        }else if ($plaindata == -6){
            $returnMsg  = "복호화 데이터 오류";
        }else if ($plaindata == -9){
            $returnMsg  = "입력값 오류";
        }else if ($plaindata == -12){
            $returnMsg  = "사이트 비밀번호 오류";
        }else{
            // 복호화가 정상적일 경우 데이터를 파싱합니다.
            $ciphertime = `$cb_encode_path CTS $sitecode $sitepasswd $enc_data`;	// 암호화된 결과 데이터 검증 (복호화한 시간획득)
        
            $requestnumber = GetValue($plaindata , "REQ_SEQ");
            $responsenumber = GetValue($plaindata , "RES_SEQ");
            $authtype = GetValue($plaindata , "AUTH_TYPE");
            $name = GetValue($plaindata , "NAME");
            //$name = GetValue($plaindata , "UF8_NAME");
            $birthdate = GetValue($plaindata , "BIRTHDATE");
            $gender = GetValue($plaindata , "GENDER");
            $nationalinfo = GetValue($plaindata , "NATIONALINFO");	//내/외국인정보(사용자 매뉴얼 참조)
            $dupinfo = GetValue($plaindata , "DI");
            $conninfo = GetValue($plaindata , "CI");

            
            //인증 결과 session저장
            $_SESSION["certification"] = "true";
            $_SESSION["birthdate"] = $birthdate;
            $_SESSION["gender"] = $gender;
            $_SESSION["name"] = $name;

            if(strcmp($_SESSION["REQ_SEQ"], $requestnumber) != 0)
            {
            	
                //echo "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.<br>";
                $requestnumber = "";
                $responsenumber = "";
                $authtype = "";
                $name = "";
            		$birthdate = "";
            		$gender = "";
            		$nationalinfo = "";
            		$dupinfo = "";
            		$conninfo = "";
            }
        }
    }
?>

<?
    function GetValue($str , $name)
    {
        $pos1 = 0;  //length의 시작 위치
        $pos2 = 0;  //:의 위치

        while( $pos1 <= strlen($str) )
        {
            $pos2 = strpos( $str , ":" , $pos1);
            $len = substr($str , $pos1 , $pos2 - $pos1);
            $key = substr($str , $pos2 + 1 , (int)$len);


            $pos1 = $pos2 + $len + 1;
            if( $key == $name )
            {
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $value = substr($str , $pos2 + 1 , $len);
                return $value;
            }
            else
            {
                // 다르면 스킵한다.
                $pos2 = strpos( $str , ":" , $pos1);
                $len = substr($str , $pos1 , $pos2 - $pos1);
                $pos1 = $pos2 + $len + 1;
            }            
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="X-UA-Compatible" content="requiresActiveX=true" />
    <title>NICE평가정보 - 본인인증</title>
    <style>
        body{
            background: url("../../images/common/certi_ok.png") no-repeat;
                  
        }
        #div_wrapper{
            width: 300px;
            height: 20px;
            padding-top: 98px;
        }

        #bu_okay{
            posistion: absolute;
            
            background: #ff8400;
            color: #fff;
            
            height: 20px;
            line-height: 20px;
            margin-left: 13px;
            text-align: center;

            padding: 1px 10px 1px 10px;
            margin: 0px 0px 0px 126px;
            text-decoration: blink;
        }

    </style>
    <script>
        javascript:window.resizeTo(345,213);
        alert("본인 인증이 완료 되었습니다.");
        window.close();
    </script>
</head>
<body >
    <div id="div_wrapper">
        <!--
        <p><p><p><p>
        본인인증이 완료 되었습니다.<br>
        <table border=1 background="../../images/common/certi_ok.png">
            <tr>
                <td>복호화한 시간</td>
                <td><?= $ciphertime ?> (YYMMDDHHMMSS)</td>
            </tr>
            <tr>
                <td>요청 번호</td>
                <td><?= $requestnumber ?></td>
            </tr>            
            <tr>
                <td>나신평응답 번호</td>
                <td><?= $responsenumber ?></td>
            </tr>            
            <tr>
                <td>인증수단</td>
                <td><?= $authtype ?></td>
            </tr>
                    <tr>
                <td>성명</td>
                <td><?= $name ?></td>
            </tr>
                    <tr>
                <td>생년월일</td>
                <td><?= $birthdate ?></td>
            </tr>
                    <tr>
                <td>성별</td>
                <td><?= $gender ?></td>
            </tr>
                    <tr>
                <td>내/외국인정보</td>
                <td><?= $nationalinfo ?></td>
            </tr>
                    <tr>
                <td>DI(64 byte)</td>
                <td><?= $dupinfo ?></td>
            </tr>
                    <tr>
                <td>CI(88 byte)</td>
                <td><?= $conninfo ?></td>
            </tr>
            <tr>
              <td>RESERVED1</td>
              <td><?= $sReserved1 ?></td>
    	      </tr>
    	      <tr>
    	          <td>RESERVED2</td>
    	          <td><?= $sReserved2 ?></td>
    	      </tr>
    	      <tr>
    	          <td>RESERVED3</td>
    	          <td><?= $sReserved3 ?></td>
    	      </tr>
        </table>
        -->
        창이 자동으로 닫히지 않으시면 수동으로 닫아주세요. 감사합니다.<br>
        <a href="#" id="bu_okay" onclick="window.close()">확인</a>
    </div>
    <script>
       
    </script>    
</body>
</html>

<?php

function getPdoObject(){
	return new PDO('mysql:host=db.myclair.kr;
		dbname=dbmyclair', 'myclair', 'myclair8702', 
		array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}



function insertOrder( $p_connection, $p_order ){
		$sql = "insert into order_list(	
				orderDate, orderTime, userId, userName, pmtMethod,
				orderAddr1,	pictureAddr, price,	pictureType, color, amount,	hp,
				orderAddr2,	expressMemo, tid, orderStatus

			) values(
				'".$p_order["orderDate"]."',
				'".$p_order["orderTime"]."',
				'".$p_order["userId"]."', 
				'".$p_order["userName"]."', 
				'".$p_order["pmtMethod"]."',
				'".$p_order["orderAddr1"]."',
				'".$p_order["pictureAddr"]."',
				'".$p_order["price"]."',
				'".$p_order["pictureType"]."',
				'".$p_order["color"]."',
				'".$p_order["amount"]."',
				'".$p_order["hp"]."',
				'".$p_order["orderAddr2"]."',
				'".$p_order["expressMemo"]."',
				'".$p_order["tid"]."',
				'".$p_order["orderStatus"]."'
			)";
		
		$result = mysqli_query($p_connection, $sql);

		return $result;
	}

function insertOrder_new( $p_order ){

	$dbh = getPdoObject();

	

	$stmt = $dbh->prepare( 'insert into order_list(
		orderDate, orderTime, userId, userName, pmtMethod,
		orderAddr1,	pictureAddr, price,	pictureType, color, amount,	hp,
		orderAddr2,	expressMemo, tid, orderStatus
		) values (
		:orderDate, :orderTime, :userId, :userName, :pmtMethod,
		:orderAddr1,	:pictureAddr, :price,	:pictureType, :color, :amount,	:hp,
		:orderAddr2,	:expressMemo, :tid, 	:orderStatus )' );
	$stmt->bindValue(':orderDate', $p_order["orderDate"], PDO::PARAM_STR);
	$stmt->bindValue(':orderTime', $p_order["orderTime"], PDO::PARAM_STR);
	$stmt->bindValue(':userId', $p_order["userId"], PDO::PARAM_STR);
	$stmt->bindValue(':userName', iconv('EUC-KR', 'UTF-8',$p_order["userName"]), PDO::PARAM_STR);
	$stmt->bindValue(':pmtMethod', $p_order["pmtMethod"], PDO::PARAM_STR);
	$stmt->bindValue(':orderAddr1', iconv('EUC-KR', 'UTF-8', $p_order["orderAddr1"]), PDO::PARAM_STR);
	$stmt->bindValue(':pictureAddr', $p_order["pictureAddr"], PDO::PARAM_STR);
	$stmt->bindValue(':price', $p_order["price"], PDO::PARAM_STR);
	$stmt->bindValue(':pictureType', iconv('EUC-KR', 'UTF-8', $p_order["pictureType"]), PDO::PARAM_STR);
	$stmt->bindValue(':color', $p_order["color"], PDO::PARAM_STR);
	$stmt->bindValue(':amount', $p_order["amount"], PDO::PARAM_STR);
	$stmt->bindValue(':hp', $p_order["hp"], PDO::PARAM_STR);
	$stmt->bindValue(':orderAddr2', iconv('EUC-KR', 'UTF-8', $p_order["orderAddr2"]), PDO::PARAM_STR);
	$stmt->bindValue(':expressMemo', iconv('EUC-KR', 'UTF-8',$p_order["expressMemo"]), PDO::PARAM_STR);
	$stmt->bindValue(':tid', $p_order["tid"], PDO::PARAM_STR);
	$stmt->bindValue(':orderStatus', iconv('EUC-KR', 'UTF-8', $p_order["orderStatus"]), PDO::PARAM_STR);
	
	$result = $stmt->execute();
	
	
	$resultArray = array( );

	$resultArray["result"] = $result;

/*	$resultArray["errorCode"] = $stmt->errorCode();
	$resultArray["errorInfo"] = $stmt->errorInfo();
*/

	return json_encode( $resultArray );


}



?>

angular.module('orderProc2')
 
.factory('factory', ['$http', '$upload', function($http, $upload){
	var obj = {};

	obj.loginUserInfo = {
		"userId":""
	};

 	obj.getPriceSum = function( p_cartList ){
		var sum = 0;
	 	for( i = 0 ; i < p_cartList.length; i++){
	 		sum = sum + parseInt( p_cartList[i].price );	 		
	 	}
	 	return sum;
	};

	obj.getRestfulResults = function( p_url ){
		return $http({
	      method: 'GET',
	      url: p_url,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.getRestfulResult = function( p_url, p_parameter ){
		return $http({
	      method: 'GET',
	      url: p_url + p_parameter,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.postRestfulResult = function( p_url, p_data ){
		return $http({
			method:'POST',
			url: p_url,
			data: p_data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}

		});

	};

	obj.putRestfulResult = function( p_url, p_data ){
		return $http({
	      method: 'POST',
	      url: p_url,
	      data: p_data,
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    });
	};

	obj.fileUpload = function(p_url, p_order, p_file){
		return $upload.upload({
	        url: p_url, //upload.php script, node.js route, or servlet url
	        data: {myObj: p_order},
	        file: p_file,
	      })
	};

	obj.showImageOrignSize = function(){
		//image, location of print
		alert("imageorg");

	};
	

	return obj;
}]);



angular.module('orderProc2')
.controller('mainController', ['$scope','$http', '$upload', 'factory', function($scope, $http, $upload, factory){
	
	$scope.modalShown = false;
  	$scope.toggleModal = function() {
  		
    	$scope.modalShown = !$scope.modalShown;
  	};



	

/*--------------------------------------------------
-- code for test
----------------------------------------------------*/
	$scope.test_order = function(){
		var test_tempOrder = {
			"userId":"oceanfog",
	    	"totalPrice":"178000",
	    	"discountAmount":"",
	    	"finalPrice":"178000",
	    	"pictureName":"463834047_584.jpg",
	    	"pictureType":"picture",
	    	"color":"green",
	    	"amount":"1",
	    	"price":"178000",
	    	"hp":"010-3588-6265",
	    	"addr1":"서울시 관악구 오피스",
	    	"addr2":"3동 402호",
	    	"agree":"ok"
		};

		
		var data = 'tmpOrderData='+JSON.stringify( test_tempOrder );
		factory.putRestfulResult( '../restful/order_tmp.php/put' , data )
		.success(function(response) {
			//console.log( response )	;
			if( response.insertTmpOrderResult){
				moveLocation();				


			}else{
				alert("tempOrder가 저장되지 않았습니다.");
			}
		});

		
		


	};





}]);




var cartItem = {
	color:"그린",
	type:"사진형",
	amount:"1",
	phrase:"",
	price:"",
	pictureName:"",
	userId:""

}


var validationPictureInputForm = function( p_cartItem ){
	
	/***************************************************
	**	모든 조건을 만족하면 true를 하나라도 만족하지 않으면 false를 return한다.
	**	receive cartItem object and check whether the value exisist nor nothing
	*************************************************/

	if( p_cartItem.pictureName == "" ){
		alert("사진을 올려주세요.");
		return false;
	}

	if( p_cartItem.agree != true ){
		alert("안내문을 읽었습니다에 체크 해주세요.");
		return false;
	}
}


var validationCallOrder = function(){

}


var MobileCheck = function(){
	var MobileVersi = navigator.userAgent.toLowerCase();
	var MobileArray = new Array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone");
	var checkCount  = 0;
	for(i=0; i<MobileArray.length; i++){ if(MobileVersi.indexOf(MobileArray[i]) != -1){ 
		checkCount++; break; }
	}
	
	return (checkCount >= 1) ? "Mobile" : "Computer";
}


function openDaumPostcode( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            
            p_scope.address.zip1 = data.postcode1;
            p_scope.address.zip2 = data.postcode2;
            p_scope.address.orderAddr1 = data.address;

            document.getElementById('zip1').value = data.postcode1;
            document.getElementById('zip2').value = data.postcode2;
            document.getElementById('orderAddr1').value = data.address;

            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;

           
        }
    }).open();
}


function openDaumPostcode2( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            p_scope.mobileOrderModel.zip1 = data.postcode1;
            p_scope.mobileOrderModel.zip2 = data.postcode2;
            p_scope.mobileOrderModel.orderAddr1 = data.address;

            document.getElementById('zip1').value = data.postcode1;
            document.getElementById('zip2').value = data.postcode2;
            document.getElementById('orderAddr1').value = data.address;

            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;

            
            
        },
        width : '100%',
        height : '100%'
    }).embed(element);

        // iframe을 넣은 element를 보이게 한다.
    element.style.display = 'block';
}


var moveLocation = function(){
	if( MobileCheck() == "Computer"){
		location.href = "./m_inicis.html";
		//location.href = "./order.php";
	} else {
		location.href = "./m_inicis.html";
	}
}
<?php
include("../Dao/pdoObject.php");

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->post('/update/', 'updateOrderTemp');
$app->get('/', 'getAskModifyList');
$app->post('/test/', 'echoInputValue');
$app->run();


function getAskModifyList(){
	$dbh = getPdoObject();

	$stmt = $dbh->prepare( "SELECT * FROM ask_modify" );
	$result = $stmt->execute();
	$list = $stmt->fetchAll();
	
	$resultArray = array( );

	$resultArray["result"] = $result;
	$resultArray["list"] = $list;
	$resultArray["requestJsonData"] = json_encode($tmpOrderData);

	echo json_encode( $resultArray );
}

function echoInputValue(){
	global $app;
  	$jsondata = $app->request->params("askModifyArticle");
  	$tmpOrderData = json_decode( $jsondata, true );

	$dbh = getPdoObject();

	$stmt = $dbh->prepare( "INSERT INTO ask_modify (userId, content, orderNo) VALUES (:userId, :content, :orderNo)" );
	
	$stmt->bindValue(':orderNo', $tmpOrderData["orderNo"], PDO::PARAM_STR);
	$stmt->bindValue(':userId', $tmpOrderData["userId"], PDO::PARAM_STR);
	$stmt->bindValue(':content', $tmpOrderData["content"], PDO::PARAM_STR);

	$result = $stmt->execute();

	if( $result ){
		$stmt = $dbh->prepare( "UPDATE order_list SET orderStatus = '시안수정요청' WHERE no = :orderNo" );	
		$stmt->bindValue(':orderNo', $tmpOrderData["orderNo"], PDO::PARAM_STR);
		$result2 = $stmt->execute();
	}

	
	$resultArray = array( );

	$resultArray["result"] = $result;
	$resultArray["result2"] = $result2;
	$resultArray["requestJsonData"] = json_encode($tmpOrderData);
//	$resultArray["errorInfo"] = $stmt->errorInfo();
//	$resultArray["errorCode"] = $stmt->errorCode();

	echo json_encode( $resultArray );
}

function getOrder($id){	
	$dbh = getPdoObject();
	$sql = 'SELECT * FROM order_tmp WHERE userId = :id';
	$stmt = $dbh->prepare( $sql );
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	$list = $stmt->fetchAll();

	echo "{\"result\":".json_encode( $list ).","
	."\"id\":\"".$id."\","
	."\"sql\":\"".$sql."\"}";

}

function updateOrderTemp(){
	global $app;
  	$jsondata = $app->request->params("tmpOrderItem");
  	$tmpOrderData = json_decode( $jsondata, true );


	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_tmp SET pictureType = :type WHERE userId=:id' );
	$stmt->bindValue(':type', $tmpOrderData["pictureType"], PDO::PARAM_STR);
	$stmt->bindValue(':id', $tmpOrderData["userId"], PDO::PARAM_STR);

	$result = $stmt->execute();
	
	
	$resultArray = array( );

	$resultArray["result"] = $result;
	$resultArray["requestJsonData"] = json_encode($tmpOrderData);
	$resultArray["errorInfo"] = $stmt->errorInfo();
	$resultArray["errorCode"] = $stmt->errorCode();


	echo json_encode( $resultArray );

	//print_r( $tmpOrderData );


}







?>
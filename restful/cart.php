<?php
include("../master/db_connect.inc");
include("../master/JSON.php");

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();


$app = new \Slim\Slim();
$app->get('/cart/', 'getCart');
$app->get('/cart/:id', 'getCartId');
$app->get('/delete_item/:no', 'deleteCartItem');
$app->post('/put/', 'putCart');

$app->get('/delete/:id', 'deleteCartId');
$app->run();


function getConnection(){
	$connect=mysqli_connect('db.myclair.kr','myclair','myclair8702') or die ('not connect to server');
	mysqli_select_db($connect, 'dbmyclair') or die('select db error');
	mysqli_query($connect, 'set names utf8');

	return $connect;
}

function selectCart( $p_conn, $p_sql){
	
	$sql = $p_sql;
	$result = mysqli_query($p_conn, $sql);
	$number = mysqli_num_rows($result);
	$i = 1;

	if (!$result) {
	    printf("Error: %s\n", mysqli_error($p_conn));
	    exit();
	}


	echo "{\"cartList\":[";
	while($row = mysqli_fetch_array($result)){
	    echo json_encode( $row );

		if( $i<$number ){
			echo "," ;
		}
	    
	    $i++;
	}
	echo "],\"input\":[";
	echo "";
	echo "]}";
}

function getCart(){
	$connect=getConnection();

	global $app;
  	$requestParameter = $app->request->params("searchCondition");
  	$searchCondition = json_decode( $requestParameter, true );

  	$sql="select * from cart";

  	selectCart( $connect, $sql);
	
}

function putCart(){
	global $app;
  	$user = $app->request->params("cartItem");
  	$productInfo = json_decode( $user, true );
  	
  	//db에 insert
  	$result = insertCart( getConnection(), $productInfo);


	echo "{"."\"insertCartResult\":".$result.","
		."\"hello\":"."\"$productInfo\"".""
	."}";
		
}


function insertCart( $p_connect, $p_cartItem){
	$sql="insert into cart (
		userId,
		color,
		type,
		amount,
		phrase,
		price,
		pictureName
		)
		values(
			'".$p_cartItem['userId']."', 
			'".$p_cartItem['color']."', 
			'".$p_cartItem['type']."', 
			'".$p_cartItem['amount']."', 
			'".$p_cartItem['phrase']."', 
			'".$p_cartItem['price']."', 
			'".$p_cartItem['pictureName']."')";


	
	$result = mysqli_query($p_connect, $sql);

	return $result;

}


function deleteCartId($p_id){

	$connect=getConnection();

	$sql = "delete from cart where userId ='".$p_id."'";

	$result = mysqli_query($connect, $sql);


	echo "{"."\"deleteCartResult\":\"$result\"".","
		."\"sql\":\"$sql\""."}";
}


function getCartId($p_id){
	$connect=getConnection();

	$sql = "select * from cart where userId ='".$p_id."'";

	selectCart( $connect, $sql);

}

function deleteCartItem($p_no){
	$connect=getConnection();

	$sql = "delete from cart where no ='".$p_no."'";

	$result = mysqli_query($connect, $sql);


	echo "{"."\"deleteCartItemResult\":\"$result\"".","
		."\"sql\":\"$sql\""."}";
}



?>
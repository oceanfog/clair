<?php
include("../Dao/pdoObject.php");

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->get('/order/:id', 'getOrder');
$app->post('/update/', 'updateOrderTemp');
$app->get('/delete/:id', 'deleteOrderTemp');
$app->get('/order/', 'getOrderTempAll');
$app->post('/put/', 'putTmpOrder');

$app->get('/test/', 'printPdoHello');

$app->run();





function printPdoHello(){
	$dbh = getPdoObject();
	$stmt = $dbh->prepare('SELECT * FROM member');
	$stmt->execute();
	$list = $stmt->fetchAll();
	
	echo "{\"result\":".json_encode( $list )."}";

}



function getConnection(){
	$connect=mysqli_connect('db.myclair.kr','myclair','myclair8702') or die ('not connect to server');
	mysqli_select_db($connect, 'dbmyclair') or die('select db error');
	mysqli_query($connect, 'set names utf8');

	return $connect;
}



function getOrder($id){
	
	$dbh = getPdoObject();
	$sql = 'SELECT * FROM order_tmp WHERE userId = :id';
	$stmt = $dbh->prepare( $sql );
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	$list = $stmt->fetchAll();

	echo "{\"result\":".json_encode( $list ).","
	."\"id\":\"".$id."\","
	."\"sql\":\"".$sql."\"}";

}

function getOrderTempAll(){
	$dbh = getPdoObject();
	$stmt = $dbh->prepare('SELECT * FROM order_tmp');
	$stmt->execute();
	$list = $stmt->fetchAll();
	

	echo "{\"result\":".json_encode( $list )."}";
}

function deleteOrderTemp( $p_id ){

	$dbh = getPdoObject();
	$sql = 'DELETE FROM order_tmp WHERE userId = :id';
	$stmt = $dbh->prepare( $sql );
	$stmt->bindParam(':id', $p_id);
	$stmt->execute();
	$list = $stmt->fetch();

	echo "{\"result\":".json_encode( $list ).","
	."\"id\":\"".$id."\","
	."\"sql\":\"".$sql."\"}";

}

function updateOrderTemp(){
	global $app;
  	$jsondata = $app->request->params("tmpOrderItem");
  	$tmpOrderData = json_decode( $jsondata, true );


	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_tmp SET pictureType = :type WHERE userId=:id' );
	$stmt->bindValue(':type', $tmpOrderData["pictureType"], PDO::PARAM_STR);
	$stmt->bindValue(':id', $tmpOrderData["userId"], PDO::PARAM_STR);

	$result = $stmt->execute();
	
	
	$resultArray = array( );

	$resultArray["result"] = $result;
	$resultArray["requestJsonData"] = json_encode($tmpOrderData);
	$resultArray["errorInfo"] = $stmt->errorInfo();
	$resultArray["errorCode"] = $stmt->errorCode();


	echo json_encode( $resultArray );

	//print_r( $tmpOrderData );


}


function resultSql( $p_sql ){
	$connect=getConnection();

	if( $p_sql != ""){
		$result = mysqli_query($connect, $p_sql);
		$number = mysqli_num_rows($result);
		$i = 1;

		if (!$result) {
		    printf("Error: %s\n", mysqli_error($connect));
		    exit();
		}

		echo "{\"result\":[";
		while($row = mysqli_fetch_array($result)){
		    echo json_encode( $row );

			if( $i<$number ){
				echo "," ;
			}
		    
		    $i++;
		}
		echo "]}";
	} else {
		echo "{\"result\":\"error\"}";
	}
}


function putTmpOrder(){
	global $app;
  	$jsondata = $app->request->params("tmpOrderData");
  	$orderInfo = json_decode( $jsondata, true );
  	
  	$connection = getConnection();


	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'DELETE FROM order_tmp WHERE userId=:id' );
	$stmt->bindValue(':id', $orderInfo["userId"], PDO::PARAM_STR);
	$result1 = $stmt->execute();


	$stmt2 = $dbh->prepare(
	 'INSERT INTO order_tmp ( userId, pictureName,	pictureType, color, amount,	price )
	 	 VALUES( :userId, :pictureName, :pictureType, :color, :amount, :totalPrice )'
	  );

	$stmt2->bindValue ( ':userId', $orderInfo['userId'], PDO::PARAM_STR );
	$stmt2->bindValue ( ':pictureName', $orderInfo['pictureName'], PDO::PARAM_STR );
	$stmt2->bindValue ( ':pictureType', $orderInfo['pictureType'], PDO::PARAM_STR );
	$stmt2->bindValue ( ':color', $orderInfo['color'], PDO::PARAM_STR );
	$stmt2->bindValue ( ':amount', $orderInfo['amount'], PDO::PARAM_INT );
	$stmt2->bindValue ( ':totalPrice', $orderInfo['totalPrice'], PDO::PARAM_INT );

	$result2 = $stmt2->execute();

	//$stmt2->debugDumpParams();

		
	$resultArray = array( );
	$resultArray["deleteOk"] = $result1;
	$resultArray["insertTmpOrderResult"] = $result2;
	$resultArray["input"] = $jsondata;


	echo json_encode( $resultArray );
		
}




?>
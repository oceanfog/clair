<?php
include("../Dao/pdoObject.php");

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->get('/order/:id', 'getOrder');
$app->get('/askcancel/:no', 'updateAskCancel');
$app->get('/order/', 'getOrderAll');
$app->post('/update/', 'updateOrderItem');
$app->get('/update/test/:no', 'updatePictureAddr');

$app->post('/put/', 'putOrder');
$app->run();




function updatePictureAddr( $no ){
	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_list SET orderStatus = :orderStatus WHERE no=:no' );
	$stmt->bindValue(':orderStatus', "시안확정", PDO::PARAM_STR);
	$stmt->bindValue(':no', $no, PDO::PARAM_STR);

	$result = $stmt->execute();



	$returnArray = array();

	$returnArray["testOk"] = "true";
	$returnArray["parameterNo"] = $no;
	$resultArray["result"] = $result;

	echo json_encode( $returnArray );
}


function updateAskCancel( $no ){
	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_list SET orderStatus = :orderStatus WHERE no=:no' );
	$stmt->bindValue(':orderStatus', "주문취소요청", PDO::PARAM_STR);
	$stmt->bindValue(':no', $no, PDO::PARAM_STR);

	$result = $stmt->execute();



	$returnArray = array();

	$returnArray["testOk"] = "true";
	$returnArray["parameterNo"] = $no;
	$resultArray["result"] = $result;

	echo json_encode( $returnArray );
}



function getConnection(){
	$connect=mysqli_connect('db.myclair.kr','myclair','myclair8702') or die ('not connect to server');
	mysqli_select_db($connect, 'dbmyclair') or die('select db error');
	mysqli_query($connect, 'set names utf8');

	return $connect;
}



function updateOrderItem(){
	global $app;
  	$jsondata = $app->request->params("orderItem");
  	$orderData = json_decode( $jsondata, true );


	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE order_list SET orderStatus = :orderStatus WHERE no=:no' );
	$stmt->bindValue(':no', intval($orderData["no"]), PDO::PARAM_INT);
	$stmt->bindValue(':orderStatus', $orderData["orderStatus"], PDO::PARAM_STR);

	$result = $stmt->execute();
	
	//$stmt->debugDumpParams();
	
	$resultArray = array( );

	$resultArray["result"] = $result;
	$resultArray["requestJsonData"] = json_encode($orderData);
	$resultArray["errorInfo"] = $stmt->errorInfo();
	$resultArray["errorCode"] = $stmt->errorCode();


	echo json_encode( $resultArray );

	//print_r( $tmpOrderData );


}


function getOrder($id){
	$connect=getConnection();

	$sql="select * from order_list where userId='".$id."' order by no desc";
	resultSql( $sql );

/*
	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'SELECT * from order_list where userId= :userId' );
	$stmt->bindValue(':userId', $id, PDO::PARAM_STR);
	$result = $stmt->execute();
	$list = $stmt->fetchAll();

	$resultArray = array( );
	$resultArray["result"] = $list;
*/

}

function getOrderAll(){
	$connect=getConnection();

	$sql="select * from order_list";
	resultSql( $sql );


}



function resultSql( $p_sql ){
	$connect=getConnection();

	if( $p_sql != ""){
		$result = mysqli_query($connect, $p_sql);
		$number = mysqli_num_rows($result);
		$i = 1;

		if (!$result) {
		    printf("Error: %s\n", mysqli_error($connect));
		    exit();
		}

		echo "{\"result\":[";
		while($row = mysqli_fetch_array($result)){
		    echo json_encode( $row );

			if( $i<$number ){
				echo "," ;
			}
		    
		    $i++;
		}
		echo "]}";
	} else {
		echo "{\"result\":\"error\"}";
	}
}


function putOrder(){
	global $app;
  	$user = $app->request->params("orderData");
  	$orderInfo = json_decode( $user, true );
  	
  	//db에 insert
  	$result = insertCart( getConnection(), $orderInfo);


	echo "{\"insertOrderResult\":$result}";
		
}


function insertCart( $p_connect, $p_cartItem){
	$sql="insert into cart (
		userId,
		color,
		type,
		amount,
		phrase,
		price,
		pictureAddr
		)
		values(
			'".$p_cartItem['userId']."', 
			'".$p_cartItem['color']."', 
			'".$p_cartItem['type']."', 
			'".$p_cartItem['amount']."', 
			'".$p_cartItem['phrase']."', 
			'".$p_cartItem['price']."', 
			'".$p_cartItem['pictureLocation']."')";


	
	$result = mysqli_query($p_connect, $sql);

	return $result;

}



?>
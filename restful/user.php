<?php
include("../Dao/pdoObject.php");

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();


$app = new \Slim\Slim();
$app->get('/user/', 'getUserAll');
$app->get('/user/:id', 'getUser');
$app->get('/duplChk/:id', 'resultDuplChk');
$app->post('/findid/', 'findId');
$app->post('/findpassword/', 'findPassword');
$app->post('/update/', 'updateUserInfo');

$app->run();



function pdoContextWithStatementStrategy(   ){
	//void

}

function getConnection(){
	$connect=mysqli_connect('db.myclair.kr','myclair','myclair8702') or die ('not connect to server');
	mysqli_select_db($connect, 'dbmyclair') or die('select db error');
	mysqli_query($connect, 'set names utf8');

	return $connect;
}



function resultDuplChk($p_id){
	//중복 검사 기능
	/*********************************
		id를 받아서 db조회를 한다.
		중복되는게 있으면 '1'을
		중복되는게 없으면 '0' 를 출력 한다.
	**********************************/
	$connect=getConnection();

	$sql="select count(*) from member where mem_id='".$p_id."'";
	
	$result = mysqli_query($connect, $sql);
	//$resultArray = mysqli_fetch_array($result);
	if (!$result) {
	    printf("Error: %s\n", mysqli_error($connect));
	    exit();
	}

	
	$fetchAssoc = mysqli_fetch_assoc( $result );

	$count = $fetchAssoc["count(*)"];

	echo "{\"resultDuplChk\":\"$count\",".
			"\"input\":\"$p_id\",".
			"\"sql\":\"$sql\"".
		"}";

}

function getUserAll(){
	$connect=getConnection();

	$sql="select * from member";
	resultSql( $sql );

}


function getUser($id){
	$dbh = getPdoObject();
	$sql = 'SELECT mem_id, kmem_name1, hp, email, zip1, zip2, addr1, addr2 FROM member WHERE mem_id = :id';
	$stmt = $dbh->prepare( $sql );
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	$list = $stmt->fetch();

	$resultArray = array();
	$resultArray["functionName"] = "getUser";
	$resultArray["result"] = $list;
	$resultArray["id"] = $id;
	
	echo json_encode( $resultArray );

}


function updateUserInfo(){
	global $app;
  	$jsondata = $app->request->params("userInfo");
  	$userInfo = json_decode( $jsondata, true );


  	$dbh = getPdoObject();
	$stmt = $dbh->prepare( 'UPDATE member SET email=:email, hp=:hp, zip1 = :zip1, zip2=:zip2, addr1 = :addr1, addr2 = :addr2 WHERE mem_id=:id' );
	$stmt->bindValue(':id', $userInfo["mem_id"], PDO::PARAM_STR);
	$stmt->bindValue(':email', $userInfo["email"], PDO::PARAM_STR);
	$stmt->bindValue(':hp', $userInfo["hp"], PDO::PARAM_STR);
	$stmt->bindValue(':zip1', $userInfo["zip1"], PDO::PARAM_STR);
	$stmt->bindValue(':zip2', $userInfo["zip2"], PDO::PARAM_STR);
	$stmt->bindValue(':addr1', $userInfo["addr1"], PDO::PARAM_STR);
	$stmt->bindValue(':addr2', $userInfo["addr2"], PDO::PARAM_STR);

	$result = $stmt->execute();
	
	
	$resultArray = array( );

	$resultArray["updateUserInfoResult"] = $result;
	$resultArray["requestJsonData"] = json_encode($userInfo);
	$resultArray["errorInfo"] = $stmt->errorInfo();
	$resultArray["errorCode"] = $stmt->errorCode();


	echo json_encode( $resultArray );


}


function resultSql( $p_sql ){
	
	$connect=getConnection();

	if( $p_sql != ""){
		$result = mysqli_query($connect, $p_sql);
		$number = mysqli_num_rows($result);
		$i = 1;

		if (!$result) {
		    printf("Error: %s\n", mysqli_error($connect));
		    exit();
		}

		echo "{\"result\":[";
		while($row = mysqli_fetch_array($result)){
		    echo json_encode( $row );

			if( $i<$number ){
				echo "," ;
			}
		    
		    $i++;
		}
		echo "]}";
	} else {
		echo "{\"result\":\"error\"}";
	}
}



function findId(){

	global $app;
  	$jsondata = $app->request->params("findIdInfo");
  	$findIdInfo = json_decode( $jsondata, true );

	$dbh = getPdoObject();
	$stmt = $dbh->prepare('SELECT kmem_name1, mem_id, email FROM member where kmem_name1=:name and email=:email');
	$stmt->bindValue(':email', $findIdInfo["email"], PDO::PARAM_STR);
	$stmt->bindValue(':name', $findIdInfo["name"], PDO::PARAM_STR);

	$sqlSuccess = $stmt->execute();
	$list = $stmt->fetch();
	$rowCount = $stmt->rowCount();


	if( $rowCount > 0  ){
		sendFindIdEmail( $list["mem_id"], $list["email"]);
	}

	$resultArray = array();
	$resultArray["rowCount"] = $rowCount;
	$resultArray["insert"] = $findIdInfo;
	$resultArray["sqlSuccess"] = $sqlSuccess;
	$resultArray["list"] = $list;

	echo json_encode($resultArray);
}



function findPassword(){
	global $app;
  	$jsondata = $app->request->params("findPasswordInfo");
  	$findPasswordInfo = json_decode( $jsondata, true );


  	$dbh = getPdoObject();
	$stmt = $dbh->prepare('SELECT kmem_name1, mem_pass, mem_id, email FROM member where mem_id=:id and email=:email');
	$stmt->bindValue(':email', $findPasswordInfo["email"], PDO::PARAM_STR);
	$stmt->bindValue(':id', $findPasswordInfo["id"], PDO::PARAM_STR);

	$sqlSuccess = $stmt->execute();
	$list = $stmt->fetch();
	$rowCount = $stmt->rowCount();

	if( $rowCount > 0  ){
		sendFindPasswordEmail( $list["mem_pass"], $list["email"]);
	}


  	$resultArray = array();
	$resultArray["rowCount"] = $rowCount;
	$resultArray["insert"] = $findPasswordInfo;
	$resultArray["sqlSuccess"] = $sqlSuccess;
	$resultArray["list"] = $list;

	echo json_encode($resultArray);
}




function sendFindIdEmail( $p_id, $p_email ){
	$to      = $p_email;
	$message = '회원님의 아이디는 '.$p_id. "입니다.";
	$conv_message = iconv("UTF-8","EUC-KR", $message);				

	$subject = "=?EUC-KR?B?".base64_encode(iconv("UTF-8","EUC-KR", '안녕하세요 마이클레어 입니다.'))."?=";
	
	$headers = 'From: myclair@myclair.kr' . "\r\n" .
	    'Reply-To: myclair@myclair.kr' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
	
	$headers .= "Content-Type: text/html; charset=utf-8\r\n";
	$headers .= "MIME-Version: 1.0\r\n";


	mail($to, $subject, $conv_message, $headers);
}



function sendFindPasswordEmail( $p_password, $p_email ){
	$to      = $p_email;
	$message = '회원님의 비밀번호는 '.$p_password. "입니다.";
	$conv_message = iconv("UTF-8","EUC-KR", $message);

	$subject = "=?EUC-KR?B?".base64_encode(iconv("UTF-8","EUC-KR", '안녕하세요 마이클레어 입니다.'))."?=";
	
	$headers = 'From: myclair@myclair.kr' . "\r\n" .
	    'Reply-To: myclair@myclair.kr' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();
	
	$headers .= "Content-Type: text/html; charset=utf-8\r\n";
	$headers .= "MIME-Version: 1.0\r\n";


	mail($to, $subject, $conv_message, $headers);
}


?>
<!-- header -->
<div id="header">
    <div class="inner">
        <h1><a href="../main/main.php"><img src="../images/common/logo.png" alt="clair" /></a></h1>
        <!-- gnb -->
        <ul id="gnb">
            <li><a href="../main/main.php#wrap">clair소개</a></li>
            <li><a href="../main/main.php#filter">미세먼지의 위험성</a></li>
            <li><a href="../main/main.php#pdt_shop">제품안내</a></li>
            <li><a href="../shop/buy.php">구매하기</a></li>
        </ul>
        <!-- // gnb -->
        
        <!-- ico -->
        <div class="ico">
            <a href="../shop/buy_list.php"><img src="../images/common/ico_02.png" alt="" /></a>
            <a href="#none"><img src="../images/common/ico_03.png" alt="" /></a>
            <a href="../shop/login.php"><img src="../images/common/ico_01.png" alt="비회원로그인" /></a>
            <a href="../shop/shop.php"><img src="../images/common/ico_04.png" alt="" /></a>
        </div>
        <!-- // ico -->
    </div>
</div>
<!-- // header -->
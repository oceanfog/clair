
<!-- header -->
<div class="visible-xs navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
        <div class="navbar-brand">
            <a href="http://myclair.kr/"><img src="../img/common/logo.png" style="width:75%;" ></a>
        </div>
        
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>

        </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../info/info4.php" >마이클레어란?</a></li>
        <li><a href="../product/r_product.php" >클레어 기술<br class="visible-sm visible-md"/> 알아보기</a></li> 
        <li><a href="../gallery/r_gallery.html" >마이클레어<br class="visible-sm visible-md"/> 디자인엿보기</a></li>
        <li><a href="../shop2/orderProc5.html" >마이클레어<br class="visible-sm visible-md"/> 주문하기</a></li>
        <?
            if( $user_name != ""){
            ?>
            	<li><a href="../shop2/logout.php" class="menu_background1">로그아웃</a></li>
            	<li><a href="../shop2/myPage.php" class="menu_background1">마이페이지</a></li>
            <?	
                } else {
            ?>
                
                <li><a href="../shop2/login3.html" class="menu_background1" >로그인</a></li>
            	<li><a href="../regist/regist3.html" class="menu_background1">회원가입</a></li>
                
            <?
                }
            ?> 
      </ul>
    </div>
  </div>
</div>




<nav class=" visible-sm visible-md visible-lg width_lg ">
    <!--
    <div ng-include="'../inc/header.html'" ></div>
    -->
    
    <div class="navbar-header">
      <a class="navbar-brand" href="http://myclair.kr"><img src="../img/common/logo.png"></a>

    </div>
    <div class="col-sm-6">
        <ul class="bnav nav-justified">
            <li class="menu01" ><a href="../info/info4.php" ng-style="topMenuStyle.01">마이클레어란?</a></li>
            <li class="menu03" ><a href="../product/r_product.php" ng-style="topMenuStyle.02">클레어 기술 알아보기</a></li>
            <li class="menu02" ><a href="../gallery/r_gallery.html" ng-style="topMenuStyle.03">마이클레어 디자인엿보기</a></li>            
            <li class="menu04" ><a href="../shop2/orderProc5.html" ng-style="topMenuStyle.04">마이클레어 주문하기</a></li>
        </ul>
    </div>
              
    <div id="top_btn" class="col-sm-2 visible-sm visible-md visible-lg">
        <!-- ico -->
        <div class="top_btn_area">

            <p>
            <?php
                $user_id = $_SESSION["user_id"];
                $user_name = $_SESSION["user_name"];

                if(  $_SESSION["mem_gubun"] == "a" ){
                    //echo "<span>".$user_name."</span>"."님 반갑습니다.";
                    echo "<a href='../shop2/adminPage2.html' >관리자페이지</button>";
                }
            ?>

            </p>

            <ul>
                <li class="top_btn_home" >
                    <a href="http://myclair.kr/">  </a>
                </li>
            
            <?
            if( $user_name != ""){
            ?>
            	<li class="top_btn_nonIcon"><a href="../shop2/logout.php">로그아웃</a></li>
            	<li class="top_btn_nonIcon"><a href="../shop2/myPage.php">마이페이지</a></li>
            <?	
                } else {
            ?>
                
                <li class="top_btn_nonIcon"><a href="../shop2/login2.html" >로그인</a></li>
            	<li class="top_btn_nonIcon"><a href="../regist/regist.html" >회원가입</a></li>
                
            <?
                }
            ?>
                
            </ul>            
        </div>
	</div>
        <!-- // ico -->

    
</nav>

<!-- // header -->

    
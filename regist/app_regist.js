angular.module('orderProc2')
.controller('JoinController', ['$scope','$http', '$upload', 'factory', function($scope, $http, $upload, factory){
	

		$scope.years = [];
		for(var i = 1901 ; i < 2030 ; i++) {
		  $scope.years.push( {"value":i, "displayName":i} );
		}
		
		$scope.user = user;

		var now = new Date();
		$scope.year = now.getFullYear();
		$scope.month = now.getMonth();
		$scope.day = now.getDay();

		$scope.findZip = function(){
			openDaumPostcode($scope);

		}
		  
		$scope.updateDate = function (){
		  $scope.date = new Date($scope.year, $scope.month - 1, $scope.day);
		};
		  
		$scope.updateDate();

		$scope.insertTestForm = function(){

			$scope.user = testUserObject;
		};

		$scope.duplChk = function(){

			if( $scope.user.mem_id == "" ){
				alert("id를 입력 해주세요.");
				return;
			}

			$scope.user.duplChk == "";

			data = "hello";
						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			$http({
		      method: 'GET',
		      url: '../restful/user.php/duplChk/'+$scope.user.mem_id,
		      data: data,
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		    }).
		    success(function(response) {
		        if( response.resultDuplChk == 0){
		        	$scope.user.duplChk = "canuse";
		        }
		        //console.log( $scope.user );

		        if( $scope.user.duplChk == "canuse" ){
					alert( $scope.user.mem_id + "은(는)사용하실 수 있습니다." );
				}else {
					alert( $scope.user.mem_id + "은(는)이미 존재하는 id입니다." );
				}
		    }).
		    error(function(response) {
		        $scope.codeStatus = response || "Request failed";
		    });

			
			
		};

		



		$scope.saveData = function(){
			//console.log ( $scope.user );
			
			if( validationUserJoinForm( $scope.user ) != false ){
				//user object를 json형태로 보냄
				data = 'registInfo='+JSON.stringify( $scope.user);

				factory.postRestfulResult('regist.php', data).
			    success(function(response) {

			        if( response.resultCode == 1 ){
						//session에 id, 이름 등 로그인 정보를 입력한다.
						//main page로 이동한다.
						alert("회원 가입 되었습니다. 메인페이지로 이동합니다.");
						

						data = 'loginInfo='+JSON.stringify( $scope.user);

						factory.postRestfulResult('../shop2/login.php', data).
					    success(function(response) {
					    	location.href = "../main/main.php";	
					    });


					} else if( response.resultCode == 2 ) {
						alert("아이디가 이미 존재 합니다.");
						
					} else{
						alert("회원 등록에 실패했습니다.");
					}
			    }).
			    error(function(response) {
			        $scope.codeStatus = response || "Request failed";
			    });
			}
			
		};

		$scope.cancel = function(){
			location.href = "../shop2/login2.html";
		};



	}]);



var getDuplChk = function( p_http, p_id ){
	//id를 받아서 중복체크 결과 값을 돌려준다.

	//data = 'registInfo='+JSON.stringify( $scope.user);
				//console.log(data);
	data = "hello";
				
	p_http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
	p_http({
      method: 'GET',
      url: '../restful/user.php/duplChk/'+p_id,
      data: data,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).
    success(function(response) {
        //console.log( $scope.user );
        //console.log( response );
		/*
        if( response.resultDuplChk == 0){
        	user.duplChk = "canuse";
        }
        */

    }).
    error(function(response) {
        $scope.codeStatus = response || "Request failed";
    });


}

var getCertification = function(){
	var result = true;
	//본인 인증 process를 실행한다.

	if( result == true){
		alert("본인 인증이 완료 되었습니다.");
	} else {
		result = false;
		alert("본인 인증이 실패 했습니다. 다시한번 시도 해주세요.");
	}

	return result;
}

var validationUserJoinForm = function( p_userObject ){
	//user object를 받아서 값이 있는지 없는지 검사한다.
	/*
		모든 조건을 만족하면 true를 하나라도 만족하지 않으면 false를 return한다.
	*/

	if( p_userObject.mem_id == "" ){
		alert("id를 입력 해주세요.");
		return false;
	}

	if( p_userObject.duplChk != "canuse" ){
		alert("id중복 체크를 해주세요.");
		return false;
	}

	if( p_userObject.mem_pass == "" ){
		alert("비밀번호를 입력 해주세요.");
		return false;
	}

	if( p_userObject.mem_pass != p_userObject.passwordCheck ){
		alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
		return false;
	}

	if( p_userObject.kmem_name1 == "" ){
		alert("이름을 입력 해주세요.");
		return false;
	}

	if( p_userObject.hp == "" ){
		alert("핸드폰 번호를 입력 해주세요.");
		return false;
	}

	if( p_userObject.certification == "" ){
		alert("본인인증을 해주시기 바랍니다.");
		return false;
	}

	if( p_userObject.addr1 == "" ){
		alert("주소를 입력 해주세요.");
		return false;
	}

	if( p_userObject.email == "" ){
		alert("이메일 주소를 입력 해주세요.");
		return false;
	}

	if( p_userObject.agree != true ){
		alert("이용약관에 동의 해주세요.");
		return false;
	}


}


var years = [
	
]

var user = {
	mem_id:"",
	duplChk:"",
	mem_pass:"",
	passwordCheck:"",
	kmem_name1:"",
	birthYear:"",
	birthMonth:"",
	birthDate:"",
	sex:"",
	hp:"",
	certification:"",
	zip1:"",
	zip2:"",
	addr1:"",
	addr2:"",
	email:"",
	certification:"false"
}

var testUserObject = {
	mem_id:"oceanfog",
	duplChk:"",
	mem_pass:"1123",
	passwordCheck:"1123",
	kmem_name1:"김경록",
	birthYear:1987,
	birthMonth:"11",
	birthDate:"19",
	sex:"male",
	hp:"010-3588-6265",
	certification:"",
	zip1:"464",
	zip2:"896",
	addr1:"서울시 관악구 봉천동 ",
	addr2:"49-5 다정빌 203호",
	email:"oceanfog1@gmail.com",
	certification:"false"
}


var printMessage = function( p_resultObject ){
	if( p_resultObject.resultCode == 1 ){
		//session에 id, 이름 등 로그인 정보를 입력한다.
		//main page로 이동한다.
		alert("회원 가입 되었습니다.");
		location.href = "../shop2/login2.html";

	} else if( p_resultObject.resultCode == 2 ) {
		alert("아이디가 이미 존재 합니다.");
		
	} else{
		alert("회원 등록에 실패했습니다.");
	}
}


function openDaumPostcode( p_scope ) {
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
            // 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

            
            document.getElementById('post1').value = data.postcode1;
            document.getElementById('post2').value = data.postcode2;
            document.getElementById('addr1').value = data.address;

            p_scope.user.zip1 = data.postcode1;
            p_scope.user.zip2 = data.postcode2;
            p_scope.user.addr1 = data.address;


            //전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,
            //아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.
            //var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');
            //document.getElementById('addr').value = addr;

            document.getElementById('addr2').focus();
        }
    }).open();
}
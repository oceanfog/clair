create table schedule(
schedule_no int not null auto_increment primary key,
schedule_day date,
schedule_month varchar(2),
schedule_year char(2),
schedule_title varchar(40),
schedule_content text,
schedule_date varchar(30)
);
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<style>
A:link { text-decoration: none; color:#4E4E4E }
A:visited { text-decoration: none; color:#4E4E4E}
A:hover { text-decoration: none; color:#990000}
table{font-size:9pt;color:#555555}
.t{border:1x solid #999999;font-size:8pt}
input{background-color:white;border:1x solid;color:#999999;font-size:9pt}
</style>
<script>
<!--
function open_win(url,bb,cc){
	window.open(url,bb,cc);
}
//-->
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000">
<table width="560" border="0" cellspacing="0" cellpadding="0">
  <!-- tr1 start -->
  <tr> 
    <td> 
      <?
		if(!$year||!$month){
		    $month=date("m");
		    $year=date("Y");
		}
		$pre_month=date("m",mktime(0,0,0,$month-1,1,$year)); //지난달 mktime(시,분,초,달,일,년)
		$pre_year=date("m",mktime(0,0,0,$month,1,$year-1));//지난달 년도
		$next_month=date("m",mktime(0,0,0,$month+1,1,$year));//다음달 
		$next_year=date("m",mktime(0,0,0,$month,1,$year+1));//다음년도

		$first_date=mktime(0,0,0,$month,1,$year);//선택된 년월의 첫번째 일자
		$show_date=1;
		$now_day=date('Y-m-d');
	  ?>
    </td>
  </tr>
  <!-- tr1 end -->   
  <!-- tr2 start --> 
  <tr> 
    <!-- new table,날짜출력부분, form start -->
    <td> 
      <form action="<?echo $PHP_SELF;?>" method="post" name="form1">
        <table width=560 border=0 cellspacing=0 cellpadding=0>
          <tr align="right"> 
            <td colspan="3"> 
              <select name=year onChange=submit()>
                <?
 				 for($i=1995;$i<2011;$i++){
				     if($year==$i){
				         echo"<option value=$i selected>$i";
				     }else{
				 		echo"<option value=$i>$i";
					 }
				 }
			  	?>
              </select>
              년 &nbsp; 
              <select name=month onChange=submit()>
                <?
				for($i=1;$i<13;$i++){
					if($i==$month){
						echo"<option value=$i selected>$i";
					}else{
						echo"<option value=$i>$i";
					}
				}
				?>
              </select>
              월</td>
          </tr>
          <tr align="right">
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr> 
            <!-- 이전달 버튼 들어가는 부분 -->
            <td> 
              <?
				if($month <= 1){  //$month 가 1보다 작거나 같다는 것은 현재달의 전달이 이전년도를 의미한다.
					echo"<a href='$PHP_SELF?year=$pre_year&month=$pre_month'><img src='../images/sch/back.gif' width='80' height='25' border='0'></a>";
				}else{
					echo"<a href='$PHP_SELF?year=$year&month=$pre_month'><img src='../images/sch/back.gif' width='80' height='25' border='0'></a>";
				}
			?>
            </td>
            <!-- 이전달 버튼 들어가는 부분 -->
            <!-- 현재 스케쥴 년,월 표시 시작 -->
            <td background='../images/sch/t_day.gif' width='240' height='30'> 
              <table width="183" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td align="center"> <font color='#B42E34'>
                    <?echo $year."년 ".$month."월";?>
                    </font> </td>
                </tr>
              </table>
            </td>
            <!-- 현재 스케쥴 년,월 표시 끝 -->
            <!-- 다음달 버튼 들어가는 부분 -->
            <td align="right"> 
              <?
				if($month >=12){ //현재달의 다음달이 다음년도의 달이라는걸 의미한다.
					echo"<a href='$PHP_SELF?year=$next_year&month=$next_month'><img src='../images/sch/next.gif' width='80' height='25' border='0'></a>";
				}else{
					echo"<a href='$PHP_SELF?year=$year&month=$next_month'><img src='../images/sch/next.gif' width='80' height='25' border='0'></a>";
				}
			?>
            </td>
            <!-- 다음달 버튼 들어가는 부분 끝-->
          </tr>
        </table>
      </form>
    </td>
    <!-- new table,날짜출력부분, form end -->
  </tr>
  <!-- tr2 end -->   
  <!-- tr3 start -->    
  <tr>
  <!-- new table,스케쥴 출력 부분 시작 --> 
    <td> 
      <table width="560" border="0" cellspacing="0" cellpadding="0" class=t>
        <tr> 
          <td colspan="7" height="30" background="../images/sch/day.gif">&nbsp;</td>
        </tr>
		<!--########################### 달력 출력 시작 ###############################-->
		<?
		for($i=0;$i<6;$i++){   //6주간의 달력을 그린다.
		echo"<tr>";    
		for($j=0;$j<7;$j++){   //일요일부터 토요일까지 출력

		/*####### 현재달이 아니거나 첫주가 아니고 첫번째 일의요일보다 작다면 #######*/
		//첫번째 if 문 시작
		if(($month!=date("m",$first_date)) || ($i==0 && $j < date("w",$first_date))){
			echo"<td width=80 height=80 align=center background='../images/sch/0.gif'>&nbsp;</td>";
        }
		// 첫번째 if 문 끝
		
		//첫번째 else 문 시작
		else{  
		// 두번째 if 문(현재 날자는 배경색 다르게,글자색도 다르게 지정)
			if(date('Y-m-d',$first_date)==date("Y-m-d")){


				echo"<td width=80 height=80 align=center background='../images/sch/now.gif'>";
			  echo"<b>$show_date</b></td>";
		}
		//두번째 if 문 끝
		// 두번째 else 문 시작
		else{

			/*일요일,토요일일 경우 날자색을 다르게 표시한다*/
			if($j==0) $bg="#990000";
			if($j==6) $bg='blue';
			if($j!=0 and $j!=6) $bg="#999999";

			echo"<td width=80 height=80 align=center valign=top background='../images/sch/$show_date.gif' bgcolor=$bg><br><br>";
			echo"</td>";
			}
		// 두번째 else 문 끝

		//일자를 하나씩 증가시킨다.
			$first_date=mktime(0,0,0,$month,$show_date+1,$year);
			$show_date++;
		}
				
		// 첫번째 else 문 끝
		}   //for
		echo"</tr>";
		}     //for
		?>
      </table>	  
    </td>
  <!-- new table,스케쥴 출력 부분 끝 -->	
  </tr>
  <!-- tr3 end -->   
</table>
</body>
</html>

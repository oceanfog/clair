<?
include("../config.php");
if($board_code=="") $board_inc="../pds_notice/list.php";
else if($board_code=="write") $board_inc="../pds_notice/input.php";
else if($board_code=="edit") $board_inc="../pds_notice/edit.php";
else if($board_code=="view") $board_inc="../pds_notice/read.php";

if($tbl_name=="category") {
	if($board_code=="") $board_inc="../category/list.php";
	else if($board_code=="write") $board_inc="../category/write.php";
	else if($board_code=="edit") $board_inc="../category/edit.php";
}

if($tbl_name=="goods") {
	if($board_code=="") $board_inc="../shop/list.php";
	else if($board_code=="write" || $board_code=="edit") $board_inc="../shop/goods_input.php";
}
if($tbl_name=="sales") {
	if($board_code=="") $board_inc="../shop/all_salelist.php";
	else if($board_code=="view") $board_inc="../shop/saleread.php";
}

if($tbl_name=="banner") {
	if($board_code=="") $board_inc="../banner/list.php";
	else if($board_code=="write" || $board_code=="edit") {
		$board_inc="../banner/write.php";
		$onload="onload=gogo(document.form1);";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//0EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clair 관리자</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/common.css" />

</head>

<body <?=$onload;?>>
<div class="wrapper">
<div class="wide_top_section">

	<div class="top_section">
		<h1>Clair 홈페이지 관리자 페이지</h1>

		<ul class="toputil">
			<li class="mr7"><a href="../../"><span class="btn_type_Red btn_type">Homepage</span></a></li>
			<li><a href="../logout.php"><span class="btn_type_Black btn_type">Logout</span></a></li>
		</ul>


		<div class="category_bar">
			<ul>
				<li><a href="../contents/notice01.php?tbl_name=notice">BOARD</a></li>
				<li><a href="../contents/shop01.php?tbl_name=goods" class="on">SHOP</a></li>

			</ul>
		</div>
	<!--//top_section--></div>
<!--//wide_top_section--></div>

<div class="wide_location_bar">
	<div class="location_bar">

	<p class="lnb">HOME &gt; 제품관리 &gt; <?=$ti_txt;?></p>
	<!--//location_bar--></div>
<!--//wide_location_bar--></div>


<div class="wide_container">
	<div class="container">
		<div class="leftmenu">
			<h2>제품관리</h2>
			


			<div class="menu_list">
				<h4>제품관리</h4>
				<ul>
					<!--<li <?=$tbl_name=='category'?' style="background-color:mintcream";':'';?>><a href="./shop01.php?tbl_name=category">- 분류관리</a></li>-->
					<li <?=$tbl_name=='goods'?' style="background-color:mintcream";':'';?>><a href="./shop01.php?tbl_name=goods">- 제품리스트</a></li>
				</ul>
				<h4>구매관리</h4>
				<ul>
					<li <?=$tbl_name=='sales'?' style="background-color:mintcream";':'';?>><a href="./shop01.php?tbl_name=sales">- 주문리스트</a></li>
				</ul>
				<h4>배너관리</h4>
				<ul>
					<li <?=$tbl_name=='banner'?' style="background-color:mintcream";':'';?>><a href="./shop01.php?tbl_name=banner">- 배너리스트</a></li>
				</ul>
			<!--//menu_list--></div>

		<!--//leftmenu--></div>

		<div class="contents"><?include $board_inc;?></div>


	<!--//container--></div>
<!--//wide_container--></div>








<!--//wrapper--></div>
<div class="footer">
COPYRIGHT (C)   ALL RIGHT RESERVED
<!--//footer--></div>
</body>
</html>
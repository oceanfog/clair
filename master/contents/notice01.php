<?
$code="KSW_notice";
$menu_code="notice";
include("../config.php");
if($board_code=="") $board_inc="../pds_notice/list.php";
else if($board_code=="write") $board_inc="../pds_notice/input.php";
else if($board_code=="edit") $board_inc="../pds_notice/edit.php";
else if($board_code=="view") $board_inc="../pds_notice/read.php";


if($tbl_name=="online") {
	if($board_code=="") $board_inc="../online/list.php";
	else if($board_code=="view") $board_inc="../online/read.php";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//0EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Clair 관리자</title>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
</head>

<body>
<div class="wrapper">
<div class="wide_top_section">

	<div class="top_section">
		<h1>Clair 홈페이지 관리자 페이지</h1>

		<ul class="toputil">
			<li class="mr7"><a href="../../"><span class="btn_type_Red btn_type">Homepage</span></a></li>
			<li><a href="../logout.php"><span class="btn_type_Black btn_type">Logout</span></a></li>
		</ul>


		<div class="category_bar">
			<ul>
				<li><a href="../contents/notice01.php?tbl_name=notice" class="on">BOARD</a></li>
				<li><a href="../contents/shop01.php?tbl_name=goods">SHOP</a></li>

			</ul>
		</div>
	<!--//top_section--></div>
<!--//wide_top_section--></div>

<div class="wide_location_bar">
	<div class="location_bar">

	<p class="lnb">HOME &gt; BOARD &gt; <?=$ti_txt;?></p>
	<!--//location_bar--></div>
<!--//wide_location_bar--></div>


<div class="wide_container">
	<div class="container">
		<div class="leftmenu">
			<h2>게시판관리</h2>
			


			<div class="menu_list">
				<h4>커뮤니티</h4>
				<ul>
					<li <?=$tbl_name=='notice'?' style="background-color:mintcream";':'';?>><a href="./notice01.php?tbl_name=notice">- 공지사항</a></li>



				</ul>


			<!--//menu_list--></div>

		<!--//leftmenu--></div>

		<div class="contents"><?include $board_inc;?></div>


	<!--//container--></div>
<!--//wide_container--></div>








<!--//wrapper--></div>
<div class="footer">
COPYRIGHT (C)   ALL RIGHT RESERVED
<!--//footer--></div>
</body>
</html>
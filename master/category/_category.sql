CREATE TABLE _category (
  no int(11) NOT NULL auto_increment,
  gubun varchar(10) NOT NULL default '',
  cno int(11) NOT NULL default '0',
  depth char(1) default '1',
  title varchar(50) default NULL,
  parent int(11) NOT NULL default '0',
  etitle varchar(100) default NULL,
  ck enum('n','y') default 'y',
  regdate datetime default NULL,
  img1 varchar(50) default NULL,
  img2 varchar(50) default NULL,
  PRIMARY KEY  (no)
);
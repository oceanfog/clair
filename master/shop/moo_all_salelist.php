<?
include("../db_connect.inc");
include("../admin_check.php");
include("../function.php");

/***** 리스트 조회 쿼리 작성 *******************************************/
$pay_type="온라인결제";
$sql = "select distinct sid,baesong_date,cancel_date,gu_name,opt2,sang_name,sdate,redate,income,pay_type,baesong,level,mid,scount,scost,total_payment,sang_no,no,cp,use_point,(UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(sdate))/3600 as elapsed_time,(to_days(sdate)-to_days(now())) as rday,approvalno,bill from sales";
if($key !="" || $baesong !="" || $pay_type !="" || ($sd !="" && $ed !="")) {
	$sql.=" where";
}

if ($key != "") {
	$sql.=" $key  like '%$searchword%'";   //검색어가 있으면 선택
}
if($key =="" && ($sd !="" && $ed !="")) {
	$sd=$sd." 00:00:00";
	$ed=$ed." 23:59:59";
	//$sql.=" redate between '$sd' and '$ed'";
	$sql.=" sdate >= '$sd' and sdate <= '$ed'";
} else if($key !="" && ($sd !="" && $sd !="")) {
	$sd=$sd." 00:00:00";
	$ed=$ed." 23:59:59";
	$sql.=" and sdate >= '$sd' and sdate <= '$ed'";
	//$sql.=" and redate between '$sd' and '$ed'";
}

if($baesong !="" && ($key=="" && $sd =="" && $ed=="")) {
	$sql.=" baesong='$baesong'";
} else if($baesong !="" && ($key !="" || ($sd !="" && $ed !=""))) {
	$sql.=" and baesong='$baesong'";
}

if($pay_type !="" && ($baesong =="" && $key=="" && $sd =="" && $ed=="")) {
	$sql.=" pay_type='$pay_type'";
} else if($pay_type !="" && ($baesong !="" || $key !="" || ($sd !="" && $ed !=""))) {
	$sql.=" and pay_type='$pay_type'";
}


$sql = $sql . " group by sid order by no desc";

$result = @mysql_query($sql);
$total = @mysql_affected_rows();
/*###############################################################*/
/*################ 페이지 카운터 기본값 설정 #######################*/
if(!$page) $page=1;        //현재페이지 값설정
$list=10;             //한 페이지에 리스트시킬 레코드수
if(($total%$list) ==0) {
           $totalpage=intval($total/$list);
} else {
          $totalpage=intval($total/$list)+1;
}

// 페이지 그룹값 설정(페이지 카운터를 몇개까지 출력할건지 설정)
if(($page%10) ==0) {
                    $pagelist=intval($page/10);
}else {
                   $pagelist=intval($page/10)+1;
}

$start=($page-1)* $list;   //현페이지에서 시작페코드값설정
$end=($page)*$list;   //현재페이지에서 마지막레코드 값 설정

if ($end>$total) $end=$total;
if ($page > 1) mysql_data_seek($result, $start);
/*################ 페이지 카운터 기본값 설정 #######################*/
/*###############################################################*/
?>
<script>
<!--
function del(sid,cancle,page,scount,sang_no,pay_type){
	wow=confirm("해당 주문 내용이 삭제됩니다 \n\r\n\r 정말 삭제할까요?");
	if(wow){
	window.location='../shop/cancel.php?sid='+sid+'&mode='+cancle+'&page='+page+'&scount='+scount+'&sang_no='+sang_no+'&pay_type='+pay_type;
	}else{
		return;
	}
}
function baesong(sid,baesong,page){
	if(baesong=="baesong_cancel") {
		ck=confirm("취소된 주문은 복구할수없습니다 취소하시겠습니까?");
		if(ck) {
			window.location='../shop/new_baesong.php?sid='+sid+'&mode='+baesong+'&page='+page;
		}
	} else {
			window.location='../shop/new_baesong.php?sid='+sid+'&mode='+baesong+'&page='+page;
	}
}

function bill(sid,mode) {
	window.open("../shop/bill.php?mode="+mode+"&sid="+sid,"","width=400,height=130");
}


function showMSG(no,view){

sMSG.innerHTML=no;

sMSG.style.pixelLeft=event.clientX+15;
sMSG.style.pixelTop=event.clientY+15+document.body.scrollTop;

if(view) sMSG.style.display="block";
else sMSG.style.display="none";
}

function moveMSG(){
sMSG.style.pixelLeft=event.clientX+15;
sMSG.style.pixelTop=event.clientY+15+document.body.scrollTop;
}

function delivery_persuit(URL, delivery_num) {
	window.open(URL, "delivery", "leftmargin=0, toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,resicopyhistory=0,width=850,height=700");
}

function ck_date(s,e) {
	frm=document.tfrm;
	frm.sd.value=s;
	frm.ed.value=e;
}

function move_jumoon(frm) {
	if(frm.sang.value=="") {
		alert('이동하고자하는 주문상태를 선택해주세요');
		frm.sang.focus();
		return;
	}
    var chk    = document.getElementsByName("selectlist[]"); 
    var option_num = 0;         
     
    for(var i=0; i<chk.length; i++) { 
        if(chk[i].checked == true) { 
            option_num++; 
        } 
    } 

    if(option_num==0) { 
    alert('이동하실 주문을 선택해주세요'); 
    return; 
    } 
	frm.action="../shop/jumoon_move.php";
	frm.submit();

}

function reverse(form)
{
	for( var i=0; i<form.elements.length; i++) {
	  if (form.elements[i].name =='selectlist[]') {
		if(form.elements[i].checked == false) {
			form.elements[i].checked = true; }
		else {
			form.elements[i].checked = false; }
	  }
	}
	return;
}
//-->
</script>
<style type=text/css>
input.calendar { behavior:url(../shop/calendar1.HTC); }
</style>
</head>

<body leftmargin="0" topmargin="10" marginwidth="0" marginheight="0">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<form name="tfrm" action="<?echo $PHP_SELF;?>" method="post">
<input type=hidden name=page value=1>
<input type="hidden" name="ur" value="<?echo $REQUEST_URI;?>">
  <tr> 
    <td align="center"> <table border=0 cellpadding=5 cellspacing=0 width=100% align="center">
      </table>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" style="border:1x solid silver";>
			<tr><td>
			
			 <input type=text name="sd" size=10 class="calendar" value="<?echo substr($sd,0,10);?>"> 부터 ~ <input type=text name="ed" value="<?echo substr($ed,0,10);?>" size=10 class="calendar"> 까지 
			 &nbsp&nbsp;&nbsp;
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(14);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_2week.gif"></a>
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(30);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_month.gif"></a>
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(60);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_2month.gif"></a>
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(90);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_3month.gif"></a>
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(180);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_6month.gif"></a>
&nbsp;&nbsp;<a href="javascript:ck_date('<?echo date_sum(365);?>','<?echo date("Y-m-d");?>');"><img src="../shop/thumb/btn_oneyear.gif"></a>

			</td></tr>
			<tr><td>
			 &nbsp;
			 결제방법 : 
			 <select name="pay_type" onChange="document.tfrm.submit()";>
			 <option value="">전체
			 <option value="신용카드" <?if($pay_type=="신용카드") echo"selected";?>>신용카드
			 <option value="온라인결제" <?if($pay_type=="온라인결제") echo"selected";?>>온라인결제
			 <select>
			 &nbsp;
			 발주상태 : 
			 <select name="baesong" onChange="document.tfrm.submit()";>
			 <option value="">전체발주내역
			 <option value="0" <?if($baesong=="0") echo"selected";?>>입금확인중
			 <option value="1" <?if($baesong=="1") echo"selected";?>>발주확인(입금완료)
			 <option value="2" <?if($baesong=="2") echo"selected";?>>출고준비중
			 <option value="3" <?if($baesong=="3") echo"selected";?>>출고완료(배송준비중)
			 <option value="4" <?if($baesong=="4") echo"selected";?>>주문취소
			 <option value="5" <?if($baesong=="5") echo"selected";?>>배송완료
			 </select>&nbsp;&nbsp;



				<select name="key">
                <option value="">선택</option>
                <option value="sid" <?if($key=="sid") echo"selected";?>>주문번호</option>
                <option value="mid" <?if($key=="mid") echo"selected";?>>아이디</option>
                <option value="gu_name" <?if($key=="gu_name") echo"selected";?>>구매자명</option>
                <option value="su_name" <?if($key=="su_name") echo"selected";?>>수취인명</option>
                <option value="sendname" <?if($key=="sendname") echo"selected";?>>입금자명</option>

              </select>
			  &nbsp;&nbsp;검색어&nbsp; <input type="text" name="searchword" size="20" value="<?echo $searchword;?>"> 
              <a href="javascript:onClick=document.tfrm.submit()";><img src="../shop/thumb/btn_inquiry.gif"></a>&nbsp;&nbsp;&nbsp;<a href="<?echo $PHP_SELF;?>">[전체보기]</A>
			 </td>
          </tr>
		</table><br>
<?
if($baesong=="") $t_txt="<font color=red>전체주문내역</font> [총 $total 건]";
else if($baesong=="0") $t_txt="<font color=red>입금확인중</font> [총 $total 건]";
else if($baesong=="1") $t_txt="<font color=red>발주확인[입금완료]</font> [총 $total 건]";
else if($baesong=="2") $t_txt="<font color=red>출고준비중</font> [총 $total 건]";
else if($baesong=="3") $t_txt="<font color=red>출고완료[배송준비중]</font> [총 $total 건]";
else if($baesong=="4") $t_txt="<font color=red>주문취소</font> [총 $total 건]";
else if($baesong=="5") $t_txt="<font color=red>배송완료</font> [총 $total 건]";

if($pay_type=="") $pt=" 전체결제  &nbsp; > ";
else if($pay_type=="신용카드") $pt=" 신용카드  &nbsp; > ";
else if($pay_type=="온라인결제") $pt=" 온라인결제  &nbsp; > ";

?>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" style="border:1x solid silver";>
		<tr><td> * 선택한 주문을 
			 <select name="sang">
			 <option value="">선택하세요
			 <option value="0">입금확인중
			 <option value="1">발주확인(입금완료)
			 <option value="2">출고준비중
			 <option value="3">출고완료(배송준비중)
			 <option value="4">주문취소
			 <option value="5">배송완료
			 </select> (으)로<input type="button" value="이동하기" onClick="move_jumoon(document.tfrm);";>&nbsp;&nbsp;
			 <font color=red><?echo $pt;?></font>&nbsp;&nbsp;&nbsp;<?echo $t_txt;?>
			 &nbsp;&nbsp;<a href="../shop/sale_excel2.php?page=<?=$page?>&key=<?=$key?>&searchword=<?=$searchword?>&sd=<?=$sd;?>&ed=<?echo $ed;?>&baesong=<?echo $baesong;?>&pay_type=<?echo $pay_type;?>"><img src="../icon/btn_excel.gif"></a>&nbsp;&nbsp;




		</td></tr>
		</table><br>




	<table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr bgcolor="gainsboro" height="30"> 
          <td align="center" width="40"><font color="#000000"><a href="javascript:reverse(document.tfrm)";>선택</a></font></td>
          <td align="center" width="80"><font color="#000000">주문일</font></td>
          <td height="25" align="center" width="120"><font color="#000000">주문번호</font></td>
          <td align="center" width="120"><font color="#000000">주문자</font></td>
          <td align="center" width="230"><font color="#000000">주문제품</font></td>
          <td align="center" width="60"><font color="#000000">가격</font></td>
          <td align="center" width="40"><font color="#000000">수량</font></td>
          <td align="center" width="110"><font color="#000000">결제방식</font></td>
          <td align="center" width="100"><font color="#000000">처리상태</font></td>
          <td align="center" width="80"><font color="#000000">발주확인일</font></td>
          <td align="center" width="100"><font color="#000000">배송예정일</font></td>
          <td align="center" width="120"><font color="#000000">송장번호</font></td>
        </tr>
<?
/***** 리스트 출력  **********************************************/
for($i=$start;$i<$end;$i++){ // 현재 페이지에 해당 하는 리스트만 출력
$row = @mysql_fetch_object($result);
$sid=$row->sid;


$mid=$row->mid;
$scount=$row->scount;//각 레코드당 총 주문량	
$pay_type=$row->pay_type;
$sang_no=$row->sang_no;

//운송장번호
if($row->bill=="") {
	$bill="";
} else {
	$bill="<a href=\"javascript:delivery_persuit('http://nexs.cjgls.com/web/service02_01.jsp?slipno=$row->bill', '$row->bill');\"><img src='../shop/cj.bmp'></a>";
}

//해당회원의 총 포인트
$qq="select sum(point) as point from point where id='$mid'";
$roo=mysql_fetch_object(mysql_query($qq));
if($mid=="고객") {
	$point=0;
} else {
	$point=$roo->point;
}

$now=date("Y-m-d");
$reg=explode(" ",$row->sdate);
if($reg[0]==$now) {
	$bg="white";
} else {
	$bg="white";
}

//이미지
$iro=@mysql_fetch_object(@mysql_query("select img1,point from goods where sang_no=$row->sang_no"));
$total_payment=$row->total_payment - $row->use_point - $row->cp;
if($row->total_payment < 30000) $total_payment=$total_payment + 2500;

$total_payment=round($total_payment,-1);

$wrel=@mysql_query("select * from sales where sid='$row->sid'");
$wtot=@mysql_affected_rows();

for($ww=0;$ww<$wtot;$ww++) {
	$wro=@mysql_fetch_object($wrel);
	$arr_sang_name[$ww]=$wro->sang_name." ".$wro->weight;
	$arr_scost[$ww]=$wro->scost;
	$arr_scount[$ww]=$wro->scount;
}


$rsp=$wtot;

if($row->baesong=="1") $bae="입금완료";
else if($row->baesong=="2") $bae="<font color=blue>출고준비중</font>";
else if($row->baesong=="3") $bae="<font color=blue>배송준비중</font>";
else if($row->baesong=="4") $bae="<font color=red>주문취소</font><br>$row->cancel_date";
else if($row->baesong=="0") $bae="<font color=black>입금확인중</font>";
else if($row->baesong=="5") $bae="<font color=blue>배송완료</font>";

if($row->pay_type=="신용카드") $sb="승번 : $row->opt2";
else $sb="";
?>
        <tr bgcolor=<?echo $bg;?>> 
		  <td align="center" rowspan="<?echo $rsp;?>"><input type="checkbox" name="selectlist[]" value="<?echo $row->no;?>"></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="80"><? echo "$row->sdate"; ?></font></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="120"><a href="sale.php?board_code=view&sid=<?echo $sid; ?>&page=<?echo $page?>&key=<?echo $key?>&searchword=<?echo $searchword?>&no=<?echo $row->no;?>"> 
            <!--<img src="../shop/thumb/<?echo $iro->img1;?>" width=50 border=0>-->
			<br><? echo $row->sid;?></a></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="120">회원 ID : <? echo $mid ?><br> <font color="#006699">구매자명 : <? echo $row->gu_name?></font><br> 
            적립포인트 : <? echo number_format($point);?></td>
          <td align="center" width="230"><?echo $arr_sang_name[0];?></td>
          <td align="center"><?echo $arr_scost[0];?></td>
          <td align="center"><?echo $arr_scount[0];?></td>
          <td align="center" rowspan="<?echo $rsp;?>" ><?echo $row->pay_type;?><br>입금액 : <font color=red><?echo number_format($total_payment);?>원<br><?echo $sb;?></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="100"><?echo $bae;?></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="80"><?echo substr($row->redate,0,10);?></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="100"><input type=text name="baesong_date[]" value="<? echo $row->baesong_date;?>" size=10 class="calendar"></td>
          <td align="center" rowspan="<?echo $rsp;?>" width="120"><input type=text name="bi[]" style="color:blue"; size=12 value="<?echo $row->bill;?>"><?echo $bill;?></td>
        </tr>
	  <?
		  if($rsp > 1) {
			for($w=1;$w<$wtot;$w++){
				echo"<tr><td align='center'>$arr_sang_name[$w]</td><td align='center'>$arr_scost[$w]</td><td align='center'>$arr_scount[$w]</td></tr>";
			}
		}
	  ?>
        <? $rsp=""; }?>

      </table></td>
  </tr>
  </form>
  <tr> 
    <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="5">
        <!--***** 검색 form  *******************************************//-->
          <tr> 
            <td> 
<?
/****************************** 페이지 카운터 출력 ***********************************/
if ($pagelist > 1) { // 페이지 그룹단위로 앞으로 이동
	$pagepre=($pagelist-1)*10;
  echo "<a href='$PHP_SELF?page=$pagepre&sd=$sd&ed=$ed&key=$key&searchword=$searchword&baesong=$baesong&pay_type=$pay_type'>PRE 10</a>";
}
$startpage = ($pagelist-1)*10+1;
$endpage = $pagelist*10;
if ($totalpage < $pagelist*10) $endpage = $totalpage;
for ($i=$startpage; $i<=$endpage; $i++) {  // 현재 페이지
    if ($i==$page) {
	echo "[$i]";
    }else{
	echo "<a href='$PHP_SELF?page=$i&sd=$sd&ed=$ed&key=$key&searchword=$searchword&baesong=$baesong&pay_type=$pay_type'>[$i]</a>";
    }
}
if ($pagelist*10 < $totalpage) { // 페이지 그룹단위로 뒤로 이동
	$pagenext=($pagelist*10)+1;
   echo "<a href='$PHP_SELF?page=$pagenext&sd=$sd&ed=$ed&key=$key&searchword=$searchword&baesong=$baesong&pay_type=$pay_type'>NEXT 10</a>";
}
/****************************** 페이지 카운터 출력 ***********************************/
?>
            </td>
            <td align="right"> 
			</td>
          </tr>
      </table></td>
  </tr>
</table>
<span id=sMSG style="width:300;height=300;padding:4pt;font-size:9pt;line-height:13pt;position:absolute;left:0;top:0;z-index:20; display:none;border:2x solid gainsboro;background-color:white;color:gray"></span> 
<? mysql_close();?>
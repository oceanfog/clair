CREATE TABLE KSW_bbs (
  bbs_no int(11) NOT NULL auto_increment,
  bbs_gid int(11) default NULL,
  bbs_pos int(11) default NULL,
  bbs_tab int(11) default NULL,
  bbs_name varchar(20) default NULL,
  bbs_email varchar(50) default NULL,
  bbs_title varchar(100) default NULL,
  bbs_content text,
  bbs_hits int(11) default NULL,
  bbs_regdate varchar(20) default NULL,
  tbl_name varchar(20) NOT NULL default '',
  bbs_password varchar(50) default NULL,
  bbs_filename varchar(50) default NULL,
  bbs_filename2 varchar(50) default NULL,
  bbs_ip varchar(20) default NULL,
  num int(11) default '0',
  ck enum('on','off') default 'off',
  PRIMARY KEY  (bbs_no)
) TYPE=MyISAM;

CREATE TABLE KSW_comment (
  c_no int(11) NOT NULL auto_increment,
  num int(11) NOT NULL default '0',
  c_name varchar(20) NOT NULL default '',
  c_pass varchar(20) NOT NULL default '',
  comment text,
  c_date datetime default NULL,
  tbl_name varchar(20) default NULL,
  PRIMARY KEY  (c_no)
) TYPE=MyISAM;

CREATE TABLE KSW_notice (
  no int(11) NOT NULL auto_increment,
  name varchar(15) default NULL,
  title varchar(100) default NULL,
  content text,
  regdate date default NULL,
  hits int(11) default NULL,
  ck varchar(10) default NULL,
  types enum('news','notice') default 'notice',
  tbl_name varchar(20) NOT NULL default '',
  img1 varchar(50) default NULL,
  img2 varchar(50) default NULL,
  img3 varchar(50) default NULL,
  img4 varchar(50) default NULL,
  img5 varchar(50) default NULL,
  img6 varchar(50) default NULL,
  img7 varchar(50) default NULL,
  img8 varchar(50) default NULL,
  img9 varchar(50) default NULL,
  img10 varchar(50) default NULL,
  PRIMARY KEY  (no)
) TYPE=MyISAM;
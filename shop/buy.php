<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
include("../master/db_connect.inc");
include("../master/function.php");

$qq="select * from imsi where cook='$buy_cookie'";
$re=@mysql_query($qq);
$to=@mysql_affected_rows();

if($to < 1){ //장바구니가 비어있는 상태라면
	echo"<script>alert('주문하신 상품이 없습니다.');window.location='../product/list.php';</script>";
	exit;
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
	function KeyCodeCheck(){
		if ((event.keyCode < 48)||(event.keyCode > 57)) event.returnValue=false;
	}

function mail_ck(frm) {
	if(frm.mail2.value=="etc") {
		document.getElementById('mail3').readOnly=false; 
		frm.mail3.value="";

	} else {
		document.getElementById('mail3').readOnly=true;
		frm.mail3.value=frm.mail2.value;
	}

}

function nzipopen(){
  window.open("./search_zipcd.php?mode=zipin3","zipcode","width=520,height=320,left=300,top=300,toolbar=no,location=no,directories=no ,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no");
   }
function nzipopen2(){
  window.open("./search_zipcd2.php?mode=zipin3","zipcode","width=520,height=320,left=300,top=300,toolbar=no,location=no,directories=no ,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no");
   }
function send(frm) {
	if(frm.gu_name.value=="") {
		alert('주문자명을 입력해주세요');
		frm.gu_name.focus();
		return;
	}
	if(frm.hp1.value=="" || frm.hp2.value=="" || frm.hp3.value=="") {
		alert('구매자 휴대폰 번호를 입력해주세요');
		frm.hp2.focus();
		return;
	}
	if(frm.mail1.value=="") {
		alert('메일 앞부분을 적어주세요');
		frm.mail1.focus();
		return;
	}
	if(frm.mail3.value=="") {
		alert('메일 뒷부분을 적어주세요');
		frm.mail3.focus();
		return;
	}
	if(frm.pass.value=="") {
		alert('주문 비밀번호를 입력해주세요');
		frm.pass.focus();
		return;
	}
	if(frm.pass_check.value=="") {
		alert('주문 비밀번호 확인란을 입력해주세요');
		frm.pass_check.focus();
		return;
	}
	if(frm.pass.value != frm.pass_check.value) {
		alert('주문 비밀번호와 확인란이 일치하지않습니다');
		frm.pass.focus();
		return;
	}
	if(frm.czip1.value=="" || frm.czip2.value=="") {
		alert('우편번호를 주소찿기를 통해서 입력해주세요');
		frm.czip1.focus();
		return;
	}
	if(frm.caddr1.value=="") {
		alert('주소를 입력해주세요');
		frm.caddr1.focus();
		return;
	}
	if(frm.caddr2.value=="") {
		alert('세부주소를 입력해주세요');
		frm.caddr2.focus();
		return;
	}
	if(frm.su_name.value=="") {
		alert('받을사람 이름을 적어주세요');
		frm.su_name.focus();
		return;
	}
	if(frm.shp1.value=="" || frm.shp2.value=="" || frm.shp3.value=="") {
		alert('받을사람 휴대폰 번호를 입력해주세요');
		frm.shp2.focus();
		return;
	}
	if(frm.zip1.value=="" || frm.zip2.value=="") {
		alert('받을사람 우편번호를 주소찿기를 통해서 입력해주세요');
		frm.zip1.focus();
		return;
	}
	if(frm.addr1.value=="") {
		alert('받을사람 주소를 입력해주세요');
		frm.addr1.focus();
		return;
	}
	if(frm.addr2.value=="") {
		alert('받을사람 세부주소를 입력해주세요');
		frm.addr2.focus();
		return;
	}


	if(!frm.pay_type[0].checked && !frm.pay_type[1].checked && !frm.pay_type[2].checked && !frm.pay_type[3].checked){
		alert('결제방식을 선택해주세요');
		return;
	}
    if(frm.pay_type[0].checked==true){		
		if(frm.bank.value==""){
		alert('온라인 결제를 송금하실 은행구좌를 선택하셔야합니다');
		frm.bank.focus();
		return;
		}
		if(frm.sendname.value==""){
		alert('온라인 송금을 하실경우는 입금자명을 적으셔야합니다');
		frm.sendname.focus();
		return;
		}
		if(frm.receipt[0].checked==true) {
			if(frm.so_gubun[0].checked==true) {
				if(frm.rhp1.value=="" || frm.rhp2.value=="" || frm.rhp3.value=="") {
					alert('현금영수증에 적용할 휴대폰 번호를 적어주세요');
					frm.rhp1.focus();
					return;
				}
			} else {
				if(frm.sa1.value=="" || frm.sa2.value=="" || frm.sa3.value=="") {
					alert('사업자 번호를 적어주세요');
					frm.sa1.focus();
					return;
				}


			}
		}
		check_order=confirm("장바구니 상품을 온라인 주문하시겠습니까?");
		if(check_order) frm.action='./order.php';
		else return;
	}  else {
		frm.action="./card.php";
	}
	frm.submit();
}

function check_same(){
	form1=document.form1;

	if(form1.same_check.checked){
		form1.zip1.value=form1.czip1.value;
		form1.zip2.value=form1.czip2.value;
		form1.su_name.value=form1.gu_name.value;
		form1.shp1.value=form1.hp1.value;
		form1.shp2.value=form1.hp2.value;
		form1.shp3.value=form1.hp3.value;
		form1.addr1.value=form1.caddr1.value;
		form1.addr2.value=form1.caddr2.value;
	}else{
		form1.zip1.value="";
		form1.zip2.value="";
		form1.su_name.value="";
		form1.shp1.value="";
		form1.shp2.value="";
		form1.shp3.value="";
		form1.addr1.value="";
		form1.addr2.value="";
		}
}
</script>
</head>
<body>
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
        
        <!-- container -->
        <div class="container">
            <div class="inner">
                
                <h2>주문/결재</h2>
                <h3>주문상품</h3>
				<form method=post name="form1" action="./order.php">

                    <fieldset>
                        <legend>주문상품</legend>
                        <table summary="주문상품 목록입니다. 각 항목으로는 선택, 상품옵션정보, 총 상품금액, 배송비가 있습니다.">
                            <caption>주문상품 목록</caption>
                            <colgroup>
                                <col width="*" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">상품옵션정보</th>
                                    <th scope="col">총 상품금액</th>
                                    <th scope="col">수량</th>
                                </tr>
                            </thead>
                            <tbody>
<?
						/** 쇼핑 카트 정보 출력 **/
						$sang_photo="../../master/shop/thumb/";
						$cid=0;
						$tgcost=0;
						$tpoint=0;
						$qs="select * from imsi where cook='$buy_cookie'";
						$rel=@mysql_query($qs) or die('db error');
						$total=@mysql_affected_rows();

						for($i=0;$i<$total;$i++){
						    $ro=@mysql_fetch_object($rel);
							$sql = "select * from goods where sang_no = $ro->imsi_sang_no";
							$result = @mysql_query($sql) or die('query error');
							
							$row = mysql_fetch_object($result);
							//이미지 없을때 있을때...
							if(file_exists($sang_photo.$row->img1) && $row->img1 !="") {
								$filename=$sang_photo.$row->img1;
							} else {
								$filename=$sang_photo."not_img_small.gif";
							}
							$price=$ro->imsi_sang_price;
							$point=intval($price) * $ro->imsi_gcount * ($row->point / 100);


							if($ro->imsi_sang_color !="") $imc=" / ".$ro->imsi_sang_color;
							else $imc="";

?>
                                <tr>
                                    <td class="option">
                                        <img width="85" height="85" src="../master/shop/thumb/<?=$row->img1;?>" alt="" />
                                        <dl>
                                            <dt><?=$row->sang_name;?></dt>
                                            <dd><?=number_format($ro->imsi_sang_price);?>원 <?=$imc;?></dd>
                                        </dl>
                                    </td>
                                    <td><?=number_format(($price*$ro->imsi_gcount),0); ?>원</td>
                                    <td><?=$ro->imsi_gcount;?>개</td>
                                </tr>
<?
			$cid=$cid+1;
			$tgcost=$tgcost+($price*$ro->imsi_gcount)+ $ro->item_price;
			$tpoint+=$row->point * $ro->imsi_gcount;
			}
			/*회원등급에 따라 배송료 설정*/

			//if($member_gubun=="e" || $member_gubun=="v") $charges=0;
			//else $charges=2500;
			if($tgcost > 30000) $charges=0;
			else $charges=0;

?>
                            </tbody>
                        </table>
                        
                        <h3 class="mt45">구매자정보</h3>
                        <table class="table_row" summary="구매자정보입니다. 각 항목으로는 주문자, 휴대폰, 이메일, 주문 비밀번호, 주문비밀번호 확인, 주소가 있습니다.">
                            <caption>구매자정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="buyer">주문자</label></th>
                                <td><input type="text" name="gu_name" id="buyer" class="w198" /></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone">휴대폰</label></th>
                                <td>
                                    <select id="phone" name="hp1" class="w88">
									  <option value="010" <?if($hp[0]=="010") echo"selected";?>>010 </option>
									  <option value="011" <?if($hp[0]=="011") echo"selected";?>>011 </option>
									  <option value="016" <?if($hp[0]=="016") echo"selected";?>>016 </option>
									  <option value="017" <?if($hp[0]=="017") echo"selected";?>>017 </option>
									  <option value="018" <?if($hp[0]=="018") echo"selected";?>>018 </option>
									  <option value="019" <?if($hp[0]=="019") echo"selected";?>>019 </option>
									</select> - <input type="text" name="hp2" class="w88" maxlength="4" /> - <input type="text" name="hp3" class="w88" maxlength="4" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="mail1">이메일</label></th>
                                <td>
                                    <input type="text" id="mail1" name="mail1" class="w148" /> @ <input type="text" id="mail3" name="mail3" class="w148" />
									<select class="w148" name="mail2" onchange="mail_ck(document.form1);">
																<option value="naver.com">naver.com</option>
																<option value="nate.com">nate.com</option>
																<option value="chol.com">chol.com</option>
																<option value="daum.net">daum.net</option>
																<option value="dreamwiz.com">dreamwiz.com</option>
																<option value="empal.com">empal.com</option>
																<option value="freechal.com">freechal.com</option>
																<option value="hanafos.com">hanafos.com</option>
																<option value="hotmail.com">hotmail.com</option>
																<option value="intizen.com">intizen.com</option>
																<option value="kebi.com">kebi.com</option>
																<option value="korea.com">korea.com</option>
																<option value="lycos.co.kr">lycos.co.kr</option>
																<option value="netian.com">netian.com</option>
																<option value="paran.com">paran.com</option>
																<option value="yahoo.co.kr">yahoo.co.kr</option>
																<option value="etc" selected>직접입력</option>
																</select>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="pw">주문 비밀번호</label></th>
                                <td><input type="password" name="pass" id="pw" class="w148" /> <span class="c_txt">6~15자로 입력해주세요.</span></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="pw02">주문 비밀번호 확인</label></th>
                                <td><input type="password" name="pass_check" id="pw02" class="w148" /> <span class="c_txt">비밀번호를 한번 더 입력해주세요.</span></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr">주소</label></th>
                                <td>
                                    <ul>
                                        <li><input type="text" name="czip1" id="addr" class="w60" />-<input type="text" name="czip2" id="addr" class="w60" /> <a href="javascript:nzipopen();" ><img src="../images/content/btn_addr.gif" alt="우편번호찾기" /></a></li>
                                        <li><input type="text"  name="caddr1" class="w708" /></li>
                                        <li><input type="text"  name="caddr2" class="w708" /></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        
                        <h3 class="mt45">받는분정보</h3>
                        <p class="txt_same"><input type="checkbox" name="same_check" id="same" onClick="check_same();"; /><label for="same">구매자 정보와 동일</label></p>
                        <table class="table_row" summary="받는분정보입니다. 각 항목으로는 받을사람 이름, 휴대폰, 주소가 있습니다.">
                            <caption>받는분정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="buyer02">받을사람 이름</label></th>
                                <td><input type="text" name="su_name" id="buyer02" class="w198" /></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone02">휴대폰</label></th>
                                <td>
                                    <select id="phone02" name="shp1" class="w88">
									  <option value="010" <?if($hp[0]=="010") echo"selected";?>>010 </option>
									  <option value="011" <?if($hp[0]=="011") echo"selected";?>>011 </option>
									  <option value="016" <?if($hp[0]=="016") echo"selected";?>>016 </option>
									  <option value="017" <?if($hp[0]=="017") echo"selected";?>>017 </option>
									  <option value="018" <?if($hp[0]=="018") echo"selected";?>>018 </option>
									  <option value="019" <?if($hp[0]=="019") echo"selected";?>>019 </option>
									  </select> - <input type="text" class="w88" name="shp2" maxlength="4" /> - <input type="text" name="shp3" class="w88" maxlength="4" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr02">주소</label></th>
                                <td>
                                    <ul>
                                        <li><input type="text" name="zip1" id="addr02" class="w60" />-<input type="text" name="zip2" id="addr02" class="w60" /> <a href="javascript:nzipopen2();" ><img src="../images/content/btn_addr.gif" alt="우편번호찾기" /></a></li>
                                        <li><input type="text" name="addr1" class="w708" /></li>
                                        <li><input type="text"  name="addr2" class="w708" /></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                        
                        <h3 class="mt45">결제정보</h3>
                        <table summary="결제정보 목록입니다. 각 항목으로는 주문금액, 배송비, 결재하실 금액이 있습니다.">
                            <caption>결제정보 목록</caption>
                            <colgroup>
                                <col width="30%" />
                                <col width="30%" />
                                <col width="*" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">주문금액</th>
                                    <th scope="col">배송비</th>
                                    <th scope="col">결재하실 금액</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?=number_format($tgcost);?>원</td>
                                    <td>0원</td>
                                    <td class="pay"><?=number_format($tgcost + $charges);?>원</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <h3 class="mt45">결제방식</h3>
                        <table class="table_row" summary="결제방식입니다. 각 항목으로는 입금은행, 현금영수증, 무통장입금 이용안내가 있습니다.">
                            <caption>결제방식</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <td scope="col" colspan="2">
                                        <input type="radio" id="b_type01" name="pay_type" value="1" class="nobank" /><label for="b_type01" class="ml10">무통장입금</label>
                                        <input type="radio" id="b_type02" name="pay_type" value="2" class="ml30" /><label for="b_type02" class="ml10">실시간계좌이체</label>
                                        <input type="radio" id="b_type03" name="pay_type" value="3" class="ml30" /><label for="b_type03" class="ml10">신용카드</label>
                                        <input type="radio" id="b_type03" name="pay_type" value="4" class="ml30" /><label for="b_type03" class="ml10">에스크로(가상계좌)</label>

                                    </td>
                                </tr>
                            </thead>
                            <tr class="hide">
                                <th scope="row"><label for="bank">입금은행</label></th>
                                <td>
                                    <select id="bank" name="bank" class="w148">
                                        <option value="">은행선택</option>
                                        <option value="은행1 111-222-55555">은행1 111-222-55555</option>
                                        <option value="은행2 111-222-55555">은행2 111-222-55555</option>

                                    </select>
                                </td>
                            </tr>
                            <tr class="hide">
                                <th scope="row"><label for="b_name">입금자명</label></th>
                                <td><input type="text" name="sendname" id="b_name" class="w148" /></td>
                            </tr>
                            <tr class="hide rec_input">
                                <th scope="row">현금영수증</th>
                                <td>
                                    <input type="radio" name="receipt" id="rec01" value="0" /><label for="rec01" class="ml05">발행</label>
                                    <input type="radio" name="receipt" id="rec02" value="1" class="ml10" checked="checked" /><label for="rec02" class="ml05">미발행</label>
                                    <div class="rec_box hide">
                                        <input type="radio" id="receipt01" name="so_gubun"  value="0" checked /><label for="receipt01" class="ml05">개인소득공제용</label>
                                        <select class="w88 ml10" name="rhp1">
						<option value="010"> 010 </option>
						<option value="011"> 011 </option>
						<option value="016"> 016 </option>
						<option value="017"> 017 </option>
						<option value="018"> 018 </option>
						<option value="019"> 019 </option>
						</select> - <input type="text" class="w88" name="rhp2" maxlength="4" /> - <input type="text"  maxlength="4" class="w88" name="rhp3" /> <span class="pl05">무통장입금 입금완료후 2일 이내 현금영수증이 발급됩니다.</span>
                                    </div>
                                    <div class="rec_box hide">
                                        <input type="radio" id="receipt02" name="so_gubun" value="1" class="ml10" name="receipt" /><label for="receipt02" class="ml05">사업자증빙용</label>
                                        <input type="text" class="w88" name="sa1" /> - <input type="text" class="w88" name="sa2" /> - <input type="text" name="sa3" class="w88" /> <span class="pl05">무통장입금 입금완료후 2일 이내 현금영수증이 발급됩니다.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="hide">
                                <th scope="row">무통장입금 이용안내</th>
                                <td>
                                    <ul class="t_list">
                                        <li>ㆍ주문접수 후 다음 날 24시 전까지 입금해 주시면, 결제가 완료됩니다.</li>
                                        <li>ㆍ입금대기 기간 내 입금되지 않거나 주문하신 상품이 판매종료 또는  매진될 경우 주문이 자동으로 취소됩니다.</li>
                                        <li>ㆍ무통장입금은 부분환불 및 부분취소가 불가능합니다. 취소하실 경우 전체취소 후 다시 주문을 진행해 주세요.</li>
                                        <li>ㆍ자동화 기기(CD/ATM) 이용 시에는 카드를 통해 이체해 주시기 바랍니다. 일부 기기에서 현금, 통장을 이용한 입금이 제한될 수 있습니다.)</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="req">배송시요구사항</label></th>
                                <td><textarea class="w708" name="msg" id="req" rows="3"></textarea></td>
                            </tr>
                        </table>
                        
                        <script type="text/javascript">
                        $(function(){
                            $('.nobank').click(function(){
                                $('.table_row tr.hide').css({'display':'table-row'});
                                $('.nobank').siblings().click(function(){
                                    $('.table_row tr.hide').css({'display':'none'});
                                });
                            });
                            $('.rec_input td > input').change(function(){
                                $(this).addClass('on');
                                $(this).siblings('input').removeClass('on');
                                
                                if($('#rec01').attr("class") =="on"){
                                    $('.rec_box').show();
                                }
                                if($('#rec01').attr("class") ==""){
                                    $('.rec_box').hide();
                                }
                            });
                        });
                        </script>
                        
                        <!-- btn_center -->
                        <div class="btn_center">
                            <a href="javascript:send(document.form1);">주문하기</a>
                        </div>
                        <!-- // btn_center -->
                    </fieldset>
                <input type=hidden name=log value="<? echo $member_log;?>">
                <input type=hidden name=total_cost value=<?echo $tgcost;?>>
                <input type=hidden name=payment value=<?echo $tgcost;?>>
                <input type=hidden name=point value="<? echo $hyunjae_point;?>">
				<input type=hidden name=cp>
                </form>
            </div>
        </div>
        <!-- // container -->
        
        
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>
    <!-- wrap -->
    
</body>
</html>
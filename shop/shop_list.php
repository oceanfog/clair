<?
if($buy_cookie==""){
	mt_srand((double)microtime() * 1000000);
	$ti=date('Ymdhis');
    $wow=uniqid(mt_rand(10,1000000));
	$kek=$wow.$ti;
	setcookie("buy_cookie","$kek",0,"/");
}

include "../master/db_connect.inc";
include "../master/function.php";

$row=@mysql_fetch_object(@mysql_query("select * from goods where sang_no=$sang_no"));

if($row->img1 !="") $arr_im[]=$row->img1;
if($row->img2 !="") $arr_im[]=$row->img2;
if($row->img3 !="") $arr_im[]=$row->img3;
if($row->img4 !="") $arr_im[]=$row->img4;
if($row->img5 !="") $arr_im[]=$row->img5;

//내가본상품 6시간이 지난 레코드 삭제한다...
$qq="delete from myview where (UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(regdate))/3600 > 6";
@mysql_query($qq);

$sang_photo = "../master/shop/sang_photo/";
if($gcount=="") $gcount=1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
    $(function(){
        $("input[name='amount01']").TouchSpin({
            postfix_extraclass: "btn"
        });
        $('.layer_open').click(function(){
            $('.basket_layer').fadeIn();
            $('.layer_close').click(function(){
                $('.basket_layer').fadeOut();
            });
        });
    });

</script>
<script type="text/javascript" src="./js/view.js"></script>

</head>
<body>
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
        
        <!-- container -->
        <div class="container">
            <div class="inner">
                <h2>구매하기</h2>
				<form name="vfrm" action="" method="post">
                <!-- shop -->
                <div class="shop">
                    <!-- photo -->
                    <div class="photo">
                        <img src="../master/shop/sang_photo/<?=$arr_im[0];?>" width="450" height="450" alt="" id="view2" class="big" />
                        <ul>
						<?
						for($i=0;$i<count($arr_im);$i++) {
							if($arr_im[$i]=="") continue;
						?>
                            <li><a href="javascript:im('../master/shop/sang_photo/<?=$arr_im[$i];?>','<?=$i;?>','<?=count($arr_im);?>');" id="wows" <?if($i==0) echo "class='current'";?>><img src="../master/shop/thumb/<?=$arr_im[$i];?>" width="48" height="48" alt="" /></a></li>
						<?}?>

                        </ul>
                    </div>
                    <!-- // photo -->
                    
                    <!-- pdt_info -->
				    <fieldset>
					<input type="hidden" name="sang_price" value="<?echo $row->mprice;?>">
					<input type="hidden" name="sang_no" value="<?echo $sang_no;?>">
					<input type="hidden" name="mem_id" value="<?echo $member_log;?>">
					<input type="hidden" name="uri" value="<?echo base64_encode($HTTP_REFERER);?>">
					<input type="hidden" name="color" value="<?=$row->color;?>" />
                    <div class="pdt_info">
                        <h4><?=$row->sang_name;?></h4>
                        <dl class="pdt_txt">
						<?if($row->opt1 !=""){?>
                            <dt>정격전압</dt>
                            <dd><?=$row->opt1;?>&nbsp;</dd>
						<?}?>
						<?if($row->opt2 !=""){?>
                            <dt>성능 및 소비전력</dt>
                            <dd><?=$row->opt2;?>&nbsp;</dd>
						<?}?>
						<?if($row->opt3 !=""){?>
                            <dt>권장사용장소</dt>
                            <dd><?=$row->opt3;?>&nbsp;</dd>
						<?}?>
						<?if($row->opt4 !=""){?>
                            <dt>크기</dt>
                            <dd><?=$row->opt4;?>&nbsp;</dd>
						<?}?>
						<?if($row->opt5 !=""){?>
                            <dt>무게</dt>
                            <dd><?=$row->opt5;?>&nbsp;</dd>
						<?}?>
                            <dt>가격</dt>
                            <dd><?=number_format($row->mprice);?> won</dd>
							<?if($row->color !=""){
								$arr_color=explode("/",$row->color);
								?>
                            <dt>색상</dt>

                            <dd>
                                <select name="colors">
                                    <option value="">선택</option>
									<?
									for($c=0;$c<count($arr_color);$c++) {
									echo"<option value='$arr_color[$c]'>$arr_color[$c]</option>";
								}
								?>
                                </select>&nbsp;
                            </dd>
							<?}?>
                            <dt>수량</dt>
                            <dd class="number">
							<select name="count" onChange="go_pr(document.vfrm);">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>

							</select>
							</dd>
                        </dl>
                        <dl class="total">
                            <dt>요금합계</dt>
                            <dd><span id="wow"><?=number_format($row->mprice);?> won</span></dd>
                        </dl>
                        <div class="btns">
                            <a href="javascript:jumoon_now(document.vfrm);" class="orange layer_open">바로구매</a>
                            <a href="javascript:jang(document.vfrm);" class="gray layer_open">장바구니</a>
                        </div>
                        
                        <!-- basket_layer -->
						<!--
                        <div class="basket_layer">
                            <p>장바구니에 등록되었습니다.<br />지금 바로 확인해 보시겠습니까? </p>
                            <div class="btns">
                                <a href="../shop/shop.php" class="gray02">장바구니</a>
                                <a href="#none" class="green layer_close">계속쇼핑하기</a>
                            </div>
                        </div>
						-->
                        <!-- // basket_layer -->
                        
                    </div>
                    <!-- // pdt_info -->
                </div>
                <!-- // shop -->
					</form>
					<?=stripslashes($row->content);?>
					<!-- movie_img 삭제 금지 주석처리 바람 -->
					<!--
                    <div class="movie_img">
                        <img src="../images/content/TD1866_1100.jpg" alt="" />
                        <div class="m_obj">
                            <object width="706" height="432">
                            <param name="movie" value="http://www.youtube.com/v/UPdmCuN4jKE?version=2&amp;loop=1&amp;enablejsapi=1&amp;playerapiid=ytplayer&amp;fs=1&amp;hd=1">
                            <param name="allowFullScreen" value="true">
                            <param name="allowscriptaccess" value="always">
                            <param name="windowlessVideo" value="true">
                            <embed src="http://www.youtube.com/v/UPdmCuN4jKE?version=2&amp;loop=1&amp;enablejsapi=1&amp;playerapiid=ytplayer&amp;fs=1&amp;hd=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="706" height="432" wmode="transparent">
                            </object>
                        </div>
                    </div>
					-->
                    <!-- // movie_img -->

                <!-- service -->
                <div class="service">
                    <div class="inner">
                        <div class="box">
                            <h2>고객 서비스센터(AS문의)</h2>
                            <p><span>0</span><span>8</span><span>0</span>-<span>5</span><span>8</span><span>1</span>-<span>2</span><span>9</span><span>1</span><span>7</span></p>
                            <h3>구매정보</h3>
                            <ul>
                                <li>1인당 구매가능수량: 제한없음</li>
                                <li>택배사: 우체국택배</li>
                                <li>배송비: 무료배송 1개까지 묶음배송 가능</li>
                                <li>배송방식: 빠른배송</li>
                                <li>빠른배송 일일 마감시간: 매일 오후12시까지</li>
                                <li>출고이리 구매시부터 익일 순차발송 (주말, 공휴일 및 택배사 사정에 따라 지연될 수 있습니다 / 빠른 배송을 위해 발송마감 시간 후 취소를 하실 수 없습니다.)</li>
                            </ul>
                            <h3>안내사항</h3>
                            <ul>
                                <li>수령일을 포함하여 7일 이후 교환, 환불이 불가합니다.</li>
                                <li>고객 변심에 의한 환불의 경우 왕복 택배비 6,000원은 고객이 부담합니다.</li>
                                <li>상품의 개봉으로 또는 고객의 부주의로 인하여 상품가치 훼손시에는 교환, 반품이 불가합니다.</li>
                                <li>제품불량시 1년 무상 A/S를 보증해드리며 초기불량 및 사용중에 문제가 생기는 경우에는 국가에서 운영하는 중소기업 A/S센터에서 책임지고 처리해 드립니다.</li>
                                <li>전자제품 특성상 개봉후 교환/반품이 불가합니다,(불량제외)</li>
                                <li>도서, 산간지역은 배송비가 추가될 수 있습니다.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- // service -->
                
            </div>
        </div>
        <!-- // container -->
        
        
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>
    <!-- wrap -->
    
</body>
</html>
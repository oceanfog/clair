<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<!--주문상품 보기 시작-->
<?
include("../master/db_connect.inc");
include("../master/function.php");

$sang_photo = "../master/shop/thumb/";

?>

<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
	function KeyCodeCheck(){
		if ((event.keyCode < 48)||(event.keyCode > 57)) event.returnValue=false;
	}

function mail_ck(frm) {
	if(frm.mail2.value=="etc") {
		document.getElementById('mail3').readOnly=false; 
		frm.mail3.value="";

	} else {
		document.getElementById('mail3').readOnly=true;
		frm.mail3.value=frm.mail2.value;
	}

}

function nzipopen(){
  window.open("./search_zipcd.php?mode=zipin3","zipcode","width=520,height=320,left=300,top=300,toolbar=no,location=no,directories=no ,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no");
   }
function nzipopen2(){
  window.open("./search_zipcd2.php?mode=zipin3","zipcode","width=520,height=320,left=300,top=300,toolbar=no,location=no,directories=no ,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no");
   }


</script>
</script>
</head>
<body>
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
        
        <!-- container -->
        <div class="container">
            <div class="inner">
                
                <h2>무통장입금안내</h2>
                <h3>주문상품</h3>
                <form name="form1">
                    <fieldset>
                        <legend>주문상품</legend>
                        <table summary="주문상품 목록입니다. 각 항목으로는 선택, 상품옵션정보, 총 상품금액, 배송비가 있습니다.">
                            <caption>주문상품 목록</caption>
                            <colgroup>
                                <col width="*" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">상품옵션정보</th>
                                    <th scope="col">총 상품금액</th>
                                    <th scope="col">수량</th>
                                </tr>
                            </thead>
                            <tbody>
<?
		$sql="select * from sales where sid='$sid'";
		$result = @mysql_query($sql);
		$total = @mysql_affected_rows();	
		$tscost=0;
		for ($i=0;$i<$total;$i++){
			$row = mysql_fetch_object($result);
			$price=$row->scost;
			$qq="select * from goods where sang_no=$row->sang_no";
			$rr=mysql_query($qq);
			$rrr=mysql_fetch_object($rr);
		//이미지 없을때 있을때...
		if(file_exists($sang_photo.$rrr->img1) && $rrr->img1 !="") {
			$filename=$sang_photo.$rrr->img1;
		} else {
			$filename=$sang_photo."not_img_small.gif";
		}

		/*
		if($row->item_name !="") $add_item="<br><span class='additem'>$row->item_name : ".number_format($row->item_price)."원</span>";
		else $add_item="";
		$item_price+=intval($row->item_price);

		$point=intval($row->scost) * $row->scount * ($rrr->point / 100);
		$all_point+=$point;
		*/

?>
                                <tr>
                                    <td class="option">
                                        <img src="../master/shop/thumb/<?=$rrr->img1;?>" width="85" height="85" alt="" />
                                        <dl>
                                            <dt><?=$row->sang_name;?></dt>
                                            <dd><?=number_format($row->scost);?>원 <?=$imc;?></dd>
                                        </dl>
                                    </td>
                                    <td><?=number_format(($row->scost*$row->scount),0); ?>원</td>
                                    <td><?=$row->scount;?>개</td>
                                </tr>
<?
$cid=$cid+1;
$tpoint+=$rrr->point * $row->scount;
$tgcost=$tgcost+($row->scost*$row->scount+$row->item_price);
		}
//배송료 설정
//if($member_gubun=="e" || $member_gubun=="v") $charges=0;
//else $charges=2500;
if($tgcost > 30000) $charges=0;
else $charges=0;

$real_payment=$tgcost + $charges - $use_point - $cp_money;
?>
                            </tbody>
                        </table>
                        
                        <h3 class="mt45">구매자정보</h3>
                        <table class="table_row" summary="구매자정보입니다. 각 항목으로는 주문자, 휴대폰, 이메일, 주문 비밀번호, 주문비밀번호 확인, 주소가 있습니다.">
                            <caption>구매자정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="buyer">주문자</label></th>
                                <td><?=$row->gu_name;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone">휴대폰</label></th>
                                <td><?=$row->gu_hp;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="mail1">이메일</label></th>
                                <td><?=$row->gu_email;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr">주소</label></th>
                                <td><?=$row->gu_zip1."-".$row->gu_zip2." ".$row->gu_address1." ".$row->gu_address2;?></td>
                            </tr>
                        </table>
                        
                        <h3 class="mt45">받는분정보</h3>
                        <table class="table_row" summary="받는분정보입니다. 각 항목으로는 받을사람 이름, 휴대폰, 주소가 있습니다.">
                            <caption>받는분정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="buyer02">받을사람 이름</label></th>
                                <td><?=$row->su_name;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone02">휴대폰</label></th>
                                <td><?=$row->su_hp;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr02">주소</label></th>
                                <td><?=$row->zip1."-".$row->zip2." ".$row->address1." ".$row->address2;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="req">배송시요구사항</label></th>
                                <td><?=$row->msg;?></td>
                            </tr>
                        </table>

                        
                        <h3 class="mt45">결제정보</h3>
                        <table class="table_row" summary="결제정보 목록입니다. 각 항목으로는 주문금액, 배송비, 결재하실 금액이 있습니다.">
                            <caption>결제정보 목록</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">결재금액</th>
                                <td><?=number_format($real_payment);?>원</td>
                            </tr>
                            <tr>
                                <th scope="row">결재방식</th>
                                <td><?=$row->pay_type;?></td>
                            </tr>
                            </tbody>
                        </table>
                        <?if($row->pay_type=="에스크로결제"){?>
                        <h3 class="mt45">가상계좌정보</h3>
                        <table class="table_row" summary="결제방식입니다. 각 항목으로는 입금은행, 현금영수증, 무통장입금 이용안내가 있습니다.">
                            <caption>결제방식</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="bank">입금은행</label></th>
                                <td><?=ck_bank_code($row->imsi_bank_name);?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="bank">가상계좌번호</label></th>
                                <td><?=$row->imsi_bank;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="b_name">입금기한</label></th>
                                <td><?=substr($row->imsi_bank_date,0,4)."-".substr($row->imsi_bank_date,4,2)."-".substr($row->imsi_bank_date,6,2);?> 일까지</td>
                            </tr>



                        </table>
						<?}?>
                        
                        <script type="text/javascript">
                        $(function(){
                            $('.nobank').click(function(){
                                $('.table_row tr.hide').css({'display':'table-row'});
                                $('.nobank').siblings().click(function(){
                                    $('.table_row tr.hide').css({'display':'none'});
                                });
                            });
                            $('.rec_input td > input').change(function(){
                                $(this).addClass('on');
                                $(this).siblings('input').removeClass('on');
                                
                                if($('#rec01').attr("class") =="on"){
                                    $('.rec_box').show();
                                }
                                if($('#rec01').attr("class") ==""){
                                    $('.rec_box').hide();
                                }
                            });
                        });
                        </script>
                        
                        <!-- btn_center -->
                        <div class="btn_center">
                            <a href="../buy/buy.php">계속쇼핑</a>
                        </div>
                        <!-- // btn_center -->
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- // container -->
        
        
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>
    <!-- wrap -->
    
</body>
</html>
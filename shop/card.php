<?
session_start(); 					//주의:파일 최상단에 위치시켜주세요!!

include("../master/db_connect.inc");
include("../master/function.php");

$qq="select * from imsi where cook='$buy_cookie'";
$re=@mysql_query($qq);
$to=@mysql_affected_rows();


/**회원으로주문한경우와 아닌경우 level 설정**/
if($member_log!="") {
	$level='1';
	$guest=$member_log;

}
if($member_log=="") {
	$level='2';
	$guest='고객';
}

$qs="select * from imsi where cook='$buy_cookie'";
$rel=@mysql_query($qs);
$total=mysql_affected_rows();




$gu_phone=$tel1."-".$tel2."-".$tel3;
$su_phone=$stel1."-".$stel2."-".$stel3;
$gu_email=$mail1."@".$mail3;
$su_email=$smail1."@".$smail3;
$gu_hp=$hp1."-".$hp2."-".$hp3;
$su_hp=$shp1."-".$shp2."-".$shp3;



if($total_cost > 30000) $ccc=0;
else $ccc=0;
$rep=$total_cost -($remain_point + $cp_money) + $ccc;
?>
<?php
/* INIsecurepaystart.php
 *
 * 이니페이 웹페이지 위변조 방지기능이 탑재된 결제요청페이지.
 * 코드에 대한 자세한 설명은 매뉴얼을 참조하십시오.
 * <주의> 구매자의 세션을 반드시 체크하도록하여 부정거래를 방지하여 주십시요.
 *
 * http://www.inicis.com
 * Copyright (C) 2006 Inicis Co., Ltd. All rights reserved.
 */

    /****************************
     * 0. 세션 시작				*
    ****************************/

    /**************************
     * 1. 라이브러리 인클루드 *
     **************************/
    require("../INIpay50/libs/INILib.php");

    /***************************************
     * 2. INIpay50 클래스의 인스턴스 생성  *
     ***************************************/
    $inipay = new INIpay50;

    /**************************
     * 3. 암호화 대상/값 설정 *
     **************************/
    $inipay->SetField("inipayhome", "/ydata/user/econshop_co_kr/public_html/INIpay50");       // 이니페이 홈디렉터리(상점수정 필요)
    $inipay->SetField("type", "chkfake");      // 고정 (절대 수정 불가)
    $inipay->SetField("debug", "true");        // 로그모드("true"로 설정하면 상세로그가 생성됨.)
    $inipay->SetField("enctype","asym"); 			//asym:비대칭, symm:대칭(현재 asym으로 고정)
    /**************************************************************************************************
     * admin 은 키패스워드 변수명입니다. 수정하시면 안됩니다. 1111의 부분만 수정해서 사용하시기 바랍니다.
     * 키패스워드는 상점관리자 페이지(https://iniweb.inicis.com)의 비밀번호가 아닙니다. 주의해 주시기 바랍니다.
     * 키패스워드는 숫자 4자리로만 구성됩니다. 이 값은 키파일 발급시 결정됩니다.
     * 키패스워드 값을 확인하시려면 상점측에 발급된 키파일 안의 readme.txt 파일을 참조해 주십시오.
     **************************************************************************************************/
	$inipay->SetField("admin", "1111"); 				// 키패스워드(키발급시 생성, 상점관리자 패스워드와 상관없음)
    $inipay->SetField("checkopt", "false"); 		//base64함:false, base64안함:true(현재 false로 고정)

		//필수항목 : mid, price, nointerest, quotabase
		//추가가능 : INIregno, oid
		//*주의* : 	추가가능한 항목중 암호화 대상항목에 추가한 필드는 반드시 hidden 필드에선 제거하고 
		//          SESSION이나 DB를 이용해 다음페이지(INIsecureresult.php)로 전달/셋팅되어야 합니다.
    

	if($pay_type=="4") {
		//$inipay->SetField("mid", "iniescrow0");            // 상점아이디
		$inipay->SetField("mid", "IESatnsgro");            // 상점아이디
	} else {
		//$inipay->SetField("mid", "INIpayTest");            // 상점아이디
		$inipay->SetField("mid", "atnsgroup0");            // 상점아이디
	}

    $inipay->SetField("price", "$rep");                // 가격
    $inipay->SetField("nointerest", "no");             //무이자여부(no:일반, yes:무이자)
    $inipay->SetField("quotabase", "lumpsum:00:02:03:06");//할부기간

    /********************************
     * 4. 암호화 대상/값을 암호화함 *
     ********************************/
    $inipay->startAction();

    /*********************
     * 5. 암호화 결과  *
     *********************/
 		if( $inipay->GetResult("ResultCode") != "00" ) 
		{
			echo $inipay->GetResult("ResultMsg");
			exit(0);
		}

    /*********************
     * 6. 세션정보 저장  *
     *********************/


	if($pay_type=="4") {
		$HTTP_SESSION_VARS['INI_MID'] = "IESatnsgro";	//상점ID
		//$HTTP_SESSION_VARS['INI_MID'] = "iniescrow0";	//상점ID

	} else {
		$HTTP_SESSION_VARS['INI_MID'] = "atnsgroup0";	//상점ID
		//$HTTP_SESSION_VARS['INI_MID'] = "INIpayTest";	//상점ID
	}

		//$HTTP_SESSION_VARS['INI_MID'] = "iniescrow0";	//상점ID
		$HTTP_SESSION_VARS['INI_ADMIN'] = "1111";			// 키패스워드(키발급시 생성, 상점관리자 패스워드와 상관없음)
		$HTTP_SESSION_VARS['INI_PRICE'] = "$rep";     //가격 
		$HTTP_SESSION_VARS['INI_RN'] = $inipay->GetResult("rn"); //고정 (절대 수정 불가)
		$HTTP_SESSION_VARS['INI_ENCTYPE'] = $inipay->GetResult("enctype"); //고정 (절대 수정 불가)
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>

<script language=javascript src="http://plugin.inicis.com/pay61_secuni_cross.js"></script>
<script language=javascript>
StartSmartUpdate();
</script>
<!-------------------------------------------------------------------------------
* 웹SITE 가 https를 이용하면 https://plugin.inicis.com/pay61_secunissl_cross.js 사용 
* 웹SITE 가 Unicode(UTF-8)를 이용하면 http://plugin.inicis.com/pay61_secuni_cross.js 사용
* 웹SITE 가 https, unicode를 이용하면 https://plugin.inicis.com/pay61_secunissl_cross.js 사용  
-------------------------------------------------------------------------------->
<!---------------------------------------------------------------------------------- 
※ 주의 ※
 상단 자바스크립트는 지불페이지를 실제 적용하실때 지불페이지 맨위에 위치시켜 
 적용하여야 만일에 발생할수 있는 플러그인 오류를 미연에 방지할 수 있습니다.
 
  <script language=javascript src="http://plugin.inicis.com/pay61_secuni_cross.js"></script>
  <script language=javascript>
  StartSmartUpdate();	// 플러그인 설치(확인)
  </script>
-----------------------------------------------------------------------------------> 


<script language=javascript>

var openwin;

function pay(frm)
{
	// MakePayMessage()를 호출함으로써 플러그인이 화면에 나타나며, Hidden Field
	// 에 값들이 채워지게 됩니다. 일반적인 경우, 플러그인은 결제처리를 직접하는 것이
	// 아니라, 중요한 정보를 암호화 하여 Hidden Field의 값들을 채우고 종료하며,
	// 다음 페이지인 INIsecureresult.php로 데이터가 포스트 되어 결제 처리됨을 유의하시기 바랍니다.

	if(document.ini.clickcontrol.value == "enable")
	{
		
		if(document.ini.goodname.value == "")  // 필수항목 체크 (상품명, 상품가격, 구매자명, 구매자 이메일주소, 구매자 전화번호)
		{
			alert("상품명이 빠졌습니다. 필수항목입니다.");
			return false;
		}
		else if(document.ini.buyername.value == "")
		{
			alert("구매자명이 빠졌습니다. 필수항목입니다.");
			return false;
		} 
		else if(document.ini.buyeremail.value == "")
		{
			alert("구매자 이메일주소가 빠졌습니다. 필수항목입니다.");
			return false;
		}
		else if(document.ini.buyertel.value == "")
		{
			alert("구매자 전화번호가 빠졌습니다. 필수항목입니다.");
			return false;
		}
		else if( ( navigator.userAgent.indexOf("MSIE") >= 0 || navigator.appName == 'Microsoft Internet Explorer' ) &&  (document.INIpay == null || document.INIpay.object == null) )  // 플러그인 설치유무 체크
		{
			alert("\n이니페이 플러그인 128이 설치되지 않았습니다. \n\n안전한 결제를 위하여 이니페이 플러그인 128의 설치가 필요합니다. \n\n다시 설치하시려면 Ctrl + F5키를 누르시거나 메뉴의 [보기/새로고침]을 선택하여 주십시오.");
			return false;
		}
		else
		{
			/******
			 * 플러그인이 참조하는 각종 결제옵션을 이곳에서 수행할 수 있습니다.
			 * (자바스크립트를 이용한 동적 옵션처리)
			 */
			
						 
			if (MakePayMessage(frm))
			{
				disable_click();
				//openwin = window.open("childwin.html","childwin","width=299,height=149");		
				return true;
			}
			else
			{
				if( IsPluginModule() )     //plugin타입 체크
   				{
					alert("결제를 취소하셨습니다.");
				}
				return false;
			}
		}
	}
	else
	{
		return false;
	}
}


function enable_click()
{
	document.ini.clickcontrol.value = "enable"
}

function disable_click()
{
	document.ini.clickcontrol.value = "disable"
}

function focus_control()
{
	if(document.ini.clickcontrol.value == "disable")
		openwin.focus();
}
</script>

</head>
<body onload="javascript:enable_click()" onFocus="javascript:focus_control()">
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
<form name="ini" method="post" action="../INIpay50/sample/INIsecureresult.php" onSubmit="return pay(this)"> 

        <!-- container -->
        <div class="container">
            <div class="inner">
                
                <h2>주문/결제</h2>
                <h3>주문상품</h3>
                    <fieldset>
                        <legend>주문상품</legend>
                        <table summary="주문상품 목록입니다. 각 항목으로는 선택, 상품옵션정보, 총 상품금액, 배송비가 있습니다.">
                            <caption>주문상품 목록</caption>
                            <colgroup>
                                <col width="*" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">상품옵션정보</th>
                                    <th scope="col">총 상품금액</th>
                                    <th scope="col">수량</th>
                                </tr>
                            </thead>
                            <tbody>
<?
						/** 쇼핑 카트 정보 출력 **/
						$sang_photo="../master/shop/thumb/";
						$cid=0;
						$tgcost=0;
						$tpoint=0;
						$qs="select * from imsi where cook='$buy_cookie'";
						$rel=@mysql_query($qs) or die('db error');
						$total=@mysql_affected_rows();

						for($i=0;$i<$total;$i++){
						    $ro=@mysql_fetch_object($rel);
							$sql = "select * from goods where sang_no = $ro->imsi_sang_no";
							$result = @mysql_query($sql) or die('query error');
							
							$row = mysql_fetch_object($result);
							//이미지 없을때 있을때...
							if(file_exists($sang_photo.$row->img1) && $row->img1 !="") {
								$filename=$sang_photo.$row->img1;
							} else {
								$filename=$sang_photo."not_img_small.gif";
							}
							$price=$ro->imsi_sang_price;

		//추가아이템 
		if($ro->item_name !="") $add_item="<br><span class='additem'>$ro->item_name : ".number_format($ro->item_price)."원</span>";
		else $add_item="";
		$item_price+=intval($ro->item_price);

		//적립금
		$point=intval($price) * $ro->imsi_gcount * ($row->point / 100);
		$all_point+=$point;

							if($ro->imsi_sang_color !="") $imc=" / ".$ro->imsi_sang_color;
							else $imc="";
?>



                                <tr>
                                    <td class="option">
                                        <img src="../master/shop/thumb/<?=$row->img1;?>" width="85" height="85" alt="" />
                                        <dl>
                                            <dt><?=$row->sang_name;?></dt>
                                            <dd><?=number_format($ro->imsi_sang_price);?>원 <?=$imc;?></dd>
                                        </dl>
                                    </td>
                                    <td><?=number_format(($price*$ro->imsi_gcount),0); ?>원</td>
                                    <td><?=$ro->imsi_gcount;?>개</td>
                                </tr>
<?
$cid=$cid+1;
$tpoint+=$row->point * $ro->imsi_gcount;
$tgcost=$tgcost+($price*$ro->imsi_gcount)+$ro->item_price;
		}
//배송료 설정
//if($member_gubun=="e" || $member_gubun=="v") $charges=0;
//else $charges=2500;
if($tgcost > 30000) $charges=0;
else $charges=0;

$real_payment=$tgcost + $charges - $remain_point -$cp_money;
?>
                            </tbody>
                        </table>
                        
                        <h3 class="mt45">구매자정보</h3>
                        <table class="table_row" summary="구매자정보입니다. 각 항목으로는 주문자, 휴대폰, 이메일, 주문 비밀번호, 주문비밀번호 확인, 주소가 있습니다.">
                            <caption>구매자정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tr>
                                <th scope="row"><label for="buyer">주문자</label></th>
                                <td><?=$gu_name;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone">휴대폰</label></th>
                                <td><?=$gu_hp;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="mail1">이메일</label></th>
                                <td><?=$gu_email;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr">주소</label></th>
                                <td><?=$czip1."-".$cip2." ".$caddr1." ".$caddr2;?></td>
                            </tr>
                        </table>
                        
                        <h3 class="mt45">받는분정보</h3>
                        <table class="table_row" summary="받는분정보입니다. 각 항목으로는 받을사람 이름, 휴대폰, 주소가 있습니다.">
                            <caption>받는분정보</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                             <tr>
                                <th scope="row"><label for="buyer02">받을사람 이름</label></th>
                                <td><?=$su_name;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone02">휴대폰</label></th>
                                <td><?=$su_hp;?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="addr02">주소</label></th>
                                <td><?=$zip1."-".$zip2." ".$address1." ".$address2;?></td>
                            </tr>
                        </table>

                        
                        <h3 class="mt45">결제정보</h3>
                        <table class="table_row" summary="결제정보 목록입니다. 각 항목으로는 주문금액, 배송비, 결재하실 금액이 있습니다.">
                            <caption>결제정보 목록</caption>
                            <colgroup>
                                <col width="20%" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th scope="row">결재금액</th>
                                <td><?=number_format($real_payment);?>원</td>
                            </tr>

                            <tr>
                                <th scope="row">결재방식</th>
                                <td>				<?
				if($pay_type=="3") {
					echo"신용카드";
					$gopaymethod="Card";
			  }
				else if($pay_type=="2") {
					echo"실시간계좌이체";
					$gopaymethod="DirectBank";
				}
				else if($pay_type=="4") {
					echo"에스크로 가상계좌";
					$gopaymethod="VBank";
				}
				?></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="req">배송시요구사항</label></th>
                                <td><?=$msg;?></td>
                            </tr>
                            </tbody>
                        </table>
                        
                        <script type="text/javascript">
                        $(function(){
                            $('.nobank').click(function(){
                                $('.table_row tr.hide').css({'display':'table-row'});
                                $('.nobank').siblings().click(function(){
                                    $('.table_row tr.hide').css({'display':'none'});
                                });
                            });
                            $('.rec_input td > input').change(function(){
                                $(this).addClass('on');
                                $(this).siblings('input').removeClass('on');
                                
                                if($('#rec01').attr("class") =="on"){
                                    $('.rec_box').show();
                                }
                                if($('#rec01').attr("class") ==""){
                                    $('.rec_box').hide();
                                }
                            });
                        });
                        </script>
                        
                        <!-- btn_center -->
                        <div class="btn_center">
                            <!--a href="#none">결제하기</a-->
							<input type="image" src="../images/btn_pay.gif" border="0"   />
                        </div>
                        <!-- // btn_center -->
                    </fieldset>
                
            </div>
        </div>
        <!-- // container -->
        
        
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>



    <!-- wrap -->
<!-- 기타설정 -->
<input type=hidden name=currency size=20 value="WON">

<!--
SKIN : 플러그인 스킨 칼라 변경 기능 - 6가지 칼라(ORIGINAL, GREEN, ORANGE, BLUE, KAKKI, GRAY)
HPP : 컨텐츠 또는 실물 결제 여부에 따라 HPP(1)과 HPP(2)중 선택 적용(HPP(1):컨텐츠, HPP(2):실물).
Card(0): 신용카드 지불시에 이니시스 대표 가맹점인 경우에 필수적으로 세팅 필요 ( 자체 가맹점인 경우에는 카드사의 계약에 따라 설정) - 자세한 내용은 메뉴얼  참조.
OCB : OK CASH BAG 가맹점으로 신용카드 결제시에 OK CASH BAG 적립을 적용하시기 원하시면 "OCB" 세팅 필요 그 외에 경우에는 삭제해야 정상적인 결제 이루어짐.
no_receipt : 은행계좌이체시 현금영수증 발행여부 체크박스 비활성화 (현금영수증 발급 계약이 되어 있어야 사용가능)
-->
<input type=hidden name=acceptmethod size=20 value="HPP(1):Card(0):OCB:receipt:cardpoint">


<!--
상점 주문번호 : 무통장입금 예약(가상계좌 이체),전화결재 관련 필수필드로 반드시 상점의 주문번호를 페이지에 추가해야 합니다.
결제수단 중에 은행 계좌이체 이용 시에는 주문 번호가 결제결과를 조회하는 기준 필드가 됩니다.
상점 주문번호는 최대 40 BYTE 길이입니다.
주의:절대 한글값을 입력하시면 안됩니다.
-->
<input type=hidden name=oid size=40 value="oid_1234567890">


<!--
플러그인 좌측 상단 상점 로고 이미지 사용
이미지의 크기 : 90 X 34 pixels
플러그인 좌측 상단에 상점 로고 이미지를 사용하실 수 있으며,
주석을 풀고 이미지가 있는 URL을 입력하시면 플러그인 상단 부분에 상점 이미지를 삽입할수 있습니다.
-->
<!--input type=hidden name=ini_logoimage_url  value="http://[사용할 이미지주소]"-->

<!--
좌측 결제메뉴 위치에 이미지 추가
이미지의 크기 : 단일 결제 수단 - 91 X 148 pixels, 신용카드/ISP/계좌이체/가상계좌 - 91 X 96 pixels
좌측 결제메뉴 위치에 미미지를 추가하시 위해서는 담당 영업대표에게 사용여부 계약을 하신 후
주석을 풀고 이미지가 있는 URL을 입력하시면 플러그인 좌측 결제메뉴 부분에 이미지를 삽입할수 있습니다.
-->
<!--input type=hidden name=ini_menuarea_url value="http://[사용할 이미지주소]"-->

<!--
플러그인에 의해서 값이 채워지거나, 플러그인이 참조하는 필드들
삭제/수정 불가
uid 필드에 절대로 임의의 값을 넣지 않도록 하시기 바랍니다.
-->
<?if($pay_type=="4"){?>
<input type=hidden name=acceptmethod value="SKIN(ORIGINAL):HPP(1): useescrow ">
<?}?>
<input type=hidden name=ini_encfield value="<?php echo($inipay->GetResult("encfield")); ?>" />
<input type=hidden name=ini_certid value="<?php echo($inipay->GetResult("certid")); ?>" />
<input type=hidden name=quotainterest value="" />
<input type=hidden name=paymethod value="" />
<input type=hidden name=cardcode value="" />
<input type=hidden name=cardquota value="" />
<input type=hidden name=rbankcode value="" />
<input type=hidden name=reqsign value="DONE" />
<input type=hidden name=encrypted value="" />
<input type=hidden name=sessionkey value="" />
<input type=hidden name=uid value="" /> 
<input type=hidden name=sid value="" />
<input type=hidden name=version value=4000 />
<input type=hidden name=clickcontrol value="" />



<?
if($remain_point=="") $remain_point=0;
?>
<input type="hidden" name="gopaymethod" value="<?echo $gopaymethod;?>" />
<input type="hidden" name="goodname" value="<?echo $row->sang_name;?>" />
<input type="hidden" name="buyername" value="<?echo $gu_name;?>" />
<input type="hidden" name="buyeremail" value="<?echo $gu_email;?>" />
<input type="hidden" name="buyertel" value="<?echo $gu_hp;?>" />
<input type="hidden" name="gu_phone" value="<?echo $gu_hp;?>" />
<input type="hidden" name="su_phone" value="<?echo $su_hp;?>" />
<input type="hidden" name="gu_email" value="<?echo $gu_email;?>" />
<input type="hidden" name="su_email" value="<?echo $gu_email;?>" />
<input type="hidden" name="gu_name" value="<?echo $gu_name;?>" />
<input type="hidden" name="su_name" value="<?echo $su_name;?>" />

<input type="hidden" name="use_point" value="<?echo $remain_point;?>" />
<input type="hidden" name="pass" value="<?echo $pass;?>" />
<input type="hidden" name="gu_hp" value="<?echo $gu_hp;?>" />
<input type="hidden" name="su_hp" value="<?echo $su_hp;?>" />
<input type="hidden" name="gu_zip1" value="<?echo $czip1;?>" />
<input type="hidden" name="gu_zip2" value="<?echo $czip2;?>" />
<input type="hidden" name="gu_address1" value="<?echo $caddr1;?>" />
<input type="hidden" name="gu_address2" value="<?echo $caddr2;?>" />
<input type="hidden" name="address1" value="<?echo $addr1;?>" />
<input type="hidden" name="address2" value="<?echo $addr2;?>" />
<input type="hidden" name="zip1" value="<?echo $zip1;?>" />
<input type="hidden" name="zip2" value="<?echo $zip2;?>" />
<input type="hidden" name="real_payment" value="<?echo $total_cost;?>" />
<input type="hidden" name="id" value="<?echo $member_log;?>" />
<input type="hidden" name="cook" value="<?echo $buy_cookie;?>" />
<input type="hidden" name="cp_money" value="<?echo $cp_money;?>" />
<input type="hidden" name="cp" value="<?echo $cp;?>" />
<input type="hidden" name="pay_type" value="<?echo $pay_type;?>" />
<input type="hidden" name="msg" value="<?echo $msg;?>" />

</form>
</body>
</html>
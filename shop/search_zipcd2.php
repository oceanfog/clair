<!DOCTYPE html>
<?
    function HTTP_Post($URL,$data) { // 소켓통신함수 , CURL로 구현해도 됩니다.
        $URL_Info=parse_url($URL);
        if(!empty($data)) foreach($data AS $k => $v) $str .= urlencode($k).'='.urlencode($v).'&';
        $path = $URL_Info["path"];
        $host = $URL_Info["host"];
        $port = $URL_Info["port"];
        if (empty($port)) $port=80;
        $result = "";
        $fp = fsockopen($host, $port, $errno, $errstr, 30);
        $http  = "POST $path HTTP/1.0\r\n";
        $http .= "Host: $host\r\n";
        $http .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $http .= "Content-length: " . strlen($str) . "\r\n";
        $http .= "Connection: close\r\n\r\n";
        $http .= $str . "\r\n\r\n";
        fwrite($fp, $http);
        while (!feof($fp)) { $result .= fgets($fp, 4096); }
        fclose($fp);
        return $result;
    }



?>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>우편번호검색</title>
<!--[if lt IE 9]>
    <script src="../js/html5.js"></script>
<![endif]-->
<style>
body{margin:0;padding:0;font-family:'dotum',dotum,Helvetica,sans-serif;text-align:left; background:#fff;  width:100%;}
div,ul,ol,li,dl,dt,dd,p,input,textarea,select,h1,h2,h3,h4,em,address,fieldset,form,iframe,object{margin:0;padding:0;}
li{list-style-type:none}
a, a:hover, a:active{text-decoration:none}

h1{height:50px; line-height:50px; font-size:20px; letter-spacing:-1px; color:#fff; font-weight:bold; background:#383838; padding-left:20px;}
.popwrap{position:relative; padding:20px;}
.popwrap .inner{margin:10px 0 0 0; padding:15px 30px; border:1px solid #ccc;}
.popwrap .inner p{margin:0 0 12px 0; color:#8a8a8a; line-height:20px; font-size:12px;}
.popwrap input[type="text"]{width:160px; padding: 2px 0 2px 5px; height:20px; line-height:20px; vertical-align:top; border:1px solid #d6d6d6; font-size:11px; font-family:Dotum; color:#8d8d8d; background:#f9f9f9;}
.popwrap select{border:1px solid #d6d6d6; width:120px; padding: 2px 0 2px 5px;}
.popwrap .inner a{vertical-align:top;display: inline-block;margin: 0 0 0 -4px;}
.popwrap .txt{width:100%; margin:15px 0 0 0; padding:0 0 5px 0; font-size:11px; border-bottom:1px solid #717171;}
.popwrap li a{font-size:12px; color:#777; line-height:26px; }
.popwrap li a:hover{font-size:12px; color:#777; line-height:26px; text-decoration:underline;}
.popwrap span{position:relative; width:100px; height:20px; }
.popwrap span a{display:inline-block;  font-size:12px; color:#fff; line-height:20px; background:#8c8c8c; padding: 3px 5px 3px 5px;}
</style>
<script type="text/javascript">
function go_zip(frm) {
	if(frm.search.value=="") {
		alert('검색을 원하시는 주소의 동/읍/면/리 을 입력하세요');
		frm.search.focus();
		return;
	}
	frm.submit();
}

function k_insert() 
{
	var k = document.k1;
	var mysel = k.show_addr.value;
	mysel = mysel.split("|");
	mysel1 = mysel[0];
	mysel2 = mysel1.split("-");
	
	code1 = mysel2[0];
	code2 = mysel2[1];
	
	addr = mysel[1];
	addr2 = mysel[2];
	
	opener.document.form1.zip1.value = code1;
	opener.document.form1.zip2.value = code2;
	opener.document.form1.addr1.value = addr;
	opener.document.form1.addr2.value = addr2;
	opener.document.form1.addr2.focus();
	self.close();
}
</script>
</head>
<body>
<h1>우편번호 찾기</h1>
<form name="k1"  action="<?=$PHP_SELF;?>" method="get">
<div class="popwrap">
	<div class="inner">
		<p>찾고자 하는 주소의 동(읍,면,동)을 정확히 입력해 주세요.<br />
			구지번 검색시 가산동 371-50" 식으로 동/번지입력.</p>
			<select id="choice" name='cate'>
				<option value="A" <? if ($cate == "A") {echo "checked"; } ?>>구지번</option>
				<option value="B" <? if ($cate == "B") {echo "checked"; } ?>>도로명+건물명</option>
			</select>
		<input type="text" name="search" title="검색어를 입력하세요." />
		<span><a href="javascript:go_zip(document.k1);">search</a></span> </div>

	

		<?
			$url  = "http://post.phpschool.com/json.phps.kr";

			if($cate=="B") {
				//$data = array("addr"=>$search);  
				$data = array("addr"=>$search, "ipkey"=>"4634043", "type"=>"new");
			} else if($cate=="A") {
				//$data = array("addr"=>$search, "type"=>"old");
				$data = array("addr"=>$search, "ipkey"=>"4634043", "type"=>"old");
			}
			$output = (HTTP_Post($url, $data));
			$output = substr($output, strpos($output,"\r\n\r\n")+4);

			$json = json_decode($output);
			echo "결과".$json->result;
			if ($json->result > 0) {
				echo"<p class='txt'>아래 해당하는 주소를 선택해 주세요.</p>";
				/*
				echo "검색건수 : {$json->result}\n";
				echo "검색시간 : {$json->time}\n";
				echo "조회횟수 : {$json->cnt}\n";
				echo "조회한도 : {$json->maxcnt}\n";
				*/

			?>
			<select name="show_addr" size=5 style="width:100%; height:140px; font-size:13px; " onclick="javascript:k_insert()" class="select_box">
		<?
				foreach ($json->post as $key=>$value) {
						 echo $value->post;                // 우편번호
						 //$value->addr_1;              // 시/도
						 //$value->addr_2;              // 구
						 //$value->addr_3;              // 도로명
						 //$value->addr_4;              // 동/건물
		?>
			<?if($cate=="B"){?>
				<option value='<?=substr($value->post,0,3)?>-<?=substr($value->post,3,3)?>|<?=$value->addr_1?> <?=$value->addr_2?> <?=$value->addr_3?> |<?=$value->addr_4?>'>(<?=substr($value->post,0,3)?>-<?=substr($value->post,3,3)?>) <?=$value->addr_1?> <?=$value->addr_2?> <?=$value->addr_3?> <?=$value->addr_4?> </option>
			<?}else if($cate=="A"){?>
				<option value='<?=substr($value->post,0,3)?>-<?=substr($value->post,3,3)?>|<?=$value->addr_1?> <?=$value->addr_2?> <?=$value->addr_3?> |<?=$value->addr_4?>'>(<?=substr($value->post,0,3)?>-<?=substr($value->post,3,3)?>) <?=$value->addr_1?> <?=$value->addr_2?> <?=$value->addr_3?> <?=$value->addr_4?></option>
			<?}?>
		<?
			}

		} else if($json->result == 0) {
			echo "검색결과가 없습니다";
		} else if($json->result < 0) {
			echo str_replace("Too many results.","<font color=red><b>검색결과가 너무 많습니다. 다시 검색바랍니다.</b></font>",$json->message);
		}

?>
	</select>

</div>
</form>
</body>
</html>
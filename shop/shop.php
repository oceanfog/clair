<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
include("../master/db_connect.inc");
include("../master/function.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript">
<!--
function reverse(form)
{
	for( var i=0; i<form.elements.length; i++) {
	  if (form.elements[i].name =='selectlist[]') {
		if(form.elements[i].checked == false) {
			form.elements[i].checked = true; }
		else {
			form.elements[i].checked = false; }
	  }
	}
	return;
}
function allcheck(form)
{
	for( var i=0; i<form.elements.length; i++) {
	  if (form.elements[i].name =='selectlist[]') {
			form.elements[i].checked = true; }
}
	return;
}



function cart_del(frm) {
    var chk    = document.getElementsByName("selectlist[]"); 
    var option_num = 0;         
     
    for(var i=0; i<chk.length; i++) { 
        if(chk[i].checked == true) { 
            option_num++; 
        } 
    } 

    if(option_num==0) { 
    alert('삭제하실 제품을 선택해주세요.'); 
    return; 
    } 

	frm.action="./cart_del.php";
	frm.submit();
}

function ju(frm) {
	var id=frm.id.value;
	
    var chk    = document.getElementsByName("selectlist[]"); 
    var option_num = 0;         
     
    for(var i=0; i<chk.length; i++) { 
        if(chk[i].checked == true) { 
            option_num++; 
        } 
    } 

    if(option_num==0) { 
    alert('ご注文の商品を選択してください'); 
    return; 
    } 

	frm.action="../order/order.php?board_code=write";
	frm.submit();

}
function ju_all(frm) {
	reverse(frm);
	frm.action="../order/order.php?board_code=write&mode=all";
	frm.submit();
}

function wish(frm) {
	if(frm.id.value=="") {
		alert('로그인후 이용하실수 있습니다');
		return;
	}
    var chk    = document.getElementsByName("selectlist[]"); 
    var option_num = 0;         
     
    for(var i=0; i<chk.length; i++) { 
        if(chk[i].checked == true) { 
            option_num++; 
        } 
    } 

    if(option_num==0) { 
    alert('위시리스트에 담을 상품이 선택되지 않았습니다.'); 
    return; 
    } 

	frm.action="../mypage/wish_db.php";
	frm.submit();
}
//-->
</script>

</head>
<body>
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
<form name="form1" action="" method="post">
<input type="hidden" name="id" value="<?echo $member_log;?>">
        <!-- container -->
        <div class="container">
            <div class="inner">
                
                <h2>장바구니</h2>
                <h3>장바구니 목록</h3>
                <form>
                    <fieldset>
                        <legend>장바구니</legend>
                        <table summary="장바구니 목록입니다. 각 항목으로는 선택, 상품옵션정보, 총 상품금액, 배송비가 있습니다.">
                            <caption>장비구니 목록</caption>
                            <colgroup>
                                <col width="10%" />
                                <col width="*" />
                                <col width="20%" />
                                <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">선택</th>
                                    <th scope="col">상품옵션정보</th>
                                    <th scope="col">총 상품금액</th>
                                    <th scope="col">수량</th>
                                </tr>
                            </thead>
                            <tbody>
<?
/** 쇼핑 카트 정보 출력 **/
		$sang_photo="../master/shop/thumb/";
		$tgcost=0;
		$tpoint=0;
		$qs="select * from imsi where cook='$buy_cookie'";
		$rel=@mysql_query($qs);
		$total=mysql_affected_rows();
	



		for($i=0;$i<$total;$i++){//총 세션 배열 갯수보다 적을때까지
		    $ro=@mysql_fetch_object($rel);
			$sql = "select * from goods where sang_no = $ro->imsi_sang_no";
			$result = @mysql_query($sql);

			$row = @mysql_fetch_object($result);
			$price=$ro->imsi_sang_price;

			 //이미지 없을때 있을때...
			if(file_exists($sang_photo.$row->img1) && $row->img1 !="") {
				$filename=$sang_photo.$row->img1;
			} else {
				$filename=$sang_photo."not_img_small.gif";
			}

			$item_price+=intval($ro->item_price);
			if($row->bae_chu=="y") $bae="y";

			//적립금
			/*
			if($row->per_set=="mon") $point=$row->point;
			else if($row->per_set=="per") $point=$ro->imsi_sang_price * ($row->point / 100);
			else $point=0;
			$point=ceil($point);
			*/

			if($ro->imsi_sang_color !="") $imc=" / ".$ro->imsi_sang_color;
			else $imc="";
	?>
                                <tr>
                                    <td><input type="checkbox" name="selectlist[]"  value="<?echo $ro->no;?>" /></td>
                                    <td class="option">
                                        <img src="../master/shop/sang_photo/<?=$row->img1;?>" width="85" height="85" alt="" />
                                        <dl>
                                            <dt><?=stripslashes($row->sang_name);?></dt>
                                            <dd><?echo number_format($price);?>원 <?=$imc;?></dd>
                                        </dl>
                                    </td>
                                    <td><? echo number_format(($price*$ro->imsi_gcount),0); ?>원</td>
                                    <td><?=$ro->imsi_gcount;?>개</td>
                                </tr>
<?
		$tgcost=$tgcost+($price*$ro->imsi_gcount)+ $ro->item_price;;
		$tpoint+=$point * $ro->imsi_gcount;
		unset($opt_all);
			}

		//배송비
		if($bae=="y" || $tgcost > 10000) $charge=0;
		else $charge=0;
?>
                            </tbody>
                        </table>
                        <!-- btn_left -->
                        <div class="btn_left">
                            <a href="javascript:allcheck(document.form1);">전체선택</a>
                            <a href="javascript:cart_del(document.form1);">선택삭제</a>
                        </div>
                        <!-- // btn_left -->
                        
                        <h3>결제정보</h3>
                        <table summary="결제정보 목록입니다. 각 항목으로는 주문금액, 배송비, 결재하실 금액이 있습니다.">
                            <caption>결제정보 목록</caption>
                            <colgroup>
                                <col width="30%" />
                                <col width="30%" />
                                <col width="*" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">주문금액</th>
                                    <th scope="col">배송비</th>
                                    <th scope="col">결재하실 금액</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?echo number_format($tgcost);?>원</td>
                                    <td><?=number_format($charge);?>원</td>
                                    <td class="pay"><?echo number_format($tgcost + $charge);?>원</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- btn_center -->
                        <div class="btn_center">
                            <a href="./buy.php">결재하기</a>
                        </div>
                        <!-- // btn_center -->
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- // container -->
        
        </form>
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>
    <!-- wrap -->
    
</body>
</html>
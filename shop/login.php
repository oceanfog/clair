<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- 최신 브라우저 문서모드로 변경 해주는 메타 태그 -->
<title>clair</title>
<link rel="stylesheet" type="text/css" href="../css/common.css" />
<link rel="stylesheet" media="screen and (min-width:1024px) and (max-width:1440px)" type="text/css" href="../css/notebook.css" /> <!-- mediaquery -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.easing.js"></script>
<script type="text/javascript" src="../js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.touchspin.js"></script>
<script type="text/javascript" src="../js/respond.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script>
<!--
function send(frm) {
	if(frm.buyer.value=="") {
		alert('구매자명을 입력해주세요');
		frm.buyer.focus();
		return;
	}
	if(frm.tel.value=="") {
		alert('연락처를 000-0000-0000 형식으로 입력해주세요');
		frm.tel.focus();
		return;
	}
	if(frm.pass.value=="") {
		alert('비밀번호를 입력해주세요');
		frm.pass.focus();
		return;
	}
	frm.action="./buy_list.php";
	frm.submit();
}

function op() {
	window.open("./pass.php","ww","width=390,height=270,left=600,top=320,toolbar=no,location=no,directories=no ,status=no,menubar=no,resizable=no,scrollbars=yes,copyhistory=no");
}
//-->
</script>
</head>
<body>
    <!-- wrap -->
    <div id="wrap">
        
        <? include "../inc/header.php" ?>
        
        <!-- container -->
        <div class="container">
            <div class="inner">
                
                <div class="login">
                    <form name="form1" method="post" action="">
                        <fieldset>
                            <legend>장바구니</legend>
                            <h2>주문조회</h2>
                            <p>비회원 로그인</p>
                            <div class="login_box">
                                <div class="login_cont">
                                    <dl>
                                        <dt><label for="buyer">구매자명</label></dt>
                                        <dd><input type="text" name="buyer" id="buyer" /></dd>
                                        <dt><label for="tel">연락처</label></dt>
                                        <dd><input type="text" name="tel" id="tel" /></dd>
                                        <dt><label for="pw">비밀번호</label></dt>
                                        <dd><input type="password" name="pass" id="pw" /></dd>
                                    </dl>
                                    <a href="javascript:send(document.form1);" class="green">조회하기</a>
                                    <a href="javascript:op();" class="gray">비밀번호찾기</a>
                                </div>
                                <p>비회원 구매시 입력했던<br />구매자명 / 비밀번호 / 연락처로 조회해주세요.<br />연락처는 "-" 포함해서 입력해주세요</p>

                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- // container -->
        
        
        <div class="footer">
            <p>상호명:(주)에이티앤에스그룹 통신사업자등록번호: 제 2014- 서울서초- 0014 호  사업자등록번호: 220-86-46491 주소: 서울특별시 서초구 반포대로 8 (서초동, 삼흥빌딩 5층)</p>
            <p>개인정보책임자: 강한나 TEL: 080-581-2917  FAX: 02-581-2919</p>
            <p class="copyright">Copyright(C) design creative Allright reserved.</p>
        </div>
        
    </div>
    <!-- wrap -->
    
</body>
</html>
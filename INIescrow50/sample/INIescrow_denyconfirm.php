<?php
/* INIescrow_denyconfirm.php
 *
 * 배송 등록 변경  요청을 처리한다.
 * 코드에 대한 자세한 설명은 매뉴얼을 참조하십시오.
 * <주의> 등록자의 세션을 반드시 체크하도록하여 부정등록를 방지하여 주십시요.
 *  
 * http://www.inicis.com
 * Copyright (C) 2006 Inicis Co., Ltd. All rights reserved.
 */


	/**************************
	 * 1. 라이브러리 인클루드 *
	 **************************/
	require("../libs/INILib.php");
	
	
	/***************************************
	 * 2. INIpay50 클래스의 인스턴스 생성 *
	 ***************************************/
	$iniescrow = new INIpay50;
	
	/*********************
	 * 3. 지불 정보 설정 *
	 *********************/
	$iniescrow->SetField("inipayhome", "/ydata/user/econshop_co_kr/public_html/INIescrow50");      // 이니페이 홈디렉터리(상점수정 필요)
	$iniescrow->SetField("tid",$tid); // 거래아이디
	$iniescrow->SetField("mid",$mid); // 상점아이디
    /**************************************************************************************************
     * admin 은 키패스워드 변수명입니다. 수정하시면 안됩니다. 1111의 부분만 수정해서 사용하시기 바랍니다.
     * 키패스워드는 상점관리자 페이지(https://iniweb.inicis.com)의 비밀번호가 아닙니다. 주의해 주시기 바랍니다.
     * 키패스워드는 숫자 4자리로만 구성됩니다. 이 값은 키파일 발급시 결정됩니다.
     * 키패스워드 값을 확인하시려면 상점측에 발급된 키파일 안의 readme.txt 파일을 참조해 주십시오.
     **************************************************************************************************/
	$iniescrow->SetField("admin","1111"); // 키패스워드(상점아이디에 따라 변경)
  	$iniescrow->SetField("type", "escrow"); 				                    // 고정 (절대 수정 불가)
	$iniescrow->SetField("escrowtype", "dcnf"); 				                    // 고정 (절대 수정 불가)
	$iniescrow->SetField("dcnf_name",$dcnf_name);
	$iniescrow->SetField("debug","true"); // 로그모드("true"로 설정하면 상세한 로그가 생성됨)

	/*********************
	 * 3. 거절확인 요청 *
	 *********************/
	$iniescrow->startAction();
	
	
	/**********************
	 * 4. 거절확인  결과 *
	 **********************/
	 
	 $tid          = $iniescrow->GetResult("tid"); 					// 거래번호
	 $resultCode   = $iniescrow->GetResult("ResultCode");		// 결과코드 ("00"이면 지불 성공)
	 $resultMsg    = $iniescrow->GetResult("ResultMsg");    // 결과내용 (지불결과에 대한 설명)
	 $resultDate   = $iniescrow->GetResult("DCNF_Date");    // 처리 날짜
	 $resultTime   = $iniescrow->GetResult("DCNF_Time");    // 처리 시각

?>




<html>
<head>

<title>INIescrow</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">



<style type="text/css">
	BODY{font-size:9pt; line-height:160%}
	TD{font-size:9pt; line-height:160%}
	INPUT{font-size:9pt;}
	.emp{background-color:#E0EFFE;}
</style>

</head>

<body>
<center>
<table border=0 width=500>
<tr align='center'>
<td>
<hr noshade size=1>
<b>거절 확인 처리 결과</b>
<hr noshade size=1>
</td>
</tr>
</table>
<br>

<table border=0 width=500>
	
		
	<tr>
	<td align=right nowrap>결과코드 : </td><td><font class=emp><?php echo $resultCode?></font></td>
	</tr>
	
	<tr>
	<td align=right nowrap>결과메세지 : </td><td><font class=emp><?php echo $resultMsg?></font></td>
	</tr>
	
	<tr>
	<td align=right nowrap>처리날짜(YYYYMMDD)  : </td><td><font class=emp><?php echo $resultDate?></font></td>
	</tr>
	
	<tr>
	<td align=right nowrap>처리시각(hhmmss)  : </td><td><font class=emp><?php echo $resultTime?></font></td>
	</tr>
	
	<tr>
	<td colspan=2><hr noshade size=1></td>
	</tr>
	<tr>
	<td align=right colspan=2>Copyright Inicis, Co.<br>www.inicis.com</td>
	</tr>
</table>
</center>
</body>
</html>
